To compile you will need the systemj compiler .jar and the jdom2.jar
file in your CLASSPATH environment variable.


To compile all the *.sysj files in this directory run

make


To clean the generated files run

make clean


To compile a single file run


FILE=<name> make compile


<name> is without the ".sysj" part

============================================================
This is the description of all the systemj examples.


Examples:


1.) Frequency-relay

# of clock-domains = 2 

This example finds the peaks of input power signals as sample counts and
then calculates the change in frequency and the rate of change. If the
change in frequency and the rate of change are above some thresh hold
then power switches are opened or closed. The user is allowed to change
these thresh-holds at program runtime.


2.) Asproto.sysj

# of clock-domains = 2

This example emulates a (network) protocol stack. One of the input
clock-domains act as a test-bench to the protocol stack.


3.) kite.sysj

# of clock-domains = 1

This example takes inputs from wind-surfing kite sensors and then
changes automatically the directions of the rudders (both port and
starboard sides). It also controllers the motor speeds.


4.) pcabro.sysj

# of clock-domains = 3

This is an enhanced version of the ABRO example by berry. See Berry
(Esterel) for further information.


5.) pumpcontroller.sysj

# of clock-domains = 2

This is a pump controller that controls a water pump at different level
again inspired by Berry (Esterel). We have 2 clock-domains,
SensorClockDomain gives the inputs and acts as the test-bench and the
Controller clock-domains is the actual controller. This is like modeling
plant and controller in Simulink.


6.) CruiseManager.sysj 


# of clock-domains = 1

This example is compiled from function-block specification to SystemJ
(by David Huang' compiler). This example models the cruise control of a
ship. It takes in inputs from the environment and changes the
directions, acceleration, speed, etc of the cruise ship.


7.) demoloop.sysj

# of clock-domains = 1

This is the example inspired by "me" (Avinash) it does nothing, and is
usually used by noobs when trying to understand SystemJ.


8.) server_client.sysj

# of clock-domains = 2

This example implements an authorizations and command execution script
between a server and a client. A good program to start with if one wants
to understand the different mechanisms in SystemJ.


9.) PACEMAKER

# of clock-domains = 1

This example implements the pacemaker logic.

10.) AECS

# of clock-domains = 3

This example implements the logic to control a smart room.
