package Frq;
import java.lang.Math;
public class CalculateSample {
	
  float ffl;   // fundamental frequency level 
  float hl1;   // first harmonic level
  float hl2;   // second harmonic level
  float hl3;   // third harmonic level
  float hl4;   // fourth harmonic level
  float hl5;   // fifth harmonic level

  float fps;   // fundamental frequency phase shift
  float ps1;   // first harmonic phase shift
  float ps2;   // second harmonic phase shift
  float ps3;   // third harmonic phase shift
  float ps4;   // fourth harmonic phase shift
  float ps5;   // fifth harmonic phase shift

  public CalculateSample () {
    ffl = 5; hl1 = 1; hl2 = 0.6f; hl3 = 0.3f; hl4 = 0.2f; hl5 = 0.1f;
    fps = 2.1f; ps1 = 0.9f; ps2 = 0.7f; ps3 = 0.6f; ps4 = 0.4f; ps5 = 0.3f;
  }

  public float compute (int n, int period) {
    float result;
    double pi = Math.PI;
    double ff, h1, h2, h3, h4, h5;
    ff = ffl*Math.sin(2*pi*n/period + fps);
    h1 = hl1*Math.sin(3*2*pi*n/period + ps1);
    h2 = hl2*Math.sin(5*2*pi*n/period + ps2);
    h3 = hl3*Math.sin(7*2*pi*n/period + ps3);
    h4 = hl4*Math.sin(9*2*pi*n/period + ps4);
    h5 = hl5*Math.sin(11*2*pi*n/period + ps5);
	
    result = (float)(ff + h1 + h2 + h3 + h4 + h5);

    return result;
  }
}
