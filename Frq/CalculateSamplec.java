package Frq;;

public class CalculateSamplec {

    float ffl;   // fundamental frequency level 
    float hl1;   // first harmonic level
    float hl2;   // second harmonic level
    float hl3;   // third harmonic level
    float hl4;   // fourth harmonic level
    float hl5;   // fifth harmonic level
    float [] val = new float[91];
    float fps;   // fundamental frequency phase shift
    float ps1;   // first harmonic phase shift
    float ps2;   // second harmonic phase shift
    float ps3;   // third harmonic phase shift
    float ps4;   // fourth harmonic phase shift
    float ps5;   // fifth harmonic phase shift

    public CalculateSamplec () {
        ffl = 5f; hl1 = 1.2f; hl2 = 0.6f; hl3 = 0.3f; hl4 = 0.2f; hl5 = 0.1f;
        fps = 2.1f; ps1 = 0.9f; ps2 = 0.7f; ps3 = 0.6f; ps4 = 0.4f; ps5 = 0.3f;
    }
    
    //Both n,period are just integers.

    public float compute (int n, int period) {
        float result;
        float pi = 3.141592654f;
        float ff, h1, h2, h3, h4, h5;	
        ff = ffl*(sine(2*pi*n/period + fps));
        h1 = hl1*(sine(3*2*pi*n/period + ps1));
        h2 = hl2*(sine(5*2*pi*n/period + ps2));
        h3 = hl3*(sine(7*2*pi*n/period + ps3));
        h4 = hl4*(sine(9*2*pi*n/period + ps4));
        h5 = hl5*(sine(11*2*pi*n/period + ps5));
	
	result = (float)(ff + h1 + h2 + h3 + h4 + h5);

	return result;
    }
    public float sine(float valin){
		float res = 0.01f;
		/*
		 * This is only for the frequency relay example.
		 * This does not follow IEEE precision of any kind
		 */
		
		//SINE function definition is:
		/*
		 * sum(0->infinity)((-1)^n-1/(2*n-1)!)*x^(2n-1)
		 * 1.) 0<=val<=90, just take the val from the table
		 * 2.) 90< val <= 180, val-90 take from the table.
		 * 3.) 180 < val <= 270, val-180 take from table and negate.
		 * 4.) 270 <val <= 360, 360-val take from the table and negate.
		 * 5.) if(val > 360) keep on taking the raminder until less than 360
		 * and then do the above things.
		 */			
		val[0] =0.01f;
		val[1]= 0.017452406f;
		val[2]= 0.034899497f;
		val[3]=0.052335956f;
		val[4]=0.069756474f;
		val[5]=0.087155743f;
		val[6]=0.104528463f;
		val[7]=0.121869343f;
		val[8]=0.139173101f;
		val[9]=0.156434465f;
		val[10]=0.173648178f;
		val[11]=0.190808995f;
		val[12]=0.207911691f;
		val[13]=0.224951054f;
		val[14]=0.241921896f;
		val[15]=0.258819045f;
		val[16]=0.275637356f;
		val[17]=0.292371705f;
		val[18]=0.309016994f;
		val[19]=0.325568154f;
		val[20]=0.342020143f;
		val[21]=0.35836795f;
		val[22]=0.374606593f;
		val[23]=0.390731128f;
		val[24]=0.406736643f;
		val[25]=0.422618262f;
		val[26]=0.438371147f;
		val[27]=0.4539905f;
		val[28]=0.469471563f;
		val[29]=0.48480962f;
		val[30]=0.5f;
		val[31]=0.515038075f;
		val[32]=0.529919264f;
		val[33]=0.544639035f;
		val[34]=0.559192903f;
		val[35]=0.573576436f;
		val[36]=0.587785252f;
		val[37]=0.601815023f;
		val[38]=0.615661475f;
		val[39]=0.629320391f;
		val[40]=0.64278761f;
		val[41]=0.656059029f;
		val[42]=0.669130606f;
		val[43]=0.68199836f;
		val[44]=0.69465837f;
		val[45]=0.707106781f;
		val[46]=0.7193398f;
		val[47]=0.731353702f;
		val[48]=0.743144825f;
		val[49]=0.75470958f;
		val[50]=0.766044443f;
		val[51]=0.777145961f;
		val[52]=0.788010754f;
		val[53]=0.79863551f;
		val[54]=0.809016994f;
		val[55]=0.819152044f;
		val[56]=0.829037573f;
		val[57]=0.838670568f;
		val[58]=0.848048096f;
		val[59]=0.857167301f;
		val[60]=0.866025404f;
		val[61]=0.874619707f;
		val[62]=0.882947593f;
		val[63]=0.891006524f;
		val[64]=0.898794046f;
		val[65]=0.906307787f;
		val[66]=0.913545458f;
		val[67]=0.920504853f;
		val[68]=0.927183855f;
		val[69]=0.933580426f;
		val[70]=0.939692621f;
		val[71]=0.945518576f;
		val[72]=0.951056516f;
		val[73]=0.956304756f;
		val[74]=0.961261696f;
		val[75]=0.965925826f;
		val[76]=0.970295726f;
		val[77]=0.974370065f;
		val[78]=0.978147601f;
		val[79]=0.981627183f;
		val[80]=0.984807753f;
		val[81]=0.987688341f;
		val[82]=0.990268069f;
		val[83]=0.992546152f;
		val[84]=0.994521895f;
		val[85]=0.996194698f;
		val[86]=0.99756405f;
		val[87]=0.998629535f;
		val[88]=0.999390827f;
		val[89]=0.999847695f;
		val[90]=1.01f;	
		
		
		
		int newval = recurse((int)(valin*180/3.141592654));		
		if(0 <= newval && newval <= 90){
			res = val[newval];
		}
		else if(90< newval && newval <= 180){			
			res = val[newval-90];
		}
		else if(180< newval && newval <= 270){
			res = val[newval-180];
			res = -res;
		}
		else if(270<newval && newval <= 360){
			res = val[360-newval];
			res = -res;
		}
		else
			new Exception("Greater than 360 after recursing");
				
		return res;
	}
	
	private int recurse(int val){		
		val = (val%360);
		if(val > 360){
			recurse(val);			
		}
		return val;			
	}
}
