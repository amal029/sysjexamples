import java.util.*;
import systemj.bootstrap.ClockDomain;
import systemj.lib.*;

public class hrtcs extends ClockDomain{
  public hrtcs(){super(); init();}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  public Signal TOUCH = new Signal();
  public Signal GREEN_LIGHT = new Signal();
  private int counter_thread_3;//hrtcs.sysj line: 9, column: 7
  private int S92 = 1;
  private int S30 = 1;
  private int S5 = 1;
  private int S27 = 1;
  
  private int[] ends = new int[4];
  private int[] tdone = new int[4];
  
  public void thread101(int [] tdone, int [] ends){
        S27=1;
    counter_thread_3 = 0;//hrtcs.sysj line: 9, column: 7
    counter_thread_3 = counter_thread_3 + 1;//hrtcs.sysj line: 11, column: 8
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread100(int [] tdone, int [] ends){
        S5=1;
    GREEN_LIGHT.setPresent();//hrtcs.sysj line: 5, column: 6
    currsigs.addElement(GREEN_LIGHT);
    System.out.println("Emitted GREEN_LIGHT");
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void thread98(int [] tdone, int [] ends){
        switch(S27){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        if(counter_thread_3 == 10){//hrtcs.sysj line: 14, column: 11
          ends[3]=3;
          ;//hrtcs.sysj line: 8, column: 6
          S27=0;
          active[3]=0;
          ends[3]=0;
          tdone[3]=1;
        }
        else {
          counter_thread_3 = counter_thread_3 + 1;//hrtcs.sysj line: 11, column: 8
          active[3]=1;
          ends[3]=1;
          tdone[3]=1;
        }
        break;
      
    }
  }

  public void thread97(int [] tdone, int [] ends){
        switch(S5){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        GREEN_LIGHT.setPresent();//hrtcs.sysj line: 5, column: 6
        currsigs.addElement(GREEN_LIGHT);
        System.out.println("Emitted GREEN_LIGHT");
        active[2]=1;
        ends[2]=1;
        tdone[2]=1;
        break;
      
    }
  }

  public void thread95(int [] tdone, int [] ends){
        S27=1;
    counter_thread_3 = 0;//hrtcs.sysj line: 9, column: 7
    counter_thread_3 = counter_thread_3 + 1;//hrtcs.sysj line: 11, column: 8
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread94(int [] tdone, int [] ends){
        S5=1;
    GREEN_LIGHT.setPresent();//hrtcs.sysj line: 5, column: 6
    currsigs.addElement(GREEN_LIGHT);
    System.out.println("Emitted GREEN_LIGHT");
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S92){
        case 0 : 
          S92=0;
          break RUN;
        
        case 1 : 
          S92=2;
          S92=2;
          S30=0;
          thread94(tdone,ends);
          thread95(tdone,ends);
          int biggest96 = 0;
          if(ends[2]>=biggest96){
            biggest96=ends[2];
          }
          if(ends[3]>=biggest96){
            biggest96=ends[3];
          }
          if(biggest96 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          switch(S30){
            case 0 : 
              if(TOUCH.getprestatus()){//hrtcs.sysj line: 4, column: 10
                S30=1;
                active[1]=1;
                ends[1]=1;
                break RUN;
              }
              else {
                thread97(tdone,ends);
                thread98(tdone,ends);
                int biggest99 = 0;
                if(ends[2]>=biggest99){
                  biggest99=ends[2];
                }
                if(ends[3]>=biggest99){
                  biggest99=ends[3];
                }
                if(biggest99 == 1){
                  active[1]=1;
                  ends[1]=1;
                  break RUN;
                }
                //FINXME code
                if(biggest99 == 0){
                  S30=1;
                  active[1]=1;
                  ends[1]=1;
                  break RUN;
                }
              }
            
            case 1 : 
              S30=1;
              S30=2;
              active[1]=1;
              ends[1]=1;
              break RUN;
            
            case 2 : 
              S30=2;
              S30=3;
              active[1]=1;
              ends[1]=1;
              break RUN;
            
            case 3 : 
              S30=3;
              S30=0;
              thread100(tdone,ends);
              thread101(tdone,ends);
              int biggest102 = 0;
              if(ends[2]>=biggest102){
                biggest102=ends[2];
              }
              if(ends[3]>=biggest102){
                biggest102=ends[3];
              }
              if(biggest102 == 1){
                active[1]=1;
                ends[1]=1;
                break RUN;
              }
            
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        if(!df){
          TOUCH.gethook();
          df = true;
        }
        runClockDomain();
      }
      TOUCH.setpreclear();
      GREEN_LIGHT.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      dummyint = TOUCH.getStatus() ? TOUCH.setprepresent() : TOUCH.setpreclear();
      TOUCH.setpreval(TOUCH.getValue());
      TOUCH.setClear();
      GREEN_LIGHT.sethook();
      GREEN_LIGHT.setClear();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        TOUCH.gethook();
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
