import java.util.*;
import com.systemj.ClockDomain;
import com.systemj.Signal;
import com.systemj.input_Channel;
import com.systemj.output_Channel;

public class W extends ClockDomain{
  public W(String name){super(name);}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  private Signal washon_1;
  private Signal washoff_1;
  private Signal spinon_1;
  private Signal spinoff_1;
  private Signal WASHING_1;
  private Signal DRYING_1;
  private int S134 = 1;
  private int S37 = 1;
  private int S15 = 1;
  private int S127 = 1;
  private int S59 = 1;
  private int S47 = 1;
  private int S132 = 1;
  private int S128 = 1;
  
  private int[] ends = new int[5];
  private int[] tdone = new int[5];
  
  public void thread142(int [] tdone, int [] ends){
        switch(S132){
      case 0 : 
        active[4]=0;
        ends[4]=0;
        tdone[4]=1;
        break;
      
      case 1 : 
        switch(S128){
          case 0 : 
            S128=0;
            S128=1;
            active[4]=1;
            ends[4]=1;
            tdone[4]=1;
            break;
          
          case 1 : 
            S128=1;
            washon_1.setPresent();//washing.sysj line: 25, column: 19
            currsigs.addElement(washon_1);
            System.out.println("Emitted washon_1");
            S128=2;
            active[4]=1;
            ends[4]=1;
            tdone[4]=1;
            break;
          
          case 2 : 
            S128=2;
            S128=3;
            active[4]=1;
            ends[4]=1;
            tdone[4]=1;
            break;
          
          case 3 : 
            S128=3;
            spinon_1.setPresent();//washing.sysj line: 26, column: 7
            currsigs.addElement(spinon_1);
            System.out.println("Emitted spinon_1");
            S128=4;
            active[4]=1;
            ends[4]=1;
            tdone[4]=1;
            break;
          
          case 4 : 
            S128=4;
            spinoff_1.setPresent();//washing.sysj line: 26, column: 25
            currsigs.addElement(spinoff_1);
            System.out.println("Emitted spinoff_1");
            washoff_1.setPresent();//washing.sysj line: 26, column: 38
            currsigs.addElement(washoff_1);
            System.out.println("Emitted washoff_1");
            S128=0;
            active[4]=1;
            ends[4]=1;
            tdone[4]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread141(int [] tdone, int [] ends){
        switch(S127){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        switch(S59){
          case 0 : 
            if(spinon_1.getprestatus()){//washing.sysj line: 17, column: 13
              S59=1;
              S47=0;
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            else {
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            break;
          
          case 1 : 
            if(WASHING_1.getprestatus()){//washing.sysj line: 18, column: 13
              S59=0;
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            else {
              switch(S47){
                case 0 : 
                  S47=0;
                  S47=1;
                  DRYING_1.setPresent();//washing.sysj line: 20, column: 22
                  currsigs.addElement(DRYING_1);
                  System.out.println("Emitted DRYING_1");
                  active[3]=1;
                  ends[3]=1;
                  tdone[3]=1;
                  break;
                
                case 1 : 
                  if(spinoff_1.getprestatus()){//washing.sysj line: 20, column: 8
                    S59=0;
                    active[3]=1;
                    ends[3]=1;
                    tdone[3]=1;
                  }
                  else {
                    DRYING_1.setPresent();//washing.sysj line: 20, column: 22
                    currsigs.addElement(DRYING_1);
                    System.out.println("Emitted DRYING_1");
                    active[3]=1;
                    ends[3]=1;
                    tdone[3]=1;
                  }
                  break;
                
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread140(int [] tdone, int [] ends){
        switch(S37){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        switch(S15){
          case 0 : 
            if(washon_1.getprestatus()){//washing.sysj line: 11, column: 13
              S15=1;
              WASHING_1.setPresent();//washing.sysj line: 12, column: 27
              currsigs.addElement(WASHING_1);
              System.out.println("Emitted WASHING_1");
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            else {
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            break;
          
          case 1 : 
            if(washoff_1.getprestatus()){//washing.sysj line: 12, column: 13
              S15=0;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            else {
              WASHING_1.setPresent();//washing.sysj line: 12, column: 27
              currsigs.addElement(WASHING_1);
              System.out.println("Emitted WASHING_1");
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread138(int [] tdone, int [] ends){
        S132=1;
    S128=0;
    active[4]=1;
    ends[4]=1;
    tdone[4]=1;
  }

  public void thread137(int [] tdone, int [] ends){
        S127=1;
    S59=0;
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread136(int [] tdone, int [] ends){
        S37=1;
    S15=0;
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S134){
        case 0 : 
          S134=0;
          break RUN;
        
        case 1 : 
          S134=2;
          S134=2;
          washon_1.setClear();//washing.sysj line: 2, column: 3
          washoff_1.setClear();//washing.sysj line: 3, column: 3
          spinon_1.setClear();//washing.sysj line: 4, column: 3
          spinoff_1.setClear();//washing.sysj line: 5, column: 3
          WASHING_1.setClear();//washing.sysj line: 7, column: 3
          DRYING_1.setClear();//washing.sysj line: 8, column: 3
          thread136(tdone,ends);
          thread137(tdone,ends);
          thread138(tdone,ends);
          int biggest139 = 0;
          if(ends[2]>=biggest139){
            biggest139=ends[2];
          }
          if(ends[3]>=biggest139){
            biggest139=ends[3];
          }
          if(ends[4]>=biggest139){
            biggest139=ends[4];
          }
          if(biggest139 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          washon_1.setClear();//washing.sysj line: 2, column: 3
          washoff_1.setClear();//washing.sysj line: 3, column: 3
          spinon_1.setClear();//washing.sysj line: 4, column: 3
          spinoff_1.setClear();//washing.sysj line: 5, column: 3
          WASHING_1.setClear();//washing.sysj line: 7, column: 3
          DRYING_1.setClear();//washing.sysj line: 8, column: 3
          thread140(tdone,ends);
          thread141(tdone,ends);
          thread142(tdone,ends);
          int biggest143 = 0;
          if(ends[2]>=biggest143){
            biggest143=ends[2];
          }
          if(ends[3]>=biggest143){
            biggest143=ends[3];
          }
          if(ends[4]>=biggest143){
            biggest143=ends[4];
          }
          if(biggest143 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          //FINXME code
          if(biggest143 == 0){
            S134=0;
            active[1]=0;
            ends[1]=0;
            S134=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    washon_1 = new Signal();
    washoff_1 = new Signal();
    spinon_1 = new Signal();
    spinoff_1 = new Signal();
    WASHING_1 = new Signal();
    DRYING_1 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        if(!df){
          df = true;
        }
        runClockDomain();
      }
      washon_1.setpreclear();
      washoff_1.setpreclear();
      spinon_1.setpreclear();
      spinoff_1.setpreclear();
      WASHING_1.setpreclear();
      DRYING_1.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      washon_1.setClear();
      washoff_1.setClear();
      spinon_1.setClear();
      spinoff_1.setClear();
      WASHING_1.setClear();
      DRYING_1.setClear();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
