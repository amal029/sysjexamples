# Compiling all the examples
all:
	for examples in `ls *.sysj` ; do \
		java JavaPrettyPrinter $$examples ;\
	done
	javac *.java 

compile:
	java JavaPrettyPrinter --nojavac $(FILE).sysj
	javac *.java

clean:
	rm -rf *.java *.class
