package scPackage;

public class Permission {
  public static int getPermission(char UserID, char cmd){
    int ret = 0;
    /**
     * @author amal029
     * Do Computation: Lookup database
     */
    switch(UserID){
    case 'a':
      switch(cmd){
      case 'a': break;
      case 'b': ret = 1; break;
      default: ret = 0;
      }
      break;
    case 'b':
      switch(cmd){
      case 'a': ret = 1; break;
      case 'b': break;
      default: ret = 0;
      }
      break;
    }
    return ret;
  }
}
