import java.util.*;
import systemj.bootstrap.ClockDomain;
import systemj.lib.*;

public class motorCD extends ClockDomain{
  public motorCD(){super(); init();}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  private Signal Step_1;
  private Signal Hold_1;
  private Signal Enable_1;
  private Signal Coil1_1;
  private Signal Coil2_1;
  private Signal LongStep_1;
  private int i_thread_6;//motor.sysj line: 99, column: 2
  private int i_thread_2;//motor.sysj line: 12, column: 2
  private int i_thread_4;//motor.sysj line: 64, column: 8
  private int i_thread_3;//motor.sysj line: 36, column: 8
  private int S22803 = 1;
  private int S22407 = 1;
  private int S6 = 1;
  private int S37 = 1;
  private int S8 = 1;
  private int S229 = 1;
  private int S69 = 1;
  private int S257 = 1;
  private int S244 = 1;
  private int S22433 = 1;
  private int S22409 = 1;
  private int S22801 = 1;
  private int S22454 = 1;
  
  private int[] ends = new int[7];
  private int[] tdone = new int[7];
  
  public void thread22931(int [] tdone, int [] ends){
        switch(S22801){
      case 0 : 
        active[6]=0;
        ends[6]=0;
        tdone[6]=1;
        break;
      
      case 1 : 
        switch(S22454){
          case 0 : 
            i_thread_6 = i_thread_6 + 1;//motor.sysj line: 104, column: 8
            Hold_1.setPresent();//motor.sysj line: 105, column: 8
            currsigs.addElement(Hold_1);
            System.out.println("Emitted Hold_1");
            if(i_thread_6 > 10){//motor.sysj line: 106, column: 11
              ends[6]=2;
              ;//motor.sysj line: 102, column: 4
              i_thread_6 = 0;//motor.sysj line: 112, column: 4
              S22454=1;
              i_thread_6 = i_thread_6 + 1;//motor.sysj line: 115, column: 8
              if(i_thread_6 > 7){//motor.sysj line: 116, column: 11
                ends[6]=2;
                ;//motor.sysj line: 113, column: 4
                S22454=2;
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
              }
              else {
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
              }
            }
            else {
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            break;
          
          case 1 : 
            i_thread_6 = i_thread_6 + 1;//motor.sysj line: 115, column: 8
            if(i_thread_6 > 7){//motor.sysj line: 116, column: 11
              ends[6]=2;
              ;//motor.sysj line: 113, column: 4
              S22454=2;
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            else {
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            break;
          
          case 2 : 
            S22454=2;
            i_thread_6 = 0;//motor.sysj line: 101, column: 4
            S22454=0;
            i_thread_6 = i_thread_6 + 1;//motor.sysj line: 104, column: 8
            Hold_1.setPresent();//motor.sysj line: 105, column: 8
            currsigs.addElement(Hold_1);
            System.out.println("Emitted Hold_1");
            if(i_thread_6 > 10){//motor.sysj line: 106, column: 11
              ends[6]=2;
              ;//motor.sysj line: 102, column: 4
              i_thread_6 = 0;//motor.sysj line: 112, column: 4
              S22454=1;
              i_thread_6 = i_thread_6 + 1;//motor.sysj line: 115, column: 8
              if(i_thread_6 > 7){//motor.sysj line: 116, column: 11
                ends[6]=2;
                ;//motor.sysj line: 113, column: 4
                S22454=2;
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
              }
              else {
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
              }
            }
            else {
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread22906(int [] tdone, int [] ends){
        switch(S22433){
      case 0 : 
        active[5]=0;
        ends[5]=0;
        tdone[5]=1;
        break;
      
      case 1 : 
        switch(S22409){
          case 0 : 
            if(Step_1.getprestatus()){//motor.sysj line: 82, column: 8
              Coil1_1.setPresent();//motor.sysj line: 83, column: 2
              currsigs.addElement(Coil1_1);
              Coil1_1.setValue(true);//motor.sysj line: 83, column: 2
              System.out.println("Emitted Coil1_1");
              S22409=1;
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            else {
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            break;
          
          case 1 : 
            if(Step_1.getprestatus()){//motor.sysj line: 85, column: 8
              Coil2_1.setPresent();//motor.sysj line: 86, column: 2
              currsigs.addElement(Coil2_1);
              Coil2_1.setValue(true);//motor.sysj line: 86, column: 2
              System.out.println("Emitted Coil2_1");
              S22409=2;
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            else {
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            break;
          
          case 2 : 
            if(Step_1.getprestatus()){//motor.sysj line: 88, column: 8
              Coil1_1.setPresent();//motor.sysj line: 89, column: 2
              currsigs.addElement(Coil1_1);
              Coil1_1.setValue(true);//motor.sysj line: 89, column: 2
              System.out.println("Emitted Coil1_1");
              S22409=3;
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            else {
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            break;
          
          case 3 : 
            if(Step_1.getprestatus()){//motor.sysj line: 91, column: 8
              Coil2_1.setPresent();//motor.sysj line: 92, column: 2
              currsigs.addElement(Coil2_1);
              Coil2_1.setValue(true);//motor.sysj line: 92, column: 2
              System.out.println("Emitted Coil2_1");
              S22409=4;
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            else {
              active[5]=1;
              ends[5]=1;
              tdone[5]=1;
            }
            break;
          
          case 4 : 
            S22409=4;
            S22409=0;
            active[5]=1;
            ends[5]=1;
            tdone[5]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread22904(int [] tdone, int [] ends){
        switch(S257){
      case 0 : 
        active[4]=0;
        ends[4]=0;
        tdone[4]=1;
        break;
      
      case 1 : 
        switch(S244){
          case 0 : 
            if(i_thread_4 == 7){//motor.sysj line: 67, column: 8
              ends[4]=3;
              ;//motor.sysj line: 65, column: 8
              S244=1;
              LongStep_1.setPresent();//motor.sysj line: 72, column: 8
              currsigs.addElement(LongStep_1);
              System.out.println("Emitted LongStep_1");
              active[4]=1;
              ends[4]=1;
              tdone[4]=1;
            }
            else {
              i_thread_4 = i_thread_4 + 1;//motor.sysj line: 68, column: 5
              active[4]=1;
              ends[4]=1;
              tdone[4]=1;
            }
            break;
          
          case 1 : 
            LongStep_1.setPresent();//motor.sysj line: 72, column: 8
            currsigs.addElement(LongStep_1);
            System.out.println("Emitted LongStep_1");
            active[4]=1;
            ends[4]=1;
            tdone[4]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread22885(int [] tdone, int [] ends){
        switch(S229){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        switch(S69){
          case 0 : 
            if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
              ends[3]=5;
              ;//motor.sysj line: 37, column: 8
              if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
                Enable_1.setPresent();//motor.sysj line: 47, column: 8
                currsigs.addElement(Enable_1);
                Enable_1.setValue(false);//motor.sysj line: 47, column: 8
                System.out.println("Emitted Enable_1");
                i_thread_3 = 0;//motor.sysj line: 48, column: 8
                S69=1;
                if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
                  ends[3]=4;
                  ;//motor.sysj line: 49, column: 8
                  if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
                    S69=2;
                    active[3]=1;
                    ends[3]=1;
                    tdone[3]=1;
                  }
                  else {
                    ends[3]=2;
                    tdone[3]=1;
                  }
                }
                else {
                  i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
                  active[3]=1;
                  ends[3]=1;
                  tdone[3]=1;
                }
              }
              else {
                ends[3]=2;
                tdone[3]=1;
              }
            }
            else {
              i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            break;
          
          case 1 : 
            if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
              ends[3]=4;
              ;//motor.sysj line: 49, column: 8
              if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
                S69=2;
                active[3]=1;
                ends[3]=1;
                tdone[3]=1;
              }
              else {
                ends[3]=2;
                tdone[3]=1;
              }
            }
            else {
              i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            break;
          
          case 2 : 
            S69=2;
            Enable_1.setPresent();//motor.sysj line: 35, column: 8
            currsigs.addElement(Enable_1);
            Enable_1.setValue(false);//motor.sysj line: 35, column: 8
            System.out.println("Emitted Enable_1");
            i_thread_3 = 0;//motor.sysj line: 36, column: 8
            S69=0;
            if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
              ends[3]=5;
              ;//motor.sysj line: 37, column: 8
              if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
                Enable_1.setPresent();//motor.sysj line: 47, column: 8
                currsigs.addElement(Enable_1);
                Enable_1.setValue(false);//motor.sysj line: 47, column: 8
                System.out.println("Emitted Enable_1");
                i_thread_3 = 0;//motor.sysj line: 48, column: 8
                S69=1;
                if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
                  ends[3]=4;
                  ;//motor.sysj line: 49, column: 8
                  if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
                    S69=2;
                    active[3]=1;
                    ends[3]=1;
                    tdone[3]=1;
                  }
                  else {
                    ends[3]=2;
                    tdone[3]=1;
                  }
                }
                else {
                  i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
                  active[3]=1;
                  ends[3]=1;
                  tdone[3]=1;
                }
              }
              else {
                ends[3]=2;
                tdone[3]=1;
              }
            }
            else {
              i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread22883(int [] tdone, int [] ends){
        S257=1;
    i_thread_4 = 0;//motor.sysj line: 64, column: 8
    S244=0;
    if(i_thread_4 == 7){//motor.sysj line: 67, column: 8
      ends[4]=3;
      ;//motor.sysj line: 65, column: 8
      S244=1;
      LongStep_1.setPresent();//motor.sysj line: 72, column: 8
      currsigs.addElement(LongStep_1);
      System.out.println("Emitted LongStep_1");
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
    else {
      i_thread_4 = i_thread_4 + 1;//motor.sysj line: 68, column: 5
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread22870(int [] tdone, int [] ends){
        S229=1;
    Enable_1.setPresent();//motor.sysj line: 35, column: 8
    currsigs.addElement(Enable_1);
    Enable_1.setValue(false);//motor.sysj line: 35, column: 8
    System.out.println("Emitted Enable_1");
    i_thread_3 = 0;//motor.sysj line: 36, column: 8
    S69=0;
    if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
      ends[3]=5;
      ;//motor.sysj line: 37, column: 8
      if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
        Enable_1.setPresent();//motor.sysj line: 47, column: 8
        currsigs.addElement(Enable_1);
        Enable_1.setValue(false);//motor.sysj line: 47, column: 8
        System.out.println("Emitted Enable_1");
        i_thread_3 = 0;//motor.sysj line: 48, column: 8
        S69=1;
        if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
          ends[3]=4;
          ;//motor.sysj line: 49, column: 8
          if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
            S69=2;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
          }
          else {
            ends[3]=2;
            tdone[3]=1;
          }
        }
        else {
          i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
          active[3]=1;
          ends[3]=1;
          tdone[3]=1;
        }
      }
      else {
        ends[3]=2;
        tdone[3]=1;
      }
    }
    else {
      i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
      active[3]=1;
      ends[3]=1;
      tdone[3]=1;
    }
  }

  public void thread22868(int [] tdone, int [] ends){
        S257=1;
    i_thread_4 = 0;//motor.sysj line: 64, column: 8
    S244=0;
    if(i_thread_4 == 7){//motor.sysj line: 67, column: 8
      ends[4]=3;
      ;//motor.sysj line: 65, column: 8
      S244=1;
      LongStep_1.setPresent();//motor.sysj line: 72, column: 8
      currsigs.addElement(LongStep_1);
      System.out.println("Emitted LongStep_1");
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
    else {
      i_thread_4 = i_thread_4 + 1;//motor.sysj line: 68, column: 5
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread22855(int [] tdone, int [] ends){
        S229=1;
    Enable_1.setPresent();//motor.sysj line: 35, column: 8
    currsigs.addElement(Enable_1);
    Enable_1.setValue(false);//motor.sysj line: 35, column: 8
    System.out.println("Emitted Enable_1");
    i_thread_3 = 0;//motor.sysj line: 36, column: 8
    S69=0;
    if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
      ends[3]=5;
      ;//motor.sysj line: 37, column: 8
      if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
        Enable_1.setPresent();//motor.sysj line: 47, column: 8
        currsigs.addElement(Enable_1);
        Enable_1.setValue(false);//motor.sysj line: 47, column: 8
        System.out.println("Emitted Enable_1");
        i_thread_3 = 0;//motor.sysj line: 48, column: 8
        S69=1;
        if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
          ends[3]=4;
          ;//motor.sysj line: 49, column: 8
          if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
            S69=2;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
          }
          else {
            ends[3]=2;
            tdone[3]=1;
          }
        }
        else {
          i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
          active[3]=1;
          ends[3]=1;
          tdone[3]=1;
        }
      }
      else {
        ends[3]=2;
        tdone[3]=1;
      }
    }
    else {
      i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
      active[3]=1;
      ends[3]=1;
      tdone[3]=1;
    }
  }

  public void thread22853(int [] tdone, int [] ends){
        S257=1;
    i_thread_4 = 0;//motor.sysj line: 64, column: 8
    S244=0;
    if(i_thread_4 == 7){//motor.sysj line: 67, column: 8
      ends[4]=3;
      ;//motor.sysj line: 65, column: 8
      S244=1;
      LongStep_1.setPresent();//motor.sysj line: 72, column: 8
      currsigs.addElement(LongStep_1);
      System.out.println("Emitted LongStep_1");
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
    else {
      i_thread_4 = i_thread_4 + 1;//motor.sysj line: 68, column: 5
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread22840(int [] tdone, int [] ends){
        S229=1;
    Enable_1.setPresent();//motor.sysj line: 35, column: 8
    currsigs.addElement(Enable_1);
    Enable_1.setValue(false);//motor.sysj line: 35, column: 8
    System.out.println("Emitted Enable_1");
    i_thread_3 = 0;//motor.sysj line: 36, column: 8
    S69=0;
    if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
      ends[3]=5;
      ;//motor.sysj line: 37, column: 8
      if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
        Enable_1.setPresent();//motor.sysj line: 47, column: 8
        currsigs.addElement(Enable_1);
        Enable_1.setValue(false);//motor.sysj line: 47, column: 8
        System.out.println("Emitted Enable_1");
        i_thread_3 = 0;//motor.sysj line: 48, column: 8
        S69=1;
        if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
          ends[3]=4;
          ;//motor.sysj line: 49, column: 8
          if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
            S69=2;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
          }
          else {
            ends[3]=2;
            tdone[3]=1;
          }
        }
        else {
          i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
          active[3]=1;
          ends[3]=1;
          tdone[3]=1;
        }
      }
      else {
        ends[3]=2;
        tdone[3]=1;
      }
    }
    else {
      i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
      active[3]=1;
      ends[3]=1;
      tdone[3]=1;
    }
  }

  public void thread22838(int [] tdone, int [] ends){
        S257=1;
    i_thread_4 = 0;//motor.sysj line: 64, column: 8
    S244=0;
    if(i_thread_4 == 7){//motor.sysj line: 67, column: 8
      ends[4]=3;
      ;//motor.sysj line: 65, column: 8
      S244=1;
      LongStep_1.setPresent();//motor.sysj line: 72, column: 8
      currsigs.addElement(LongStep_1);
      System.out.println("Emitted LongStep_1");
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
    else {
      i_thread_4 = i_thread_4 + 1;//motor.sysj line: 68, column: 5
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread22825(int [] tdone, int [] ends){
        S229=1;
    Enable_1.setPresent();//motor.sysj line: 35, column: 8
    currsigs.addElement(Enable_1);
    Enable_1.setValue(false);//motor.sysj line: 35, column: 8
    System.out.println("Emitted Enable_1");
    i_thread_3 = 0;//motor.sysj line: 36, column: 8
    S69=0;
    if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
      ends[3]=5;
      ;//motor.sysj line: 37, column: 8
      if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
        Enable_1.setPresent();//motor.sysj line: 47, column: 8
        currsigs.addElement(Enable_1);
        Enable_1.setValue(false);//motor.sysj line: 47, column: 8
        System.out.println("Emitted Enable_1");
        i_thread_3 = 0;//motor.sysj line: 48, column: 8
        S69=1;
        if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
          ends[3]=4;
          ;//motor.sysj line: 49, column: 8
          if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
            S69=2;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
          }
          else {
            ends[3]=2;
            tdone[3]=1;
          }
        }
        else {
          i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
          active[3]=1;
          ends[3]=1;
          tdone[3]=1;
        }
      }
      else {
        ends[3]=2;
        tdone[3]=1;
      }
    }
    else {
      i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
      active[3]=1;
      ends[3]=1;
      tdone[3]=1;
    }
  }

  public void thread22823(int [] tdone, int [] ends){
        S257=1;
    i_thread_4 = 0;//motor.sysj line: 64, column: 8
    S244=0;
    if(i_thread_4 == 7){//motor.sysj line: 67, column: 8
      ends[4]=3;
      ;//motor.sysj line: 65, column: 8
      S244=1;
      LongStep_1.setPresent();//motor.sysj line: 72, column: 8
      currsigs.addElement(LongStep_1);
      System.out.println("Emitted LongStep_1");
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
    else {
      i_thread_4 = i_thread_4 + 1;//motor.sysj line: 68, column: 5
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread22810(int [] tdone, int [] ends){
        S229=1;
    Enable_1.setPresent();//motor.sysj line: 35, column: 8
    currsigs.addElement(Enable_1);
    Enable_1.setValue(false);//motor.sysj line: 35, column: 8
    System.out.println("Emitted Enable_1");
    i_thread_3 = 0;//motor.sysj line: 36, column: 8
    S69=0;
    if(i_thread_3 == 1){//motor.sysj line: 39, column: 8
      ends[3]=5;
      ;//motor.sysj line: 37, column: 8
      if(Hold_1.getprestatus()){//motor.sysj line: 44, column: 16
        Enable_1.setPresent();//motor.sysj line: 47, column: 8
        currsigs.addElement(Enable_1);
        Enable_1.setValue(false);//motor.sysj line: 47, column: 8
        System.out.println("Emitted Enable_1");
        i_thread_3 = 0;//motor.sysj line: 48, column: 8
        S69=1;
        if(i_thread_3 == 2){//motor.sysj line: 51, column: 8
          ends[3]=4;
          ;//motor.sysj line: 49, column: 8
          if(Hold_1.getprestatus()){//motor.sysj line: 56, column: 16
            S69=2;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
          }
          else {
            ends[3]=2;
            tdone[3]=1;
          }
        }
        else {
          i_thread_3 = i_thread_3 + 1;//motor.sysj line: 52, column: 5
          active[3]=1;
          ends[3]=1;
          tdone[3]=1;
        }
      }
      else {
        ends[3]=2;
        tdone[3]=1;
      }
    }
    else {
      i_thread_3 = i_thread_3 + 1;//motor.sysj line: 40, column: 5
      active[3]=1;
      ends[3]=1;
      tdone[3]=1;
    }
  }

  public void thread22809(int [] tdone, int [] ends){
        switch(S22407){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        switch(S6){
          case 0 : 
            S6=0;
            S6=1;
            if(LongStep_1.getprestatus()){//motor.sysj line: 11, column: 15
              S37=0;
              i_thread_2 = 0;//motor.sysj line: 12, column: 2
              S8=0;
              if(i_thread_2 == 15){//motor.sysj line: 15, column: 9
                ends[2]=2;
                ;//motor.sysj line: 13, column: 2
                S6=2;
                if(Hold_1.getprestatus()){//motor.sysj line: 31, column: 15
                  thread22810(tdone,ends);
                  thread22823(tdone,ends);
                  int biggest22824 = 0;
                  if(ends[3]>=biggest22824){
                    biggest22824=ends[3];
                  }
                  if(ends[4]>=biggest22824){
                    biggest22824=ends[4];
                  }
                  if(biggest22824 == 1){
                    active[2]=1;
                    ends[2]=1;
                    tdone[2]=1;
                  }
                  if(biggest22824 == 2){
                    ends[2]=2;
                    ;//motor.sysj line: 32, column: 2
                    Step_1.setPresent();//motor.sysj line: 9, column: 7
                    currsigs.addElement(Step_1);
                    System.out.println("Emitted Step_1");
                    S6=0;
                    active[2]=1;
                    ends[2]=1;
                    tdone[2]=1;
                  }
                }
                else {
                  Step_1.setPresent();//motor.sysj line: 9, column: 7
                  currsigs.addElement(Step_1);
                  System.out.println("Emitted Step_1");
                  S6=0;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
              else {
                i_thread_2 = i_thread_2 + 1;//motor.sysj line: 16, column: 6
                S8=1;
                active[2]=1;
                ends[2]=1;
                tdone[2]=1;
              }
            }
            else {
              S37=1;
              i_thread_2 = 0;//motor.sysj line: 22, column: 2
              if(i_thread_2 == 10){//motor.sysj line: 25, column: 9
                ends[2]=2;
                ;//motor.sysj line: 23, column: 2
                S6=2;
                if(Hold_1.getprestatus()){//motor.sysj line: 31, column: 15
                  thread22825(tdone,ends);
                  thread22838(tdone,ends);
                  int biggest22839 = 0;
                  if(ends[3]>=biggest22839){
                    biggest22839=ends[3];
                  }
                  if(ends[4]>=biggest22839){
                    biggest22839=ends[4];
                  }
                  if(biggest22839 == 1){
                    active[2]=1;
                    ends[2]=1;
                    tdone[2]=1;
                  }
                  if(biggest22839 == 2){
                    ends[2]=2;
                    ;//motor.sysj line: 32, column: 2
                    Step_1.setPresent();//motor.sysj line: 9, column: 7
                    currsigs.addElement(Step_1);
                    System.out.println("Emitted Step_1");
                    S6=0;
                    active[2]=1;
                    ends[2]=1;
                    tdone[2]=1;
                  }
                }
                else {
                  Step_1.setPresent();//motor.sysj line: 9, column: 7
                  currsigs.addElement(Step_1);
                  System.out.println("Emitted Step_1");
                  S6=0;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
              else {
                i_thread_2 = i_thread_2 + 1;//motor.sysj line: 26, column: 6
                active[2]=1;
                ends[2]=1;
                tdone[2]=1;
              }
            }
            break;
          
          case 1 : 
            switch(S37){
              case 0 : 
                switch(S8){
                  case 0 : 
                    S8=0;
                    if(i_thread_2 == 15){//motor.sysj line: 15, column: 9
                      ends[2]=2;
                      ;//motor.sysj line: 13, column: 2
                      S6=2;
                      if(Hold_1.getprestatus()){//motor.sysj line: 31, column: 15
                        thread22840(tdone,ends);
                        thread22853(tdone,ends);
                        int biggest22854 = 0;
                        if(ends[3]>=biggest22854){
                          biggest22854=ends[3];
                        }
                        if(ends[4]>=biggest22854){
                          biggest22854=ends[4];
                        }
                        if(biggest22854 == 1){
                          active[2]=1;
                          ends[2]=1;
                          tdone[2]=1;
                        }
                        if(biggest22854 == 2){
                          ends[2]=2;
                          ;//motor.sysj line: 32, column: 2
                          Step_1.setPresent();//motor.sysj line: 9, column: 7
                          currsigs.addElement(Step_1);
                          System.out.println("Emitted Step_1");
                          S6=0;
                          active[2]=1;
                          ends[2]=1;
                          tdone[2]=1;
                        }
                      }
                      else {
                        Step_1.setPresent();//motor.sysj line: 9, column: 7
                        currsigs.addElement(Step_1);
                        System.out.println("Emitted Step_1");
                        S6=0;
                        active[2]=1;
                        ends[2]=1;
                        tdone[2]=1;
                      }
                    }
                    else {
                      i_thread_2 = i_thread_2 + 1;//motor.sysj line: 16, column: 6
                      S8=1;
                      active[2]=1;
                      ends[2]=1;
                      tdone[2]=1;
                    }
                    break;
                  
                  case 1 : 
                    S8=1;
                    S8=0;
                    if(i_thread_2 == 15){//motor.sysj line: 15, column: 9
                      ends[2]=2;
                      ;//motor.sysj line: 13, column: 2
                      S6=2;
                      if(Hold_1.getprestatus()){//motor.sysj line: 31, column: 15
                        thread22855(tdone,ends);
                        thread22868(tdone,ends);
                        int biggest22869 = 0;
                        if(ends[3]>=biggest22869){
                          biggest22869=ends[3];
                        }
                        if(ends[4]>=biggest22869){
                          biggest22869=ends[4];
                        }
                        if(biggest22869 == 1){
                          active[2]=1;
                          ends[2]=1;
                          tdone[2]=1;
                        }
                        if(biggest22869 == 2){
                          ends[2]=2;
                          ;//motor.sysj line: 32, column: 2
                          Step_1.setPresent();//motor.sysj line: 9, column: 7
                          currsigs.addElement(Step_1);
                          System.out.println("Emitted Step_1");
                          S6=0;
                          active[2]=1;
                          ends[2]=1;
                          tdone[2]=1;
                        }
                      }
                      else {
                        Step_1.setPresent();//motor.sysj line: 9, column: 7
                        currsigs.addElement(Step_1);
                        System.out.println("Emitted Step_1");
                        S6=0;
                        active[2]=1;
                        ends[2]=1;
                        tdone[2]=1;
                      }
                    }
                    else {
                      i_thread_2 = i_thread_2 + 1;//motor.sysj line: 16, column: 6
                      S8=1;
                      active[2]=1;
                      ends[2]=1;
                      tdone[2]=1;
                    }
                    break;
                  
                }
                break;
              
              case 1 : 
                if(i_thread_2 == 10){//motor.sysj line: 25, column: 9
                  ends[2]=2;
                  ;//motor.sysj line: 23, column: 2
                  S6=2;
                  if(Hold_1.getprestatus()){//motor.sysj line: 31, column: 15
                    thread22870(tdone,ends);
                    thread22883(tdone,ends);
                    int biggest22884 = 0;
                    if(ends[3]>=biggest22884){
                      biggest22884=ends[3];
                    }
                    if(ends[4]>=biggest22884){
                      biggest22884=ends[4];
                    }
                    if(biggest22884 == 1){
                      active[2]=1;
                      ends[2]=1;
                      tdone[2]=1;
                    }
                    if(biggest22884 == 2){
                      ends[2]=2;
                      ;//motor.sysj line: 32, column: 2
                      Step_1.setPresent();//motor.sysj line: 9, column: 7
                      currsigs.addElement(Step_1);
                      System.out.println("Emitted Step_1");
                      S6=0;
                      active[2]=1;
                      ends[2]=1;
                      tdone[2]=1;
                    }
                  }
                  else {
                    Step_1.setPresent();//motor.sysj line: 9, column: 7
                    currsigs.addElement(Step_1);
                    System.out.println("Emitted Step_1");
                    S6=0;
                    active[2]=1;
                    ends[2]=1;
                    tdone[2]=1;
                  }
                }
                else {
                  i_thread_2 = i_thread_2 + 1;//motor.sysj line: 26, column: 6
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                break;
              
            }
            break;
          
          case 2 : 
            thread22885(tdone,ends);
            thread22904(tdone,ends);
            int biggest22905 = 0;
            if(ends[3]>=biggest22905){
              biggest22905=ends[3];
            }
            if(ends[4]>=biggest22905){
              biggest22905=ends[4];
            }
            if(biggest22905 == 1){
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            if(biggest22905 == 2){
              ends[2]=2;
              ;//motor.sysj line: 32, column: 2
              Step_1.setPresent();//motor.sysj line: 9, column: 7
              currsigs.addElement(Step_1);
              System.out.println("Emitted Step_1");
              S6=0;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            //FINXME code
            if(biggest22905 == 0){
              Step_1.setPresent();//motor.sysj line: 9, column: 7
              currsigs.addElement(Step_1);
              System.out.println("Emitted Step_1");
              S6=0;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread22807(int [] tdone, int [] ends){
        S22801=1;
    i_thread_6 = 0;//motor.sysj line: 99, column: 2
    i_thread_6 = 0;//motor.sysj line: 101, column: 4
    S22454=0;
    i_thread_6 = i_thread_6 + 1;//motor.sysj line: 104, column: 8
    Hold_1.setPresent();//motor.sysj line: 105, column: 8
    currsigs.addElement(Hold_1);
    System.out.println("Emitted Hold_1");
    if(i_thread_6 > 10){//motor.sysj line: 106, column: 11
      ends[6]=2;
      ;//motor.sysj line: 102, column: 4
      i_thread_6 = 0;//motor.sysj line: 112, column: 4
      S22454=1;
      i_thread_6 = i_thread_6 + 1;//motor.sysj line: 115, column: 8
      if(i_thread_6 > 7){//motor.sysj line: 116, column: 11
        ends[6]=2;
        ;//motor.sysj line: 113, column: 4
        S22454=2;
        active[6]=1;
        ends[6]=1;
        tdone[6]=1;
      }
      else {
        active[6]=1;
        ends[6]=1;
        tdone[6]=1;
      }
    }
    else {
      active[6]=1;
      ends[6]=1;
      tdone[6]=1;
    }
  }

  public void thread22806(int [] tdone, int [] ends){
        S22433=1;
    S22409=0;
    active[5]=1;
    ends[5]=1;
    tdone[5]=1;
  }

  public void thread22805(int [] tdone, int [] ends){
        S22407=1;
    Step_1.setPresent();//motor.sysj line: 9, column: 7
    currsigs.addElement(Step_1);
    System.out.println("Emitted Step_1");
    S6=0;
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S22803){
        case 0 : 
          S22803=0;
          break RUN;
        
        case 1 : 
          S22803=2;
          S22803=2;
          Step_1.setClear();//motor.sysj line: 2, column: 3
          Hold_1.setClear();//motor.sysj line: 2, column: 3
          Enable_1.setClear();//motor.sysj line: 2, column: 3
          Coil1_1.setClear();//motor.sysj line: 3, column: 3
          Coil2_1.setClear();//motor.sysj line: 3, column: 3
          LongStep_1.setClear();//motor.sysj line: 4, column: 3
          thread22805(tdone,ends);
          thread22806(tdone,ends);
          thread22807(tdone,ends);
          int biggest22808 = 0;
          if(ends[2]>=biggest22808){
            biggest22808=ends[2];
          }
          if(ends[5]>=biggest22808){
            biggest22808=ends[5];
          }
          if(ends[6]>=biggest22808){
            biggest22808=ends[6];
          }
          if(biggest22808 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          Step_1.setClear();//motor.sysj line: 2, column: 3
          Hold_1.setClear();//motor.sysj line: 2, column: 3
          Enable_1.setClear();//motor.sysj line: 2, column: 3
          Coil1_1.setClear();//motor.sysj line: 3, column: 3
          Coil2_1.setClear();//motor.sysj line: 3, column: 3
          LongStep_1.setClear();//motor.sysj line: 4, column: 3
          thread22809(tdone,ends);
          thread22906(tdone,ends);
          thread22931(tdone,ends);
          int biggest22932 = 0;
          if(ends[2]>=biggest22932){
            biggest22932=ends[2];
          }
          if(ends[5]>=biggest22932){
            biggest22932=ends[5];
          }
          if(ends[6]>=biggest22932){
            biggest22932=ends[6];
          }
          if(biggest22932 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          //FINXME code
          if(biggest22932 == 0){
            S22803=0;
            active[1]=0;
            ends[1]=0;
            S22803=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    Step_1 = new Signal();
    Hold_1 = new Signal();
    Enable_1 = new Signal();
    Coil1_1 = new Signal();
    Coil2_1 = new Signal();
    LongStep_1 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        if(!df){
          df = true;
        }
        runClockDomain();
      }
      Step_1.setpreclear();
      Hold_1.setpreclear();
      Enable_1.setpreclear();
      Coil1_1.setpreclear();
      Coil2_1.setpreclear();
      LongStep_1.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      Step_1.setClear();
      Hold_1.setClear();
      Enable_1.setClear();
      Coil1_1.setClear();
      Coil2_1.setClear();
      LongStep_1.setClear();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
