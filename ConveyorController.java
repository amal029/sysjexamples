import java.util.*;
import systemj.bootstrap.ClockDomain;
import systemj.lib.*;
import camera.*;//sorter1pic.sysj line: 1, column: 1

public class ConveyorController extends ClockDomain{
  public ConveyorController(){super(); init();}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  public Signal NEWITEM = new Signal();
  public Signal ITEMPICKED = new Signal();
  public Signal ITEMRECOGNIZED = new Signal();
  public Signal STOPLOADER = new Signal();
  private Signal picture_1;
  private Signal edgepicture_1;
  private int load_thread_2;//sorter1pic.sysj line: 8, column: 5
  private int CAPACITY_thread_2;//sorter1pic.sysj line: 9, column: 5
  private Image imm_thread_4;//sorter1pic.sysj line: 24, column: 4
  private Image imm2_thread_4;//sorter1pic.sysj line: 25, column: 4
  private String picc_thread_5;//sorter1pic.sysj line: 29, column: 4
  private String picc2_thread_5;//sorter1pic.sysj line: 30, column: 4
  private String picc_thread_6;//sorter1pic.sysj line: 34, column: 4
  private String picc2_thread_6;//sorter1pic.sysj line: 35, column: 4
  private int S125 = 1;
  private int S61 = 1;
  private int S5 = 1;
  private int S123 = 1;
  private int S81 = 1;
  private int S69 = 1;
  private int S72 = 1;
  
  private int[] ends = new int[7];
  private int[] tdone = new int[7];
  
  public void thread150(int [] tdone, int [] ends){
        switch(S72){
      case 0 : 
        active[6]=0;
        ends[6]=0;
        tdone[6]=1;
        break;
      
      case 1 : 
        if(edgepicture_1.getprestatus()){//sorter1pic.sysj line: 33, column: 11
          picc_thread_6 = ((Image)edgepicture_1.getpreval()).get_item_type();//sorter1pic.sysj line: 34, column: 4
          picc2_thread_6 = new String(picc_thread_6);//sorter1pic.sysj line: 35, column: 4
          ITEMRECOGNIZED.setPresent();//sorter1pic.sysj line: 36, column: 4
          currsigs.addElement(ITEMRECOGNIZED);
          ITEMRECOGNIZED.setValue(picc2_thread_6);//sorter1pic.sysj line: 36, column: 4
          System.out.println("Emitted ITEMRECOGNIZED");
          S72=0;
          active[6]=0;
          ends[6]=0;
          tdone[6]=1;
        }
        else {
          active[6]=1;
          ends[6]=1;
          tdone[6]=1;
        }
        break;
      
    }
  }

  public void thread143(int [] tdone, int [] ends){
        switch(S69){
      case 0 : 
        active[5]=0;
        ends[5]=0;
        tdone[5]=1;
        break;
      
      case 1 : 
        if(picture_1.getprestatus()){//sorter1pic.sysj line: 28, column: 11
          picc_thread_5 = (String)Process_Image.edge_detection((Image)picture_1.getpreval());//sorter1pic.sysj line: 29, column: 4
          picc2_thread_5 = new String(picc_thread_5);//sorter1pic.sysj line: 30, column: 4
          edgepicture_1.setPresent();//sorter1pic.sysj line: 31, column: 4
          currsigs.addElement(edgepicture_1);
          edgepicture_1.setValue(picc2_thread_5);//sorter1pic.sysj line: 31, column: 4
          System.out.println("Emitted edgepicture_1");
          S69=0;
          active[5]=0;
          ends[5]=0;
          tdone[5]=1;
        }
        else {
          active[5]=1;
          ends[5]=1;
          tdone[5]=1;
        }
        break;
      
    }
  }

  public void thread142(int [] tdone, int [] ends){
        active[4]=0;
    ends[4]=0;
    tdone[4]=1;
  }

  public void thread140(int [] tdone, int [] ends){
        S72=1;
    active[6]=1;
    ends[6]=1;
    tdone[6]=1;
  }

  public void thread139(int [] tdone, int [] ends){
        S69=1;
    active[5]=1;
    ends[5]=1;
    tdone[5]=1;
  }

  public void thread132(int [] tdone, int [] ends){
        imm_thread_4 = Camera.takepicture();//sorter1pic.sysj line: 24, column: 4
    imm2_thread_4 = new Image(imm_thread_4);//sorter1pic.sysj line: 25, column: 4
    picture_1.setPresent();//sorter1pic.sysj line: 26, column: 4
    currsigs.addElement(picture_1);
    picture_1.setValue(imm2_thread_4);//sorter1pic.sysj line: 26, column: 4
    System.out.println("Emitted picture_1");
    active[4]=0;
    ends[4]=0;
    tdone[4]=1;
  }

  public void thread131(int [] tdone, int [] ends){
        switch(S123){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        picture_1.setClear();//sorter1pic.sysj line: 21, column: 2
        edgepicture_1.setClear();//sorter1pic.sysj line: 21, column: 2
        switch(S81){
          case 0 : 
            if(NEWITEM.getprestatus()){//sorter1pic.sysj line: 22, column: 9
              S81=1;
              thread132(tdone,ends);
              thread139(tdone,ends);
              thread140(tdone,ends);
              int biggest141 = 0;
              if(ends[4]>=biggest141){
                biggest141=ends[4];
              }
              if(ends[5]>=biggest141){
                biggest141=ends[5];
              }
              if(ends[6]>=biggest141){
                biggest141=ends[6];
              }
              if(biggest141 == 1){
                active[3]=1;
                ends[3]=1;
                tdone[3]=1;
              }
            }
            else {
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            break;
          
          case 1 : 
            thread142(tdone,ends);
            thread143(tdone,ends);
            thread150(tdone,ends);
            int biggest157 = 0;
            if(ends[4]>=biggest157){
              biggest157=ends[4];
            }
            if(ends[5]>=biggest157){
              biggest157=ends[5];
            }
            if(ends[6]>=biggest157){
              biggest157=ends[6];
            }
            if(biggest157 == 1){
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            //FINXME code
            if(biggest157 == 0){
              S81=2;
              active[3]=1;
              ends[3]=1;
              tdone[3]=1;
            }
            break;
          
          case 2 : 
            S81=2;
            picture_1.setClear();//sorter1pic.sysj line: 21, column: 2
            edgepicture_1.setClear();//sorter1pic.sysj line: 21, column: 2
            S81=0;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread130(int [] tdone, int [] ends){
        switch(S61){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        switch(S5){
          case 0 : 
            S5=0;
            if(NEWITEM.getprestatus()){//sorter1pic.sysj line: 11, column: 15
              load_thread_2 = load_thread_2 + 1;//sorter1pic.sysj line: 11, column: 24
              if(ITEMPICKED.getprestatus()){//sorter1pic.sysj line: 12, column: 15
                load_thread_2 = load_thread_2 - 1;//sorter1pic.sysj line: 12, column: 27
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
              else {
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
            }
            else {
              if(ITEMPICKED.getprestatus()){//sorter1pic.sysj line: 12, column: 15
                load_thread_2 = load_thread_2 - 1;//sorter1pic.sysj line: 12, column: 27
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
              else {
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
            }
            break;
          
          case 1 : 
            S5=1;
            S5=0;
            if(NEWITEM.getprestatus()){//sorter1pic.sysj line: 11, column: 15
              load_thread_2 = load_thread_2 + 1;//sorter1pic.sysj line: 11, column: 24
              if(ITEMPICKED.getprestatus()){//sorter1pic.sysj line: 12, column: 15
                load_thread_2 = load_thread_2 - 1;//sorter1pic.sysj line: 12, column: 27
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
              else {
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
            }
            else {
              if(ITEMPICKED.getprestatus()){//sorter1pic.sysj line: 12, column: 15
                load_thread_2 = load_thread_2 - 1;//sorter1pic.sysj line: 12, column: 27
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
              else {
                if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
                  STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
                  currsigs.addElement(STOPLOADER);
                  System.out.println("Emitted STOPLOADER");
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
                else {
                  S5=1;
                  active[2]=1;
                  ends[2]=1;
                  tdone[2]=1;
                }
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread128(int [] tdone, int [] ends){
        S123=1;
    picture_1.setClear();//sorter1pic.sysj line: 21, column: 2
    edgepicture_1.setClear();//sorter1pic.sysj line: 21, column: 2
    S81=0;
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread127(int [] tdone, int [] ends){
        S61=1;
    load_thread_2 = 0;//sorter1pic.sysj line: 8, column: 5
    CAPACITY_thread_2 = 10;//sorter1pic.sysj line: 9, column: 5
    S5=0;
    if(NEWITEM.getprestatus()){//sorter1pic.sysj line: 11, column: 15
      load_thread_2 = load_thread_2 + 1;//sorter1pic.sysj line: 11, column: 24
      if(ITEMPICKED.getprestatus()){//sorter1pic.sysj line: 12, column: 15
        load_thread_2 = load_thread_2 - 1;//sorter1pic.sysj line: 12, column: 27
        if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
          STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
          currsigs.addElement(STOPLOADER);
          System.out.println("Emitted STOPLOADER");
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
        else {
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
      }
      else {
        if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
          STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
          currsigs.addElement(STOPLOADER);
          System.out.println("Emitted STOPLOADER");
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
        else {
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
      }
    }
    else {
      if(ITEMPICKED.getprestatus()){//sorter1pic.sysj line: 12, column: 15
        load_thread_2 = load_thread_2 - 1;//sorter1pic.sysj line: 12, column: 27
        if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
          STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
          currsigs.addElement(STOPLOADER);
          System.out.println("Emitted STOPLOADER");
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
        else {
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
      }
      else {
        if(load_thread_2 >= CAPACITY_thread_2){//sorter1pic.sysj line: 13, column: 11
          STOPLOADER.setPresent();//sorter1pic.sysj line: 14, column: 2
          currsigs.addElement(STOPLOADER);
          System.out.println("Emitted STOPLOADER");
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
        else {
          S5=1;
          active[2]=1;
          ends[2]=1;
          tdone[2]=1;
        }
      }
    }
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S125){
        case 0 : 
          S125=0;
          break RUN;
        
        case 1 : 
          S125=2;
          S125=2;
          thread127(tdone,ends);
          thread128(tdone,ends);
          int biggest129 = 0;
          if(ends[2]>=biggest129){
            biggest129=ends[2];
          }
          if(ends[3]>=biggest129){
            biggest129=ends[3];
          }
          if(biggest129 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          thread130(tdone,ends);
          thread131(tdone,ends);
          int biggest158 = 0;
          if(ends[2]>=biggest158){
            biggest158=ends[2];
          }
          if(ends[3]>=biggest158){
            biggest158=ends[3];
          }
          if(biggest158 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          //FINXME code
          if(biggest158 == 0){
            S125=0;
            active[1]=0;
            ends[1]=0;
            S125=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    picture_1 = new Signal();
    edgepicture_1 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        if(!df){
          NEWITEM.gethook();
          ITEMPICKED.gethook();
          df = true;
        }
        runClockDomain();
      }
      NEWITEM.setpreclear();
      ITEMPICKED.setpreclear();
      ITEMRECOGNIZED.setpreclear();
      STOPLOADER.setpreclear();
      picture_1.setpreclear();
      edgepicture_1.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      dummyint = NEWITEM.getStatus() ? NEWITEM.setprepresent() : NEWITEM.setpreclear();
      NEWITEM.setpreval(NEWITEM.getValue());
      NEWITEM.setClear();
      dummyint = ITEMPICKED.getStatus() ? ITEMPICKED.setprepresent() : ITEMPICKED.setpreclear();
      ITEMPICKED.setpreval(ITEMPICKED.getValue());
      ITEMPICKED.setClear();
      ITEMRECOGNIZED.sethook();
      ITEMRECOGNIZED.setClear();
      STOPLOADER.sethook();
      STOPLOADER.setClear();
      picture_1.setClear();
      edgepicture_1.setClear();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        NEWITEM.gethook();
        ITEMPICKED.gethook();
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
