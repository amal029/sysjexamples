package tpjop_freyoptimizedmp0;
import java.util.*;
import java.io.*;
import com.jopdesign.sys.Const;
import com.jopdesign.sys.Native;
import systemj.lib.emb.*;
import Frq.*;//tpjop_frey.sysj line: 1, column: 1

public class tpjop_freyomp{
  private static boolean retval = false;
  private static int dl = 0;
  private static Vector currsigs0 = new Vector();
  private static Vector currsigs1 = new Vector();
  private static input_Channel SampleCount_in;
  private static output_Channel SampleCount_o;
  private static Signal SampleLink_1;
  private static Signal SymResult_3;
  private static Signal AveResult_3;
  private static Signal FreqStatLink_7;
  private static Signal RocStatLink_7;
  private static Signal switch1Link_7;
  private static Signal switch2Link_7;
  private static Signal switch3Link_7;
  private static Signal ThreshValueLink_7;
  private static Signal ThreshCodeLink_7;
  private static Signal CheckStatLink_7;
  private static Signal cancelLink_7;
  private static Signal doneLink_7;
  private static Signal skipLink_7;
  private static Signal InputPasswordLink_7;
  private static Signal WrongPasswordLink_7;
  private static Signal StartLink_7;
  private static Signal passwordLink_7;
  private static Signal Frths_7;
  private static Signal AveFreqval_8;
  private static Signal meCount_8;
  private static Signal timeout_12;
  private static Signal start2_12;
  private static Signal next_12;
  private static Signal ep_12;
  private static Signal pp_12;
  private static Signal Enter_12;
  private static Signal ic_12;
  private static Signal it_12;
  private static int i_thread_2;//tpjop_frey.sysj line: 96, column: 3
  private static int val_thread_2;//tpjop_frey.sysj line: 97, column: 3
  private static CalculateSample cs_thread_2;//tpjop_frey.sysj line: 98, column: 3
  private static int turn_thread_2;//tpjop_frey.sysj line: 99, column: 3
  private static int AveIndex_thread_4;//tpjop_frey.sysj line: 16, column: 5
  private static int AveSum_thread_4;//tpjop_frey.sysj line: 17, column: 5
  private static int AveWs_thread_4;//tpjop_frey.sysj line: 18, column: 5
  private static float[] AveWindow_thread_4;//tpjop_frey.sysj line: 19, column: 5
  private static int L_thread_5;//tpjop_frey.sysj line: 38, column: 7
  private static float x1_thread_5;//tpjop_frey.sysj line: 37, column: 5
  private static float x2_thread_5;//tpjop_frey.sysj line: 37, column: 5
  private static int HalfSymWs_thread_5;//tpjop_frey.sysj line: 40, column: 7
  private static float CorResult_thread_5;//tpjop_frey.sysj line: 41, column: 7
  private static int SymIndex_thread_5;//tpjop_frey.sysj line: 42, column: 7
  private static int SymWs_thread_5;//tpjop_frey.sysj line: 43, column: 7
  private static float[] SymWindow_thread_5;//tpjop_frey.sysj line: 44, column: 7
  private static int CorIndex_thread_5;//tpjop_frey.sysj line: 45, column: 7
  private static float first_thread_6;//tpjop_frey.sysj line: 64, column: 7
  private static float second_thread_6;//tpjop_frey.sysj line: 64, column: 7
  private static float third_thread_6;//tpjop_frey.sysj line: 64, column: 7
  private static boolean FirstPeak_thread_6;//tpjop_frey.sysj line: 66, column: 2
  private static int SampleCountVar_thread_6;//tpjop_frey.sysj line: 67, column: 2
  private static int SkipSamples_thread_6;//tpjop_frey.sysj line: 68, column: 2
  private static int u_thread_2;//tpjop_frey.sysj line: 102, column: 5
  private static float sample_thread_2;//tpjop_frey.sysj line: 139, column: 3
  private static float sum_thread_4;//tpjop_frey.sysj line: 25, column: 7
  private static int j_thread_4;//tpjop_frey.sysj line: 26, column: 7
  private static int tyi_thread_6;//tpjop_frey.sysj line: 82, column: 3
  private static int tutu__492;//tpjop_frey.sysj line: 83, column: 3
  private static int tutu__493;//tpjop_frey.sysj line: 83, column: 3
  private static int tutu__310;//tpjop_frey.sysj line: 83, column: 3
  private static int tutu__311;//tpjop_frey.sysj line: 83, column: 3
  private static int tutu__7544;//tpjop_frey.sysj line: 318, column: 7
  private static Float vall_thread_9;//tpjop_frey.sysj line: 319, column: 7
  private static int tutu__7545;//tpjop_frey.sysj line: 318, column: 7
  private static float dft0_thread_10;//tpjop_frey.sysj line: 325, column: 5
  private static float dft1_thread_10;//tpjop_frey.sysj line: 325, column: 5
  private static float dft2_thread_10;//tpjop_frey.sysj line: 325, column: 5
  private static float Sum_thread_10;//tpjop_frey.sysj line: 325, column: 5
  private static int FreqWs_thread_10;//tpjop_frey.sysj line: 327, column: 7
  private static float SamplingFreq_thread_10;//tpjop_frey.sysj line: 328, column: 7
  private static float[] FreqWindow_thread_10;//tpjop_frey.sysj line: 329, column: 7
  private static float Freq_thread_10;//tpjop_frey.sysj line: 333, column: 7
  private static int ghy_thread_10;//tpjop_frey.sysj line: 337, column: 2
  private static float drt0_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float drt1_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float drt2_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static int RocWs_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static int j34_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float[] RocWindow_thread_11;//tpjop_frey.sysj line: 383, column: 2
  private static float firstC_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float secondC_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float temp_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float Roc_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float AveRoc_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static float RocSum_thread_11;//tpjop_frey.sysj line: 380, column: 7
  private static String CurrentState_thread_13;//tpjop_frey.sysj line: 447, column: 5
  private static int enter_thread_13;//tpjop_frey.sysj line: 448, column: 5
  private static int i1_thread_17;//tpjop_frey.sysj line: 563, column: 7
  private static int PrevSwitch1_thread_18;//tpjop_frey.sysj line: 263, column: 3
  private static int PrevSwitch2_thread_18;//tpjop_frey.sysj line: 264, column: 3
  private static int PrevSwitch3_thread_18;//tpjop_frey.sysj line: 265, column: 3
  private static int i_thread_19;//tpjop_frey.sysj line: 146, column: 1
  private static int d1_thread_19;//tpjop_frey.sysj line: 146, column: 1
  private static String d2_thread_19;//tpjop_frey.sysj line: 155, column: 3
  private static boolean[] UpdateFlags_thread_19;//tpjop_frey.sysj line: 156, column: 3
  private static String CorrectPassword_thread_19;//tpjop_frey.sysj line: 157, column: 3
  private static int[] ThreshVars_thread_19;//tpjop_frey.sysj line: 158, column: 3
  private static Vector frths_thread_19;//tpjop_frey.sysj line: 159, column: 3
  private static int i_thread_20;//tpjop_frey.sysj line: 224, column: 3
  private static String PassValue_thread_20;//tpjop_frey.sysj line: 225, column: 3
  private static int tutu__7208;//tpjop_frey.sysj line: 318, column: 7
  private static int tutu__7209;//tpjop_frey.sysj line: 318, column: 7
  private static int tutu__7240;//tpjop_frey.sysj line: 318, column: 7
  private static int tutu__7241;//tpjop_frey.sysj line: 318, column: 7
  private static int i34_thread_10;//tpjop_frey.sysj line: 334, column: 7
  private static int j34_thread_10;//tpjop_frey.sysj line: 334, column: 7
  private static float toe_thread_10;//tpjop_frey.sysj line: 354, column: 2
  private static int drt_thread_14;//tpjop_frey.sysj line: 463, column: 7
  private static int dft_thread_14;//tpjop_frey.sysj line: 463, column: 7
  private static int enter_thread_14;//tpjop_frey.sysj line: 463, column: 7
  private static String sdft_thread_14;//tpjop_frey.sysj line: 472, column: 8
  private static String sdrt_thread_14;//tpjop_frey.sysj line: 477, column: 3
  private static int drt_thread_15;//tpjop_frey.sysj line: 497, column: 2
  private static int dft_thread_15;//tpjop_frey.sysj line: 497, column: 2
  private static int enter_thread_15;//tpjop_frey.sysj line: 497, column: 2
  private static String changerval_thread_15;//tpjop_frey.sysj line: 521, column: 3
  private static Integer ichan_thread_15;//tpjop_frey.sysj line: 523, column: 3
  private static int dummy1_thread_18;//tpjop_frey.sysj line: 266, column: 3
  private static int dummy2_thread_18;//tpjop_frey.sysj line: 266, column: 3
  private static int dummy3_thread_18;//tpjop_frey.sysj line: 266, column: 3
  private static int tu_thread_19;//tpjop_frey.sysj line: 199, column: 3
  private static int y_thread_20;//tpjop_frey.sysj line: 245, column: 5
  private static int u67_thread_20;//tpjop_frey.sysj line: 252, column: 5
  public static void main(String args[]){
    SampleLink_1 = new Signal();
    SymResult_3 = new Signal();
    AveResult_3 = new Signal();
    FreqStatLink_7 = new Signal();
    RocStatLink_7 = new Signal();
    switch1Link_7 = new Signal();
    switch2Link_7 = new Signal();
    switch3Link_7 = new Signal();
    ThreshValueLink_7 = new Signal();
    ThreshCodeLink_7 = new Signal();
    CheckStatLink_7 = new Signal();
    cancelLink_7 = new Signal();
    doneLink_7 = new Signal();
    skipLink_7 = new Signal();
    InputPasswordLink_7 = new Signal();
    WrongPasswordLink_7 = new Signal();
    StartLink_7 = new Signal();
    passwordLink_7 = new Signal();
    Frths_7 = new Signal();
    AveFreqval_8 = new Signal();
    meCount_8 = new Signal();
    timeout_12 = new Signal();
    start2_12 = new Signal();
    next_12 = new Signal();
    ep_12 = new Signal();
    pp_12 = new Signal();
    Enter_12 = new Signal();
    ic_12 = new Signal();
    it_12 = new Signal();
    SampleCount_o = new output_Channel();
    SampleCount_in = new input_Channel();
    SampleCount_in.set_partner(SampleCount_o);
    SampleCount_o.set_partner(SampleCount_in);
    SampleCount_in.setInit();
    SampleCount_o.setInit();
    tpjop_freyomp t_procs = new tpjop_freyomp();
        while(true){
    int ret = 0;
    boolean ret_bool = false;
    int data_call_word=Native.rd(Const.DPCR_ADDR);
    if(((data_call_word >> 31)&0x001) > 0){
    int case_number = (data_call_word>>16) & 0x7FFF;
    int clock_domain = data_call_word & 0xF;
    if(clock_domain == 0){
    ret_bool = t_procs.cbackcall0_0(case_number);
    }
    else if(clock_domain == 1){
    ret_bool = t_procs.cbackcall1_0(case_number);
    }
    else throw new RuntimeException();
    }
    int dprr = ret_bool ? 0x0003 : 0x0002;
    Native.wr(dprr, (Const.RESULT_ADDR));
    }
    }
    
        private boolean cbackcall0_0(int var){
    boolean retval = false;
        if(var == 1){
    retval = MethodCall1_0();
    }
    else if(var == 2){
    retval = MethodCall2_0();
    }
    else if(var == 3){
    retval = MethodCall3_0();
    }
    else if(var == 4){
    retval = MethodCall4_0();
    }
    else if(var == 5){
    retval = MethodCall5_0();
    }
    else if(var == 6){
    retval = MethodCall6_0();
    }
    else if(var == 7){
    retval = MethodCall7_0();
    }
    else if(var == 8){
    retval = MethodCall8_0();
    }
    else if(var == 9){
    retval = MethodCall9_0();
    }
    else if(var == 10){
    retval = MethodCall10_0();
    }
    else if(var == 11){
    retval = MethodCall11_0();
    }
    else if(var == 12){
    retval = MethodCall12_0();
    }
    else if(var == 13){
    retval = MethodCall13_0();
    }
    else if(var == 14){
    retval = MethodCall14_0();
    }
    else if(var == 15){
    retval = MethodCall15_0();
    }
    else if(var == 16){
    retval = MethodCall16_0();
    }
    else if(var == 17){
    retval = MethodCall17_0();
    }
    else if(var == 18){
    retval = MethodCall18_0();
    }
    else if(var == 19){
    retval = MethodCall19_0();
    }
    else if(var == 20){
    retval = MethodCall20_0();
    }
    else if(var == 21){
    retval = MethodCall21_0();
    }
    else if(var == 22){
    retval = MethodCall22_0();
    }
    else if(var == 23){
    retval = MethodCall23_0();
    }
    else if(var == 24){
    retval = MethodCall24_0();
    }
    else if(var == 25){
    retval = MethodCall25_0();
    }
    else if(var == 26){
    retval = MethodCall26_0();
    }
    else if(var == 27){
    retval = MethodCall27_0();
    }
    else if(var == 28){
    retval = MethodCall28_0();
    }
    else if(var == 29){
    retval = MethodCall29_0();
    }
    else if(var == 30){
    retval = MethodCall30_0();
    }
    else if(var == 31){
    retval = MethodCall31_0();
    }
    else if(var == 32){
    retval = MethodCall32_0();
    }
    else if(var == 33){
    retval = MethodCall33_0();
    }
    else if(var == 34){
    retval = MethodCall34_0();
    }
    else if(var == 35){
    retval = MethodCall35_0();
    }
    else if(var == 36){
    retval = MethodCall36_0();
    }
    else if(var == 37){
    retval = MethodCall37_0();
    }
    else if(var == 38){
    retval = MethodCall38_0();
    }
    else if(var == 39){
    retval = MethodCall39_0();
    }
    else if(var == 40){
    retval = MethodCall40_0();
    }
    else if(var == 41){
    retval = MethodCall41_0();
    }
    else if(var == 42){
    retval = MethodCall42_0();
    }
    else if(var == 43){
    retval = MethodCall43_0();
    }

    else if(var == 0){ 
for(int qw=0;qw<currsigs0.size();++qw){
            int rand496563282 = ((Signal)currsigs0.elementAt(qw)).getStatus() ? ((Signal)currsigs0.elementAt(qw)).setprepresent() : ((Signal)currsigs0.elementAt(qw)).setpreclear();
			((Signal)currsigs0.elementAt(qw)).setpreval(((Signal)currsigs0.elementAt(qw)).getValue());
			}
    currsigs0.removeAllElements();
        SampleCount_o.update_w_r();
    SampleCount_o.gethook();
SampleCount_o.sethook();
        }
    else throw new RuntimeException("CaseNum: "+var+" not found");
    return retval;
    }

        public boolean MethodCall1_0(){
i_thread_2 = 15999;//tpjop_frey.sysj line: 96, column: 3
val_thread_2 = 320;//tpjop_frey.sysj line: 97, column: 3
cs_thread_2 = new CalculateSample();//tpjop_frey.sysj line: 98, column: 3
turn_thread_2 = 0;//tpjop_frey.sysj line: 99, column: 3
    return false;
    }
    public boolean MethodCall2_0(){
currsigs0.addElement(SymResult_3);
    SymResult_3.setValue(new Float(0.0));
currsigs0.addElement(AveResult_3);
    AveResult_3.setValue(new Float(0.0));
    return false;
    }
    public boolean MethodCall3_0(){
AveIndex_thread_4 = -1;//tpjop_frey.sysj line: 16, column: 5
AveSum_thread_4 = 0;//tpjop_frey.sysj line: 17, column: 5
AveWs_thread_4 = 20;//tpjop_frey.sysj line: 18, column: 5
AveWindow_thread_4 = new float[AveWs_thread_4];//tpjop_frey.sysj line: 19, column: 5
    return false;
    }
    public boolean MethodCall4_0(){
L_thread_5 = 10;//tpjop_frey.sysj line: 38, column: 7
x1_thread_5 = 0;//tpjop_frey.sysj line: 37, column: 5
x2_thread_5 = 0;//tpjop_frey.sysj line: 37, column: 5
HalfSymWs_thread_5 = 80;//tpjop_frey.sysj line: 40, column: 7
CorResult_thread_5 = 0;//tpjop_frey.sysj line: 41, column: 7
SymIndex_thread_5 = -1;//tpjop_frey.sysj line: 42, column: 7
SymWs_thread_5 = 161;//tpjop_frey.sysj line: 43, column: 7
SymWindow_thread_5 = new float[SymWs_thread_5];//tpjop_frey.sysj line: 44, column: 7
CorIndex_thread_5 = 0;//tpjop_frey.sysj line: 45, column: 7
    return false;
    }
    public boolean MethodCall5_0(){
first_thread_6 = 10000;//tpjop_frey.sysj line: 64, column: 7
second_thread_6 = 10000;//tpjop_frey.sysj line: 64, column: 7
third_thread_6 = 10000;//tpjop_frey.sysj line: 64, column: 7
FirstPeak_thread_6 = true;//tpjop_frey.sysj line: 66, column: 2
SampleCountVar_thread_6 = 0;//tpjop_frey.sysj line: 67, column: 2
SkipSamples_thread_6 = 363;//tpjop_frey.sysj line: 68, column: 2
    return false;
    }
    public boolean MethodCall6_0(){
u_thread_2 = 0;//tpjop_frey.sysj line: 102, column: 5
    return false;
    }
    public boolean MethodCall7_0(){
return u_thread_2 == i_thread_2;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall8_0(){
u_thread_2 = 0;//tpjop_frey.sysj line: 106, column: 5
turn_thread_2 = turn_thread_2 + 1;//tpjop_frey.sysj line: 107, column: 5
if(turn_thread_2 == 1) {//tpjop_frey.sysj line: 108, column: 18
      i_thread_2 = 16499;//tpjop_frey.sysj line: 109, column: 7
      val_thread_2 = 330;//tpjop_frey.sysj line: 110, column: 7
    }
    else {//tpjop_frey.sysj line: 112, column: 9
      if(turn_thread_2 == 2) {//tpjop_frey.sysj line: 113, column: 20
        i_thread_2 = 16999;//tpjop_frey.sysj line: 114, column: 9
        val_thread_2 = 340;//tpjop_frey.sysj line: 115, column: 9
      }
      else {//tpjop_frey.sysj line: 117, column: 11
        if(turn_thread_2 == 3) {//tpjop_frey.sysj line: 118, column: 22
          i_thread_2 = 17499;//tpjop_frey.sysj line: 119, column: 4
          val_thread_2 = 350;//tpjop_frey.sysj line: 120, column: 4
        }
        else {//tpjop_frey.sysj line: 122, column: 13
          if(turn_thread_2 == 0) {//tpjop_frey.sysj line: 123, column: 17
            i_thread_2 = 15999;//tpjop_frey.sysj line: 124, column: 6
            val_thread_2 = 320;//tpjop_frey.sysj line: 125, column: 6
          }
        }
      }
    }
    return false;
    }
    public boolean MethodCall9_0(){
;//tpjop_frey.sysj line: 103, column: 5
    return false;
    }
    public boolean MethodCall10_0(){
u_thread_2 = u_thread_2 + 1;//tpjop_frey.sysj line: 133, column: 5
if(u_thread_2 == i_thread_2 && turn_thread_2 == 3) {//tpjop_frey.sysj line: 134, column: 28
      turn_thread_2 = -1;//tpjop_frey.sysj line: 135, column: 7
    }
    return false;
    }
    public boolean MethodCall11_0(){
sample_thread_2 = cs_thread_2.compute(u_thread_2, val_thread_2);//tpjop_frey.sysj line: 139, column: 3
currsigs0.addElement(SampleLink_1);
    SampleLink_1.setValue(new Float(sample_thread_2));
    return false;
    }
    public boolean MethodCall12_0(){
return u_thread_2 == i_thread_2;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall13_0(){
u_thread_2 = 0;//tpjop_frey.sysj line: 106, column: 5
turn_thread_2 = turn_thread_2 + 1;//tpjop_frey.sysj line: 107, column: 5
if(turn_thread_2 == 1) {//tpjop_frey.sysj line: 108, column: 18
      i_thread_2 = 16499;//tpjop_frey.sysj line: 109, column: 7
      val_thread_2 = 330;//tpjop_frey.sysj line: 110, column: 7
    }
    else {//tpjop_frey.sysj line: 112, column: 9
      if(turn_thread_2 == 2) {//tpjop_frey.sysj line: 113, column: 20
        i_thread_2 = 16999;//tpjop_frey.sysj line: 114, column: 9
        val_thread_2 = 340;//tpjop_frey.sysj line: 115, column: 9
      }
      else {//tpjop_frey.sysj line: 117, column: 11
        if(turn_thread_2 == 3) {//tpjop_frey.sysj line: 118, column: 22
          i_thread_2 = 17499;//tpjop_frey.sysj line: 119, column: 4
          val_thread_2 = 350;//tpjop_frey.sysj line: 120, column: 4
        }
        else {//tpjop_frey.sysj line: 122, column: 13
          if(turn_thread_2 == 0) {//tpjop_frey.sysj line: 123, column: 17
            i_thread_2 = 15999;//tpjop_frey.sysj line: 124, column: 6
            val_thread_2 = 320;//tpjop_frey.sysj line: 125, column: 6
          }
        }
      }
    }
    return false;
    }
    public boolean MethodCall14_0(){
;//tpjop_frey.sysj line: 103, column: 5
    return false;
    }
    public boolean MethodCall15_0(){
u_thread_2 = u_thread_2 + 1;//tpjop_frey.sysj line: 133, column: 5
if(u_thread_2 == i_thread_2 && turn_thread_2 == 3) {//tpjop_frey.sysj line: 134, column: 28
      turn_thread_2 = -1;//tpjop_frey.sysj line: 135, column: 7
    }
    return false;
    }
    public boolean MethodCall16_0(){
AveIndex_thread_4 = (AveIndex_thread_4 + 1) % AveWs_thread_4;//tpjop_frey.sysj line: 22, column: 7
AveWindow_thread_4[AveIndex_thread_4] = ((Float)SampleLink_1.getpreval()).floatValue();//tpjop_frey.sysj line: 23, column: 7
AveSum_thread_4 = 0;//tpjop_frey.sysj line: 24, column: 7
sum_thread_4 = 0;//tpjop_frey.sysj line: 25, column: 7
j_thread_4 = 0;//tpjop_frey.sysj line: 26, column: 7
for(j_thread_4 = 0; j_thread_4 < AveWs_thread_4; j_thread_4 = j_thread_4 + 1) {//tpjop_frey.sysj line: 27, column: 28
      sum_thread_4 = sum_thread_4 + AveWindow_thread_4[(AveIndex_thread_4 + j_thread_4) % AveWs_thread_4];//tpjop_frey.sysj line: 28, column: 2
    }
currsigs0.addElement(AveResult_3);
    AveResult_3.setValue(new Float(sum_thread_4 / AveWs_thread_4));
    return false;
    }
    public boolean MethodCall17_0(){
SymIndex_thread_5 = (SymIndex_thread_5 + 1) % SymWs_thread_5;//tpjop_frey.sysj line: 48, column: 2
SymWindow_thread_5[SymIndex_thread_5] = ((Float)AveResult_3.getpreval()).floatValue();//tpjop_frey.sysj line: 49, column: 2
CorIndex_thread_5 = (SymIndex_thread_5 - HalfSymWs_thread_5 + SymWs_thread_5) % SymWs_thread_5;//tpjop_frey.sysj line: 50, column: 2
CorResult_thread_5 = 0;//tpjop_frey.sysj line: 51, column: 2
for(int k = 0; k <= HalfSymWs_thread_5; k = k + 1) {//tpjop_frey.sysj line: 52, column: 33
      x1_thread_5 = SymWindow_thread_5[(CorIndex_thread_5 + k) % SymWs_thread_5];//tpjop_frey.sysj line: 53, column: 4
      x2_thread_5 = SymWindow_thread_5[(CorIndex_thread_5 - k + SymWs_thread_5) % SymWs_thread_5];//tpjop_frey.sysj line: 54, column: 4
      CorResult_thread_5 = CorResult_thread_5 + (L_thread_5 + x1_thread_5) * (L_thread_5 + x2_thread_5);//tpjop_frey.sysj line: 55, column: 4
    }
currsigs0.addElement(SymResult_3);
    SymResult_3.setValue(new Float(CorResult_thread_5));
    return false;
    }
    public boolean MethodCall18_0(){
return SkipSamples_thread_6 == 0;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall19_0(){
SampleCountVar_thread_6 = SampleCountVar_thread_6 + 1;//tpjop_frey.sysj line: 72, column: 6
third_thread_6 = second_thread_6;//tpjop_frey.sysj line: 73, column: 6
second_thread_6 = first_thread_6;//tpjop_frey.sysj line: 74, column: 6
first_thread_6 = ((Float)SymResult_3.getpreval()).floatValue();//tpjop_frey.sysj line: 75, column: 6
    return false;
    }
    public boolean MethodCall20_0(){
return second_thread_6 > first_thread_6 && second_thread_6 > third_thread_6;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall21_0(){
return FirstPeak_thread_6;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall22_0(){
FirstPeak_thread_6 = false;//tpjop_frey.sysj line: 78, column: 3
SampleCountVar_thread_6 = 1;//tpjop_frey.sysj line: 79, column: 3
    return false;
    }
    public boolean MethodCall23_0(){
tyi_thread_6 = SampleCountVar_thread_6 - 1;//tpjop_frey.sysj line: 82, column: 3
    return false;
    }
    public boolean MethodCall24_0(){
return SampleCount_o.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall25_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall26_0(){
tutu__492 = 0;//tpjop_frey.sysj line: 83, column: 3
tutu__492 = SampleCount_o.get_w_s();//tpjop_frey.sysj line: 83, column: 3
tutu__492++;//tpjop_frey.sysj line: 83, column: 3
SampleCount_o.set_w_s(tutu__492);//tpjop_frey.sysj line: 83, column: 3
SampleCount_o.set_value(new Float(tyi_thread_6));
    return false;
    }
    public boolean MethodCall27_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall28_0(){
;//tpjop_frey.sysj line: 83, column: 3
SampleCountVar_thread_6 = 1;//tpjop_frey.sysj line: 84, column: 3
    return false;
    }
    public boolean MethodCall29_0(){
tutu__493 = 0;//tpjop_frey.sysj line: 83, column: 3
tutu__493 = SampleCount_o.get_preempted() ? SampleCount_o.refresh() : 0;//tpjop_frey.sysj line: 83, column: 3
    return false;
    }
    public boolean MethodCall30_0(){
SkipSamples_thread_6 = SkipSamples_thread_6 - 1;//tpjop_frey.sysj line: 89, column: 6
    return false;
    }
    public boolean MethodCall31_0(){
return SampleCount_o.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall32_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall33_0(){
tutu__310 = 0;//tpjop_frey.sysj line: 83, column: 3
tutu__310 = SampleCount_o.get_w_s();//tpjop_frey.sysj line: 83, column: 3
tutu__310++;//tpjop_frey.sysj line: 83, column: 3
SampleCount_o.set_w_s(tutu__310);//tpjop_frey.sysj line: 83, column: 3
SampleCount_o.set_value(new Float(tyi_thread_6));
    return false;
    }
    public boolean MethodCall34_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall35_0(){
;//tpjop_frey.sysj line: 83, column: 3
SampleCountVar_thread_6 = 1;//tpjop_frey.sysj line: 84, column: 3
    return false;
    }
    public boolean MethodCall36_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall37_0(){
tutu__311 = 0;//tpjop_frey.sysj line: 83, column: 3
tutu__311 = SampleCount_o.get_preempted() ? SampleCount_o.refresh() : 0;//tpjop_frey.sysj line: 83, column: 3
    return false;
    }
    public boolean MethodCall38_0(){
return SampleCount_o.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall39_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall40_0(){
tutu__310 = 0;//tpjop_frey.sysj line: 83, column: 3
tutu__310 = SampleCount_o.get_w_s();//tpjop_frey.sysj line: 83, column: 3
tutu__310++;//tpjop_frey.sysj line: 83, column: 3
SampleCount_o.set_w_s(tutu__310);//tpjop_frey.sysj line: 83, column: 3
SampleCount_o.set_value(new Float(tyi_thread_6));
    return false;
    }
    public boolean MethodCall41_0(){
return SampleCount_o.get_w_s() == SampleCount_o.get_w_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall42_0(){
;//tpjop_frey.sysj line: 83, column: 3
SampleCountVar_thread_6 = 1;//tpjop_frey.sysj line: 84, column: 3
    return false;
    }
    public boolean MethodCall43_0(){
tutu__311 = 0;//tpjop_frey.sysj line: 83, column: 3
tutu__311 = SampleCount_o.get_preempted() ? SampleCount_o.refresh() : 0;//tpjop_frey.sysj line: 83, column: 3
    return false;
    }

        private boolean cbackcall1_0(int var){
    boolean retval = false;
        if(var == 1){
    retval = MethodCall1_1();
    }
    else if(var == 2){
    retval = MethodCall2_1();
    }
    else if(var == 3){
    retval = MethodCall3_1();
    }
    else if(var == 4){
    retval = MethodCall4_1();
    }
    else if(var == 5){
    retval = MethodCall5_1();
    }
    else if(var == 6){
    retval = MethodCall6_1();
    }
    else if(var == 7){
    retval = MethodCall7_1();
    }
    else if(var == 8){
    retval = MethodCall8_1();
    }
    else if(var == 9){
    retval = MethodCall9_1();
    }
    else if(var == 10){
    retval = MethodCall10_1();
    }
    else if(var == 11){
    retval = MethodCall11_1();
    }
    else if(var == 12){
    retval = MethodCall12_1();
    }
    else if(var == 13){
    retval = MethodCall13_1();
    }
    else if(var == 14){
    retval = MethodCall14_1();
    }
    else if(var == 15){
    retval = MethodCall15_1();
    }
    else if(var == 16){
    retval = MethodCall16_1();
    }
    else if(var == 17){
    retval = MethodCall17_1();
    }
    else if(var == 18){
    retval = MethodCall18_1();
    }
    else if(var == 19){
    retval = MethodCall19_1();
    }
    else if(var == 20){
    retval = MethodCall20_1();
    }
    else if(var == 21){
    retval = MethodCall21_1();
    }
    else if(var == 22){
    retval = MethodCall22_1();
    }
    else if(var == 23){
    retval = MethodCall23_1();
    }
    else if(var == 24){
    retval = MethodCall24_1();
    }
    else if(var == 25){
    retval = MethodCall25_1();
    }
    else if(var == 26){
    retval = MethodCall26_1();
    }
    else if(var == 27){
    retval = MethodCall27_1();
    }
    else if(var == 28){
    retval = MethodCall28_1();
    }
    else if(var == 29){
    retval = MethodCall29_1();
    }
    else if(var == 30){
    retval = MethodCall30_1();
    }
    else if(var == 31){
    retval = MethodCall31_1();
    }
    else if(var == 32){
    retval = MethodCall32_1();
    }
    else if(var == 33){
    retval = MethodCall33_1();
    }
    else if(var == 34){
    retval = MethodCall34_1();
    }
    else if(var == 35){
    retval = MethodCall35_1();
    }
    else if(var == 36){
    retval = MethodCall36_1();
    }
    else if(var == 37){
    retval = MethodCall37_1();
    }
    else if(var == 38){
    retval = MethodCall38_1();
    }
    else if(var == 39){
    retval = MethodCall39_1();
    }
    else if(var == 40){
    retval = MethodCall40_1();
    }
    else if(var == 41){
    retval = MethodCall41_1();
    }
    else if(var == 42){
    retval = MethodCall42_1();
    }
    else if(var == 43){
    retval = MethodCall43_1();
    }
    else if(var == 44){
    retval = MethodCall44_1();
    }
    else if(var == 45){
    retval = MethodCall45_1();
    }
    else if(var == 46){
    retval = MethodCall46_1();
    }
    else if(var == 47){
    retval = MethodCall47_1();
    }
    else if(var == 48){
    retval = MethodCall48_1();
    }
    else if(var == 49){
    retval = MethodCall49_1();
    }
    else if(var == 50){
    retval = MethodCall50_1();
    }
    else if(var == 51){
    retval = MethodCall51_1();
    }
    else if(var == 52){
    retval = MethodCall52_1();
    }
    else if(var == 53){
    retval = MethodCall53_1();
    }
    else if(var == 54){
    retval = MethodCall54_1();
    }
    else if(var == 55){
    retval = MethodCall55_1();
    }
    else if(var == 56){
    retval = MethodCall56_1();
    }
    else if(var == 57){
    retval = MethodCall57_1();
    }
    else if(var == 58){
    retval = MethodCall58_1();
    }
    else if(var == 59){
    retval = MethodCall59_1();
    }
    else if(var == 60){
    retval = MethodCall60_1();
    }
    else if(var == 61){
    retval = MethodCall61_1();
    }
    else if(var == 62){
    retval = MethodCall62_1();
    }
    else if(var == 63){
    retval = MethodCall63_1();
    }
    else if(var == 64){
    retval = MethodCall64_1();
    }
    else if(var == 65){
    retval = MethodCall65_1();
    }
    else if(var == 66){
    retval = MethodCall66_1();
    }
    else if(var == 67){
    retval = MethodCall67_1();
    }
    else if(var == 68){
    retval = MethodCall68_1();
    }
    else if(var == 69){
    retval = MethodCall69_1();
    }
    else if(var == 70){
    retval = MethodCall70_1();
    }
    else if(var == 71){
    retval = MethodCall71_1();
    }
    else if(var == 72){
    retval = MethodCall72_1();
    }
    else if(var == 73){
    retval = MethodCall73_1();
    }
    else if(var == 74){
    retval = MethodCall74_1();
    }
    else if(var == 75){
    retval = MethodCall75_1();
    }
    else if(var == 76){
    retval = MethodCall76_1();
    }
    else if(var == 77){
    retval = MethodCall77_1();
    }
    else if(var == 78){
    retval = MethodCall78_1();
    }
    else if(var == 79){
    retval = MethodCall79_1();
    }
    else if(var == 80){
    retval = MethodCall80_1();
    }
    else if(var == 81){
    retval = MethodCall81_1();
    }

    else if(var == 0){ 
for(int qw=0;qw<currsigs1.size();++qw){
            int rand496563282 = ((Signal)currsigs1.elementAt(qw)).getStatus() ? ((Signal)currsigs1.elementAt(qw)).setprepresent() : ((Signal)currsigs1.elementAt(qw)).setpreclear();
			((Signal)currsigs1.elementAt(qw)).setpreval(((Signal)currsigs1.elementAt(qw)).getValue());
			}
    currsigs1.removeAllElements();
        SampleCount_in.update_r_s();
    SampleCount_in.gethook();
SampleCount_in.sethook();
        }
    else retval = cbackcall1_1(var);
    return retval;
    }
    private boolean cbackcall1_1(int var){
    boolean retval = false;
        else if(var == 82){
    retval = MethodCall82_1();
    }
    else if(var == 83){
    retval = MethodCall83_1();
    }
    else if(var == 84){
    retval = MethodCall84_1();
    }
    else if(var == 85){
    retval = MethodCall85_1();
    }
    else if(var == 86){
    retval = MethodCall86_1();
    }
    else if(var == 87){
    retval = MethodCall87_1();
    }
    else if(var == 88){
    retval = MethodCall88_1();
    }
    else if(var == 89){
    retval = MethodCall89_1();
    }
    else if(var == 90){
    retval = MethodCall90_1();
    }
    else if(var == 91){
    retval = MethodCall91_1();
    }
    else if(var == 92){
    retval = MethodCall92_1();
    }
    else if(var == 93){
    retval = MethodCall93_1();
    }
    else if(var == 94){
    retval = MethodCall94_1();
    }
    else if(var == 95){
    retval = MethodCall95_1();
    }
    else if(var == 96){
    retval = MethodCall96_1();
    }
    else if(var == 97){
    retval = MethodCall97_1();
    }
    else if(var == 98){
    retval = MethodCall98_1();
    }
    else if(var == 99){
    retval = MethodCall99_1();
    }
    else if(var == 100){
    retval = MethodCall100_1();
    }
    else if(var == 101){
    retval = MethodCall101_1();
    }
    else if(var == 102){
    retval = MethodCall102_1();
    }
    else if(var == 103){
    retval = MethodCall103_1();
    }
    else if(var == 104){
    retval = MethodCall104_1();
    }
    else if(var == 105){
    retval = MethodCall105_1();
    }
    else if(var == 106){
    retval = MethodCall106_1();
    }
    else if(var == 107){
    retval = MethodCall107_1();
    }
    else if(var == 108){
    retval = MethodCall108_1();
    }
    else if(var == 109){
    retval = MethodCall109_1();
    }
    else if(var == 110){
    retval = MethodCall110_1();
    }
    else if(var == 111){
    retval = MethodCall111_1();
    }
    else if(var == 112){
    retval = MethodCall112_1();
    }
    else if(var == 113){
    retval = MethodCall113_1();
    }
    else if(var == 114){
    retval = MethodCall114_1();
    }
    else if(var == 115){
    retval = MethodCall115_1();
    }
    else if(var == 116){
    retval = MethodCall116_1();
    }
    else if(var == 117){
    retval = MethodCall117_1();
    }
    else if(var == 118){
    retval = MethodCall118_1();
    }
    else if(var == 119){
    retval = MethodCall119_1();
    }
    else if(var == 120){
    retval = MethodCall120_1();
    }
    else if(var == 121){
    retval = MethodCall121_1();
    }
    else if(var == 122){
    retval = MethodCall122_1();
    }
    else if(var == 123){
    retval = MethodCall123_1();
    }
    else if(var == 124){
    retval = MethodCall124_1();
    }
    else if(var == 125){
    retval = MethodCall125_1();
    }
    else if(var == 126){
    retval = MethodCall126_1();
    }
    else if(var == 127){
    retval = MethodCall127_1();
    }
    else if(var == 128){
    retval = MethodCall128_1();
    }
    else if(var == 129){
    retval = MethodCall129_1();
    }
    else if(var == 130){
    retval = MethodCall130_1();
    }
    else if(var == 131){
    retval = MethodCall131_1();
    }
    else if(var == 132){
    retval = MethodCall132_1();
    }
    else if(var == 133){
    retval = MethodCall133_1();
    }
    else if(var == 134){
    retval = MethodCall134_1();
    }
    else if(var == 135){
    retval = MethodCall135_1();
    }
    else if(var == 136){
    retval = MethodCall136_1();
    }
    else if(var == 137){
    retval = MethodCall137_1();
    }
    else if(var == 138){
    retval = MethodCall138_1();
    }
    else if(var == 139){
    retval = MethodCall139_1();
    }
    else if(var == 140){
    retval = MethodCall140_1();
    }
    else if(var == 141){
    retval = MethodCall141_1();
    }
    else if(var == 142){
    retval = MethodCall142_1();
    }
    else if(var == 143){
    retval = MethodCall143_1();
    }
    else if(var == 144){
    retval = MethodCall144_1();
    }
    else if(var == 145){
    retval = MethodCall145_1();
    }
    else if(var == 146){
    retval = MethodCall146_1();
    }
    else if(var == 147){
    retval = MethodCall147_1();
    }
    else if(var == 148){
    retval = MethodCall148_1();
    }
    else if(var == 149){
    retval = MethodCall149_1();
    }
    else if(var == 150){
    retval = MethodCall150_1();
    }
    else if(var == 151){
    retval = MethodCall151_1();
    }
    else if(var == 152){
    retval = MethodCall152_1();
    }
    else if(var == 153){
    retval = MethodCall153_1();
    }
    else if(var == 154){
    retval = MethodCall154_1();
    }
    else if(var == 155){
    retval = MethodCall155_1();
    }
    else if(var == 156){
    retval = MethodCall156_1();
    }
    else if(var == 157){
    retval = MethodCall157_1();
    }
    else if(var == 158){
    retval = MethodCall158_1();
    }
    else if(var == 159){
    retval = MethodCall159_1();
    }
    else if(var == 160){
    retval = MethodCall160_1();
    }
    else if(var == 161){
    retval = MethodCall161_1();
    }
    else if(var == 162){
    retval = MethodCall162_1();
    }

    else retval = cbackcall1_2(var);
    return retval;
    }
    private boolean cbackcall1_2(int var){
    boolean retval = false;
        else if(var == 163){
    retval = MethodCall163_1();
    }
    else if(var == 164){
    retval = MethodCall164_1();
    }
    else if(var == 165){
    retval = MethodCall165_1();
    }
    else if(var == 166){
    retval = MethodCall166_1();
    }
    else if(var == 167){
    retval = MethodCall167_1();
    }
    else if(var == 168){
    retval = MethodCall168_1();
    }
    else if(var == 169){
    retval = MethodCall169_1();
    }
    else if(var == 170){
    retval = MethodCall170_1();
    }
    else if(var == 171){
    retval = MethodCall171_1();
    }
    else if(var == 172){
    retval = MethodCall172_1();
    }
    else if(var == 173){
    retval = MethodCall173_1();
    }
    else if(var == 174){
    retval = MethodCall174_1();
    }
    else if(var == 175){
    retval = MethodCall175_1();
    }
    else if(var == 176){
    retval = MethodCall176_1();
    }
    else if(var == 177){
    retval = MethodCall177_1();
    }
    else if(var == 178){
    retval = MethodCall178_1();
    }
    else if(var == 179){
    retval = MethodCall179_1();
    }
    else if(var == 180){
    retval = MethodCall180_1();
    }
    else if(var == 181){
    retval = MethodCall181_1();
    }
    else if(var == 182){
    retval = MethodCall182_1();
    }
    else if(var == 183){
    retval = MethodCall183_1();
    }
    else if(var == 184){
    retval = MethodCall184_1();
    }
    else if(var == 185){
    retval = MethodCall185_1();
    }
    else if(var == 186){
    retval = MethodCall186_1();
    }
    else if(var == 187){
    retval = MethodCall187_1();
    }
    else if(var == 188){
    retval = MethodCall188_1();
    }
    else if(var == 189){
    retval = MethodCall189_1();
    }
    else if(var == 190){
    retval = MethodCall190_1();
    }
    else if(var == 191){
    retval = MethodCall191_1();
    }
    else if(var == 192){
    retval = MethodCall192_1();
    }
    else if(var == 193){
    retval = MethodCall193_1();
    }
    else if(var == 194){
    retval = MethodCall194_1();
    }
    else if(var == 195){
    retval = MethodCall195_1();
    }

    else throw new RuntimeException("CaseNum: "+var+" not found");
    return retval;
    }

        public boolean MethodCall1_1(){
currsigs1.addElement(AveFreqval_8);
    AveFreqval_8.setValue(new Float(0));
    return false;
    }
    public boolean MethodCall2_1(){
return SampleCount_in.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall3_1(){
return SampleCount_in.get_r_s() > SampleCount_in.get_r_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall4_1(){
tutu__7544 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7544 = SampleCount_in.get_r_r();//tpjop_frey.sysj line: 318, column: 7
tutu__7544++;//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.set_r_r(tutu__7544);//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.get_val();//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall5_1(){
;//tpjop_frey.sysj line: 318, column: 7
vall_thread_9 = (Float)SampleCount_in.get_value();//tpjop_frey.sysj line: 319, column: 7
currsigs1.addElement(meCount_8);
    meCount_8.setValue(new Float(vall_thread_9));
    return false;
    }
    public boolean MethodCall6_1(){
tutu__7545 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7545 = SampleCount_in.get_preempted() ? SampleCount_in.refresh() : 0;//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall7_1(){
dft0_thread_10 = 0;//tpjop_frey.sysj line: 325, column: 5
dft1_thread_10 = 0;//tpjop_frey.sysj line: 325, column: 5
dft2_thread_10 = 0;//tpjop_frey.sysj line: 325, column: 5
FreqWs_thread_10 = 4;//tpjop_frey.sysj line: 327, column: 7
SamplingFreq_thread_10 = 16000;//tpjop_frey.sysj line: 328, column: 7
FreqWindow_thread_10 = new float[FreqWs_thread_10];//tpjop_frey.sysj line: 329, column: 7
for(int i34 = 0; i34 < FreqWs_thread_10; i34 = i34 + 1) {//tpjop_frey.sysj line: 330, column: 42
      FreqWindow_thread_10[i34] = 50;//tpjop_frey.sysj line: 331, column: 2
    }
Freq_thread_10 = 0;//tpjop_frey.sysj line: 333, column: 7
    return false;
    }
    public boolean MethodCall8_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall9_1(){
drt0_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
drt1_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
drt2_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
RocWs_thread_11 = 4;//tpjop_frey.sysj line: 380, column: 7
j34_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
RocWindow_thread_11 = new float[RocWs_thread_11];//tpjop_frey.sysj line: 383, column: 2
firstC_thread_11 = 50;//tpjop_frey.sysj line: 380, column: 7
secondC_thread_11 = 50;//tpjop_frey.sysj line: 380, column: 7
for(j34_thread_11 = 0; j34_thread_11 < RocWs_thread_11; j34_thread_11 = j34_thread_11 + 1) {//tpjop_frey.sysj line: 385, column: 32
      RocWindow_thread_11[j34_thread_11] = 0;//tpjop_frey.sysj line: 386, column: 4
    }
temp_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
Roc_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
AveRoc_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
RocSum_thread_11 = 0;//tpjop_frey.sysj line: 380, column: 7
    return false;
    }
    public boolean MethodCall10_1(){
CurrentState_thread_13 = "0";//tpjop_frey.sysj line: 447, column: 5
enter_thread_13 = 1;//tpjop_frey.sysj line: 448, column: 5
currsigs1.addElement(switch1Link_7);
    switch1Link_7.setValue(new Integer(1));
currsigs1.addElement(switch2Link_7);
    switch2Link_7.setValue(new Integer(1));
currsigs1.addElement(switch3Link_7);
    switch3Link_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall11_1(){
currsigs1.addElement(next_12);
    next_12.setValue("");
    return false;
    }
    public boolean MethodCall12_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall13_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall14_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall15_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall16_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall17_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall18_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall19_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall20_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall21_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall22_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall23_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall24_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall25_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall26_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall27_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall28_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall29_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall30_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall31_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall32_1(){
    return false;
    }
    public boolean MethodCall33_1(){
PrevSwitch1_thread_18 = 1;//tpjop_frey.sysj line: 263, column: 3
PrevSwitch2_thread_18 = 1;//tpjop_frey.sysj line: 264, column: 3
PrevSwitch3_thread_18 = 1;//tpjop_frey.sysj line: 265, column: 3
    return false;
    }
    public boolean MethodCall34_1(){
i_thread_19 = 0;//tpjop_frey.sysj line: 146, column: 1
d1_thread_19 = 0;//tpjop_frey.sysj line: 146, column: 1
d2_thread_19 = "";//tpjop_frey.sysj line: 155, column: 3
UpdateFlags_thread_19 = new boolean[6];//tpjop_frey.sysj line: 156, column: 3
CorrectPassword_thread_19 = "r";//tpjop_frey.sysj line: 157, column: 3
ThreshVars_thread_19 = new int[6];//tpjop_frey.sysj line: 158, column: 3
frths_thread_19 = new Vector(6);//tpjop_frey.sysj line: 159, column: 3
frths_thread_19.add((float)49);//tpjop_frey.sysj line: 160, column: 3
frths_thread_19.add((float)48);//tpjop_frey.sysj line: 160, column: 25
frths_thread_19.add((float)47);//tpjop_frey.sysj line: 160, column: 47
frths_thread_19.add((float)1);//tpjop_frey.sysj line: 161, column: 3
frths_thread_19.add((float)2);//tpjop_frey.sysj line: 161, column: 24
frths_thread_19.add((float)3);//tpjop_frey.sysj line: 161, column: 45
currsigs1.addElement(Frths_7);
    Frths_7.setValue(frths_thread_19);
    return false;
    }
    public boolean MethodCall35_1(){
i_thread_20 = 0;//tpjop_frey.sysj line: 224, column: 3
PassValue_thread_20 = "r";//tpjop_frey.sysj line: 225, column: 3
i_thread_20 = 0;//tpjop_frey.sysj line: 226, column: 3
    return false;
    }
    public boolean MethodCall36_1(){
return i_thread_20 == 9;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall37_1(){
;//tpjop_frey.sysj line: 227, column: 3
    return false;
    }
    public boolean MethodCall38_1(){
i_thread_20 = i_thread_20 + 1;//tpjop_frey.sysj line: 233, column: 2
    return false;
    }
    public boolean MethodCall39_1(){
return SampleCount_in.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall40_1(){
return SampleCount_in.get_r_s() > SampleCount_in.get_r_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall41_1(){
tutu__7208 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7208 = SampleCount_in.get_r_r();//tpjop_frey.sysj line: 318, column: 7
tutu__7208++;//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.set_r_r(tutu__7208);//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.get_val();//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall42_1(){
;//tpjop_frey.sysj line: 318, column: 7
vall_thread_9 = (Float)SampleCount_in.get_value();//tpjop_frey.sysj line: 319, column: 7
currsigs1.addElement(meCount_8);
    meCount_8.setValue(new Float(vall_thread_9));
    return false;
    }
    public boolean MethodCall43_1(){
tutu__7209 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7209 = SampleCount_in.get_preempted() ? SampleCount_in.refresh() : 0;//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall44_1(){
return SampleCount_in.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall45_1(){
return SampleCount_in.get_r_s() > SampleCount_in.get_r_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall46_1(){
tutu__7208 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7208 = SampleCount_in.get_r_r();//tpjop_frey.sysj line: 318, column: 7
tutu__7208++;//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.set_r_r(tutu__7208);//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.get_val();//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall47_1(){
;//tpjop_frey.sysj line: 318, column: 7
vall_thread_9 = (Float)SampleCount_in.get_value();//tpjop_frey.sysj line: 319, column: 7
currsigs1.addElement(meCount_8);
    meCount_8.setValue(new Float(vall_thread_9));
    return false;
    }
    public boolean MethodCall48_1(){
tutu__7209 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7209 = SampleCount_in.get_preempted() ? SampleCount_in.refresh() : 0;//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall49_1(){
return SampleCount_in.get_preempted();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall50_1(){
return SampleCount_in.get_r_s() > SampleCount_in.get_r_r();//Unknown file line: 0, column: 0
    }
    public boolean MethodCall51_1(){
tutu__7240 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7240 = SampleCount_in.get_r_r();//tpjop_frey.sysj line: 318, column: 7
tutu__7240++;//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.set_r_r(tutu__7240);//tpjop_frey.sysj line: 318, column: 7
SampleCount_in.get_val();//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall52_1(){
;//tpjop_frey.sysj line: 318, column: 7
vall_thread_9 = (Float)SampleCount_in.get_value();//tpjop_frey.sysj line: 319, column: 7
currsigs1.addElement(meCount_8);
    meCount_8.setValue(new Float(vall_thread_9));
    return false;
    }
    public boolean MethodCall53_1(){
tutu__7241 = 0;//tpjop_frey.sysj line: 318, column: 7
tutu__7241 = SampleCount_in.get_preempted() ? SampleCount_in.refresh() : 0;//tpjop_frey.sysj line: 318, column: 7
    return false;
    }
    public boolean MethodCall54_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall55_1(){
Freq_thread_10 = SamplingFreq_thread_10 / ghy_thread_10;//tpjop_frey.sysj line: 340, column: 2
dft0_thread_10 = ((Float)(((Vector)Frths_7.getpreval()).get(0))).floatValue();//tpjop_frey.sysj line: 341, column: 2
dft1_thread_10 = ((Float)(((Vector)Frths_7.getpreval()).get(1))).floatValue();//tpjop_frey.sysj line: 342, column: 2
dft2_thread_10 = ((Float)(((Vector)Frths_7.getpreval()).get(2))).floatValue();//tpjop_frey.sysj line: 343, column: 2
i34_thread_10 = 0;//tpjop_frey.sysj line: 334, column: 7
j34_thread_10 = 0;//tpjop_frey.sysj line: 334, column: 7
for(i34_thread_10 = FreqWs_thread_10 - 1; i34_thread_10 > 0; i34_thread_10 = i34_thread_10 - 1) {//tpjop_frey.sysj line: 345, column: 38
      FreqWindow_thread_10[i34_thread_10] = FreqWindow_thread_10[i34_thread_10 - 1];//tpjop_frey.sysj line: 346, column: 4
    }
FreqWindow_thread_10[0] = Freq_thread_10;//tpjop_frey.sysj line: 348, column: 2
Sum_thread_10 = 0;//tpjop_frey.sysj line: 350, column: 2
for(i34_thread_10 = 0; i34_thread_10 < FreqWs_thread_10; i34_thread_10 = i34_thread_10 + 1) {//tpjop_frey.sysj line: 351, column: 34
      Sum_thread_10 = Sum_thread_10 + FreqWindow_thread_10[i34_thread_10];//tpjop_frey.sysj line: 352, column: 4
    }
toe_thread_10 = (Sum_thread_10 / FreqWs_thread_10);//tpjop_frey.sysj line: 354, column: 2
currsigs1.addElement(AveFreqval_8);
    AveFreqval_8.setValue(new Float(toe_thread_10));
    return false;
    }
    public boolean MethodCall56_1(){
return toe_thread_10 >= dft0_thread_10;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall57_1(){
currsigs1.addElement(FreqStatLink_7);
    FreqStatLink_7.setValue(new Integer(0));
    return false;
    }
    public boolean MethodCall58_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall59_1(){
return toe_thread_10 < dft0_thread_10 && toe_thread_10 >= dft1_thread_10;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall60_1(){
currsigs1.addElement(FreqStatLink_7);
    FreqStatLink_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall61_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall62_1(){
return toe_thread_10 < dft1_thread_10 && toe_thread_10 >= dft2_thread_10;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall63_1(){
currsigs1.addElement(FreqStatLink_7);
    FreqStatLink_7.setValue(new Integer(2));
    return false;
    }
    public boolean MethodCall64_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall65_1(){
return toe_thread_10 < dft2_thread_10;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall66_1(){
currsigs1.addElement(FreqStatLink_7);
    FreqStatLink_7.setValue(new Integer(3));
    return false;
    }
    public boolean MethodCall67_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall68_1(){
ghy_thread_10 = 0;//tpjop_frey.sysj line: 337, column: 2
ghy_thread_10 = (int)((Float)meCount_8.getpreval()).floatValue();//tpjop_frey.sysj line: 338, column: 2
    return false;
    }
    public boolean MethodCall69_1(){
drt0_thread_11 = ((Float)((Vector)Frths_7.getpreval()).get(3)).floatValue();//tpjop_frey.sysj line: 391, column: 4
drt1_thread_11 = ((Float)((Vector)Frths_7.getpreval()).get(4)).floatValue();//tpjop_frey.sysj line: 392, column: 4
drt2_thread_11 = ((Float)((Vector)Frths_7.getpreval()).get(5)).floatValue();//tpjop_frey.sysj line: 393, column: 4
secondC_thread_11 = firstC_thread_11;//tpjop_frey.sysj line: 396, column: 4
firstC_thread_11 = ((Float)AveFreqval_8.getpreval()).floatValue();//tpjop_frey.sysj line: 397, column: 4
temp_thread_11 = (firstC_thread_11 - secondC_thread_11) * firstC_thread_11;//tpjop_frey.sysj line: 398, column: 4
if(temp_thread_11 < 0) {//tpjop_frey.sysj line: 399, column: 17
      Roc_thread_11 = -1 * temp_thread_11;//tpjop_frey.sysj line: 400, column: 6
    }
    else {//tpjop_frey.sysj line: 402, column: 8
      Roc_thread_11 = temp_thread_11;//tpjop_frey.sysj line: 403, column: 6
    }
for(j34_thread_11 = RocWs_thread_11 - 1; j34_thread_11 > 0; j34_thread_11 = j34_thread_11 - 1) {//tpjop_frey.sysj line: 405, column: 38
      RocWindow_thread_11[j34_thread_11] = RocWindow_thread_11[j34_thread_11 - 1];//tpjop_frey.sysj line: 406, column: 6
    }
RocWindow_thread_11[0] = Roc_thread_11;//tpjop_frey.sysj line: 408, column: 4
RocSum_thread_11 = 0;//tpjop_frey.sysj line: 409, column: 4
for(j34_thread_11 = 0; j34_thread_11 < RocWs_thread_11; j34_thread_11 = j34_thread_11 + 1) {//tpjop_frey.sysj line: 410, column: 35
      RocSum_thread_11 = RocSum_thread_11 + RocWindow_thread_11[j34_thread_11];//tpjop_frey.sysj line: 411, column: 6
    }
AveRoc_thread_11 = RocSum_thread_11 / RocWs_thread_11;//tpjop_frey.sysj line: 413, column: 4
    return false;
    }
    public boolean MethodCall70_1(){
return AveRoc_thread_11 < drt0_thread_11;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall71_1(){
currsigs1.addElement(RocStatLink_7);
    RocStatLink_7.setValue(new Integer(0));
    return false;
    }
    public boolean MethodCall72_1(){
return AveRoc_thread_11 >= drt0_thread_11 && AveRoc_thread_11 < drt1_thread_11;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall73_1(){
currsigs1.addElement(RocStatLink_7);
    RocStatLink_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall74_1(){
return AveRoc_thread_11 >= drt1_thread_11 && AveRoc_thread_11 < drt2_thread_11;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall75_1(){
currsigs1.addElement(RocStatLink_7);
    RocStatLink_7.setValue(new Integer(2));
    return false;
    }
    public boolean MethodCall76_1(){
return AveRoc_thread_11 >= drt2_thread_11;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall77_1(){
currsigs1.addElement(RocStatLink_7);
    RocStatLink_7.setValue(new Integer(3));
    return false;
    }
    public boolean MethodCall78_1(){
drt_thread_14 = (RocStatLink_7.getpreval() == null) ? 0 : ((Integer)RocStatLink_7.getpreval()).intValue();//tpjop_frey.sysj line: 465, column: 2
dft_thread_14 = (FreqStatLink_7.getpreval() == null) ? 0 : ((Integer)FreqStatLink_7.getpreval()).intValue();//tpjop_frey.sysj line: 466, column: 2
enter_thread_14 = ((Integer)Enter_12.getpreval()).intValue();//tpjop_frey.sysj line: 467, column: 2
    return false;
    }
    public boolean MethodCall79_1(){
return dft_thread_14 != 0;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall80_1(){
sdft_thread_14 = dft_thread_14 + "";//tpjop_frey.sysj line: 472, column: 8
currsigs1.addElement(next_12);
    next_12.setValue(sdft_thread_14);
    return false;
    }
    public boolean MethodCall81_1(){
return enter_thread_14 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall82_1(){
enter_thread_14 = enter_thread_14 + 1;//tpjop_frey.sysj line: 482, column: 8
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_14));
    return false;
    }
    public boolean MethodCall83_1(){
return drt_thread_14 != 0;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall84_1(){
sdrt_thread_14 = drt_thread_14 + "";//tpjop_frey.sysj line: 477, column: 3
currsigs1.addElement(next_12);
    next_12.setValue(sdrt_thread_14);
    return false;
    }
    public boolean MethodCall85_1(){
return enter_thread_14 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall86_1(){
enter_thread_14 = enter_thread_14 + 1;//tpjop_frey.sysj line: 482, column: 8
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_14));
    return false;
    }
    public boolean MethodCall87_1(){
return enter_thread_14 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall88_1(){
enter_thread_14 = enter_thread_14 + 1;//tpjop_frey.sysj line: 482, column: 8
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_14));
    return false;
    }
    public boolean MethodCall89_1(){
return ((String)(pp_12.getpreval())).equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall90_1(){
currsigs1.addElement(next_12);
    next_12.setValue("0");
    return false;
    }
    public boolean MethodCall91_1(){
;//tpjop_frey.sysj line: 494, column: 8
    return false;
    }
    public boolean MethodCall92_1(){
drt_thread_15 = (RocStatLink_7.getpreval() == null) ? 0 : ((Integer)RocStatLink_7.getpreval()).intValue();//tpjop_frey.sysj line: 499, column: 4
dft_thread_15 = (FreqStatLink_7.getpreval() == null) ? 0 : ((Integer)FreqStatLink_7.getpreval()).intValue();//tpjop_frey.sysj line: 500, column: 4
enter_thread_15 = ((Integer)Enter_12.getpreval()).intValue();//tpjop_frey.sysj line: 501, column: 4
    return false;
    }
    public boolean MethodCall93_1(){
return dft_thread_15 == 3 || drt_thread_15 == 3;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall94_1(){
currsigs1.addElement(next_12);
    next_12.setValue("3");
    return false;
    }
    public boolean MethodCall95_1(){
return enter_thread_15 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall96_1(){
enter_thread_15 = enter_thread_15 + 1;//tpjop_frey.sysj line: 514, column: 3
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_15));
    return false;
    }
    public boolean MethodCall97_1(){
return ((String)ep_12.getpreval()).equals("2") && (dft_thread_15 == 2 || drt_thread_15 == 2);//Unknown file line: 0, column: 0
    }
    public boolean MethodCall98_1(){
currsigs1.addElement(next_12);
    next_12.setValue("2");
    return false;
    }
    public boolean MethodCall99_1(){
return enter_thread_15 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall100_1(){
enter_thread_15 = enter_thread_15 + 1;//tpjop_frey.sysj line: 514, column: 3
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_15));
    return false;
    }
    public boolean MethodCall101_1(){
return enter_thread_15 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall102_1(){
enter_thread_15 = enter_thread_15 + 1;//tpjop_frey.sysj line: 514, column: 3
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_15));
    return false;
    }
    public boolean MethodCall103_1(){
changerval_thread_15 = (String)ep_12.getpreval();
ichan_thread_15 = new Integer(Integer.parseInt(changerval_thread_15));//tpjop_frey.sysj line: 523, column: 3
changerval_thread_15 = ichan_thread_15 - 1 + "";//tpjop_frey.sysj line: 524, column: 3
currsigs1.addElement(next_12);
    next_12.setValue(changerval_thread_15);
    return false;
    }
    public boolean MethodCall104_1(){
;//tpjop_frey.sysj line: 529, column: 9
    return false;
    }
    public boolean MethodCall105_1(){
if(!((String)next_12.getpreval()).equals("")) {//tpjop_frey.sysj line: 558, column: 35
      CurrentState_thread_13 = (String)next_12.getpreval();//tpjop_frey.sysj line: 558, column: 36
    }
    return false;
    }
    public boolean MethodCall106_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall107_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall108_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall109_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall110_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall111_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall112_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall113_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall114_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall115_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall116_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall117_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall118_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall119_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall120_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall121_1(){
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_13));
    return false;
    }
    public boolean MethodCall122_1(){
return CurrentState_thread_13.equals("0") || CurrentState_thread_13.equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall123_1(){
currsigs1.addElement(pp_12);
    pp_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall124_1(){
return CurrentState_thread_13.equals("2") || CurrentState_thread_13.equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall125_1(){
currsigs1.addElement(ep_12);
    ep_12.setValue(CurrentState_thread_13);
    return false;
    }
    public boolean MethodCall126_1(){
return dft_thread_14 != 0;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall127_1(){
sdft_thread_14 = dft_thread_14 + "";//tpjop_frey.sysj line: 472, column: 8
currsigs1.addElement(next_12);
    next_12.setValue(sdft_thread_14);
    return false;
    }
    public boolean MethodCall128_1(){
return enter_thread_14 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall129_1(){
enter_thread_14 = enter_thread_14 + 1;//tpjop_frey.sysj line: 482, column: 8
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_14));
    return false;
    }
    public boolean MethodCall130_1(){
return drt_thread_14 != 0;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall131_1(){
sdrt_thread_14 = drt_thread_14 + "";//tpjop_frey.sysj line: 477, column: 3
currsigs1.addElement(next_12);
    next_12.setValue(sdrt_thread_14);
    return false;
    }
    public boolean MethodCall132_1(){
return enter_thread_14 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall133_1(){
enter_thread_14 = enter_thread_14 + 1;//tpjop_frey.sysj line: 482, column: 8
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_14));
    return false;
    }
    public boolean MethodCall134_1(){
return enter_thread_14 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall135_1(){
enter_thread_14 = enter_thread_14 + 1;//tpjop_frey.sysj line: 482, column: 8
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_14));
    return false;
    }
    public boolean MethodCall136_1(){
return ((String)(pp_12.getpreval())).equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall137_1(){
currsigs1.addElement(next_12);
    next_12.setValue("0");
    return false;
    }
    public boolean MethodCall138_1(){
;//tpjop_frey.sysj line: 494, column: 8
    return false;
    }
    public boolean MethodCall139_1(){
return dft_thread_15 == 3 || drt_thread_15 == 3;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall140_1(){
currsigs1.addElement(next_12);
    next_12.setValue("3");
    return false;
    }
    public boolean MethodCall141_1(){
return enter_thread_15 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall142_1(){
enter_thread_15 = enter_thread_15 + 1;//tpjop_frey.sysj line: 514, column: 3
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_15));
    return false;
    }
    public boolean MethodCall143_1(){
return ((String)ep_12.getpreval()).equals("2") && (dft_thread_15 == 2 || drt_thread_15 == 2);//Unknown file line: 0, column: 0
    }
    public boolean MethodCall144_1(){
currsigs1.addElement(next_12);
    next_12.setValue("2");
    return false;
    }
    public boolean MethodCall145_1(){
return enter_thread_15 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall146_1(){
enter_thread_15 = enter_thread_15 + 1;//tpjop_frey.sysj line: 514, column: 3
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_15));
    return false;
    }
    public boolean MethodCall147_1(){
return enter_thread_15 == 1;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall148_1(){
enter_thread_15 = enter_thread_15 + 1;//tpjop_frey.sysj line: 514, column: 3
currsigs1.addElement(Enter_12);
    Enter_12.setValue(new Integer(enter_thread_15));
    return false;
    }
    public boolean MethodCall149_1(){
changerval_thread_15 = (String)ep_12.getpreval();
ichan_thread_15 = new Integer(Integer.parseInt(changerval_thread_15));//tpjop_frey.sysj line: 523, column: 3
changerval_thread_15 = ichan_thread_15 - 1 + "";//tpjop_frey.sysj line: 524, column: 3
currsigs1.addElement(next_12);
    next_12.setValue(changerval_thread_15);
    return false;
    }
    public boolean MethodCall150_1(){
;//tpjop_frey.sysj line: 529, column: 9
    return false;
    }
    public boolean MethodCall151_1(){
return ((String)next_12.getpreval()).equals("0") || ((String)next_12.getpreval()).equals("1");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall152_1(){
currsigs1.addElement(switch2Link_7);
    switch2Link_7.setValue(new Integer(1));
currsigs1.addElement(switch3Link_7);
    switch3Link_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall153_1(){
return ((String)next_12.getpreval()).equals("0");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall154_1(){
currsigs1.addElement(switch1Link_7);
    switch1Link_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall155_1(){
currsigs1.addElement(switch1Link_7);
    switch1Link_7.setValue(new Integer(0));
    return false;
    }
    public boolean MethodCall156_1(){
return ((String)next_12.getpreval()).equals("2") || ((String)next_12.getpreval()).equals("3");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall157_1(){
currsigs1.addElement(switch1Link_7);
    switch1Link_7.setValue(new Integer(0));
currsigs1.addElement(switch2Link_7);
    switch2Link_7.setValue(new Integer(0));
    return false;
    }
    public boolean MethodCall158_1(){
return ((String)next_12.getpreval()).equals("2");//Unknown file line: 0, column: 0
    }
    public boolean MethodCall159_1(){
currsigs1.addElement(switch3Link_7);
    switch3Link_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall160_1(){
currsigs1.addElement(switch3Link_7);
    switch3Link_7.setValue(new Integer(0));
    return false;
    }
    public boolean MethodCall161_1(){
return i1_thread_17 < 1000;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall162_1(){
i1_thread_17 = i1_thread_17 + 1;//tpjop_frey.sysj line: 569, column: 8
    return false;
    }
    public boolean MethodCall163_1(){
i1_thread_17 = 0;//tpjop_frey.sysj line: 572, column: 8
    return false;
    }
    public boolean MethodCall164_1(){
;//tpjop_frey.sysj line: 566, column: 2
    return false;
    }
    public boolean MethodCall165_1(){
return i1_thread_17 < 1000;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall166_1(){
i1_thread_17 = i1_thread_17 + 1;//tpjop_frey.sysj line: 569, column: 8
    return false;
    }
    public boolean MethodCall167_1(){
i1_thread_17 = 0;//tpjop_frey.sysj line: 572, column: 8
    return false;
    }
    public boolean MethodCall168_1(){
;//tpjop_frey.sysj line: 566, column: 2
    return false;
    }
    public boolean MethodCall169_1(){
return i1_thread_17 < 1000;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall170_1(){
i1_thread_17 = i1_thread_17 + 1;//tpjop_frey.sysj line: 569, column: 8
    return false;
    }
    public boolean MethodCall171_1(){
i1_thread_17 = 0;//tpjop_frey.sysj line: 572, column: 8
    return false;
    }
    public boolean MethodCall172_1(){
;//tpjop_frey.sysj line: 566, column: 2
    return false;
    }
    public boolean MethodCall173_1(){
dummy1_thread_18 = 0;//tpjop_frey.sysj line: 266, column: 3
dummy2_thread_18 = 0;//tpjop_frey.sysj line: 266, column: 3
dummy3_thread_18 = 0;//tpjop_frey.sysj line: 266, column: 3
    return false;
    }
    public boolean MethodCall174_1(){
dummy1_thread_18 = ((Integer)switch1Link_7.getpreval()).intValue();//tpjop_frey.sysj line: 270, column: 5
dummy2_thread_18 = ((Integer)switch2Link_7.getpreval()).intValue();//tpjop_frey.sysj line: 271, column: 5
dummy3_thread_18 = ((Integer)switch3Link_7.getpreval()).intValue();//tpjop_frey.sysj line: 272, column: 5
if(!(PrevSwitch1_thread_18 == dummy1_thread_18 && PrevSwitch2_thread_18 == dummy2_thread_18 && PrevSwitch3_thread_18 == dummy3_thread_18)) {//tpjop_frey.sysj line: 273, column: 85
      System.out.println();//tpjop_frey.sysj line: 274, column: 7
      System.out.println();//tpjop_frey.sysj line: 275, column: 7
      if(dummy1_thread_18 == 1) {//tpjop_frey.sysj line: 276, column: 21
        System.out.println("switch1 closed");//tpjop_frey.sysj line: 277, column: 2
      }
      else {//tpjop_frey.sysj line: 279, column: 11
        System.out.println("switch1 open");//tpjop_frey.sysj line: 280, column: 2
      }
      if(dummy2_thread_18 == 1) {//tpjop_frey.sysj line: 282, column: 21
        System.out.println("switch2 closed");//tpjop_frey.sysj line: 283, column: 2
      }
      else {//tpjop_frey.sysj line: 285, column: 11
        System.out.println("switch2 open");//tpjop_frey.sysj line: 286, column: 2
      }
      if(dummy3_thread_18 == 1) {//tpjop_frey.sysj line: 288, column: 21
        System.out.println("switch3 closed");//tpjop_frey.sysj line: 289, column: 2
      }
      else {//tpjop_frey.sysj line: 291, column: 11
        System.out.println("switch3 open");//tpjop_frey.sysj line: 292, column: 2
      }
      System.out.println();//tpjop_frey.sysj line: 294, column: 7
      System.out.println();//tpjop_frey.sysj line: 295, column: 7
      PrevSwitch1_thread_18 = dummy1_thread_18;//tpjop_frey.sysj line: 296, column: 7
      PrevSwitch2_thread_18 = dummy2_thread_18;//tpjop_frey.sysj line: 297, column: 7
      PrevSwitch3_thread_18 = dummy3_thread_18;//tpjop_frey.sysj line: 298, column: 7
    }
    return false;
    }
    public boolean MethodCall175_1(){
for(i_thread_19 = 0; i_thread_19 < 6; i_thread_19 = i_thread_19 + 1) {//tpjop_frey.sysj line: 168, column: 28
      UpdateFlags_thread_19[i_thread_19] = false;//tpjop_frey.sysj line: 169, column: 2
    }
    return false;
    }
    public boolean MethodCall176_1(){
d2_thread_19 = (String)passwordLink_7.getpreval();
    return false;
    }
    public boolean MethodCall177_1(){
return CorrectPassword_thread_19.equals(d2_thread_19);//Unknown file line: 0, column: 0
    }
    public boolean MethodCall178_1(){
currsigs1.addElement(ThreshCodeLink_7);
    ThreshCodeLink_7.setValue(new Integer(1));
    return false;
    }
    public boolean MethodCall179_1(){
;//tpjop_frey.sysj line: 173, column: 7
    return false;
    }
    public boolean MethodCall180_1(){
i_thread_19 = 0;//tpjop_frey.sysj line: 188, column: 2
    return false;
    }
    public boolean MethodCall181_1(){
return i_thread_19 < 6;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall182_1(){
;//tpjop_frey.sysj line: 189, column: 2
for(int q_thread_19 = 0; q_thread_19 < UpdateFlags_thread_19.length; q_thread_19 = q_thread_19 + 1) {//tpjop_frey.sysj line: 211, column: 44
      if(UpdateFlags_thread_19[q_thread_19]) {//tpjop_frey.sysj line: 212, column: 20
        frths_thread_19.setElementAt((float)ThreshVars_thread_19[q_thread_19], q_thread_19);//tpjop_frey.sysj line: 213, column: 4
      }
    }
currsigs1.addElement(Frths_7);
    Frths_7.setValue(frths_thread_19);
    return false;
    }
    public boolean MethodCall183_1(){
UpdateFlags_thread_19[i_thread_19] = true;//tpjop_frey.sysj line: 194, column: 3
ThreshVars_thread_19[i_thread_19] = ((Integer)ThreshValueLink_7.getpreval()).intValue();//tpjop_frey.sysj line: 195, column: 3
    return false;
    }
    public boolean MethodCall184_1(){
return i_thread_19 < 5;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall185_1(){
tu_thread_19 = i_thread_19 + 2;//tpjop_frey.sysj line: 199, column: 3
currsigs1.addElement(ThreshCodeLink_7);
    ThreshCodeLink_7.setValue(new Integer(tu_thread_19));
i_thread_19 = i_thread_19 + 1;//tpjop_frey.sysj line: 202, column: 8
    return false;
    }
    public boolean MethodCall186_1(){
i_thread_19 = i_thread_19 + 1;//tpjop_frey.sysj line: 202, column: 8
    return false;
    }
    public boolean MethodCall187_1(){
return i_thread_19 < 6;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall188_1(){
;//tpjop_frey.sysj line: 189, column: 2
for(int q_thread_19 = 0; q_thread_19 < UpdateFlags_thread_19.length; q_thread_19 = q_thread_19 + 1) {//tpjop_frey.sysj line: 211, column: 44
      if(UpdateFlags_thread_19[q_thread_19]) {//tpjop_frey.sysj line: 212, column: 20
        frths_thread_19.setElementAt((float)ThreshVars_thread_19[q_thread_19], q_thread_19);//tpjop_frey.sysj line: 213, column: 4
      }
    }
currsigs1.addElement(Frths_7);
    Frths_7.setValue(frths_thread_19);
    return false;
    }
    public boolean MethodCall189_1(){
for(int q_thread_19 = 0; q_thread_19 < UpdateFlags_thread_19.length; q_thread_19 = q_thread_19 + 1) {//tpjop_frey.sysj line: 211, column: 44
      if(UpdateFlags_thread_19[q_thread_19]) {//tpjop_frey.sysj line: 212, column: 20
        frths_thread_19.setElementAt((float)ThreshVars_thread_19[q_thread_19], q_thread_19);//tpjop_frey.sysj line: 213, column: 4
      }
    }
currsigs1.addElement(Frths_7);
    Frths_7.setValue(frths_thread_19);
    return false;
    }
    public boolean MethodCall190_1(){
return i_thread_20 == 9;//Unknown file line: 0, column: 0
    }
    public boolean MethodCall191_1(){
;//tpjop_frey.sysj line: 227, column: 3
    return false;
    }
    public boolean MethodCall192_1(){
i_thread_20 = i_thread_20 + 1;//tpjop_frey.sysj line: 233, column: 2
    return false;
    }
    public boolean MethodCall193_1(){
currsigs1.addElement(passwordLink_7);
    passwordLink_7.setValue(PassValue_thread_20);
    return false;
    }
    public boolean MethodCall194_1(){
y_thread_20 = (int)49.1;//tpjop_frey.sysj line: 245, column: 5
currsigs1.addElement(ThreshValueLink_7);
    ThreshValueLink_7.setValue(new Integer(y_thread_20));
    return false;
    }
    public boolean MethodCall195_1(){
u67_thread_20 = (int)47.1;//tpjop_frey.sysj line: 252, column: 5
currsigs1.addElement(ThreshValueLink_7);
    ThreshValueLink_7.setValue(new Integer(u67_thread_20));
    return false;
    }

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                        