import java.util.*;
import systemj.bootstrap.ClockDomain;
import systemj.lib.*;
import UtilA.*;//R1.sysj line: 1, column: 1

public class RoomC extends ClockDomain{
  public RoomC(){super(); init();}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  public input_Channel ResetRC2_in = new input_Channel();
  public input_Channel IncreaseCount2_in = new input_Channel();
  public input_Channel DecreaseCount2_in = new input_Channel();
  public output_Channel Emergency2_o = new output_Channel();
  public output_Channel ActivateAlarm2_o = new output_Channel();
  private Signal Presence_1;
  private Signal TimeOutRC_1;
  private Signal TriggerRC_1;
  private Signal LightLevel_1;
  private Signal SetIntensity_1;
  private Signal armed_4;
  private Signal disarmed_4;
  private Signal INC_4;
  private Signal DEC_4;
  private Signal RRC_4;
  private Signal tempsigyo_4;
  private Signal tempsigyo2_4;
  private Random random_thread_2;//R1.sysj line: 157, column: 7
  private Integer SetValue_thread_6;//R1.sysj line: 10, column: 7
  private Integer CurrentLightLevel_thread_6;//R1.sysj line: 11, column: 7
  private int tutu__1423;//R1.sysj line: 111, column: 4
  private int tutu__1424;//R1.sysj line: 111, column: 4
  private int tutu__1530;//R1.sysj line: 123, column: 6
  private int tutu__1531;//R1.sysj line: 123, column: 6
  private int tutu__1637;//R1.sysj line: 135, column: 8
  private int tutu__1638;//R1.sysj line: 135, column: 8
  private int i_thread_6;//R1.sysj line: 21, column: 3
  private int tutu__815;//R1.sysj line: 53, column: 4
  private int tutu__816;//R1.sysj line: 53, column: 4
  private int tutu__791;//R1.sysj line: 53, column: 4
  private int tutu__792;//R1.sysj line: 53, column: 4
  private int tutu__864;//R1.sysj line: 55, column: 4
  private int tutu__865;//R1.sysj line: 55, column: 4
  private int tutu__840;//R1.sysj line: 55, column: 4
  private int tutu__841;//R1.sysj line: 55, column: 4
  private int count_thread_9;//R1.sysj line: 68, column: 6
  private int tutu__1408;//R1.sysj line: 111, column: 4
  private int tutu__1409;//R1.sysj line: 111, column: 4
  private Boolean val_thread_11;//R1.sysj line: 110, column: 4
  private int tutu__1493;//R1.sysj line: 111, column: 4
  private int tutu__1494;//R1.sysj line: 111, column: 4
  private int tutu__1515;//R1.sysj line: 123, column: 6
  private int tutu__1516;//R1.sysj line: 123, column: 6
  private Boolean val_thread_12;//R1.sysj line: 122, column: 6
  private int tutu__1600;//R1.sysj line: 123, column: 6
  private int tutu__1601;//R1.sysj line: 123, column: 6
  private int tutu__1622;//R1.sysj line: 135, column: 8
  private int tutu__1623;//R1.sysj line: 135, column: 8
  private Boolean val_thread_13;//R1.sysj line: 134, column: 8
  private int tutu__1707;//R1.sysj line: 135, column: 8
  private int tutu__1708;//R1.sysj line: 135, column: 8
  private int S1732 = 1;
  private int S24 = 1;
  private int S7 = 1;
  private int S29 = 1;
  private int S1730 = 1;
  private int S1729 = 1;
  private int S1099 = 1;
  private int S789 = 1;
  private int S160 = 1;
  private int S74 = 1;
  private int S1097 = 1;
  private int S839 = 1;
  private int S799 = 1;
  private int S794 = 1;
  private int S848 = 1;
  private int S843 = 1;
  private int S1407 = 1;
  private int S1201 = 1;
  private int S1167 = 1;
  private int S1110 = 1;
  private int S1109 = 1;
  private int S1199 = 1;
  private int S1168 = 1;
  private int S1173 = 1;
  private int S1514 = 1;
  private int S1438 = 1;
  private int S1413 = 1;
  private int S1621 = 1;
  private int S1545 = 1;
  private int S1520 = 1;
  private int S1728 = 1;
  private int S1652 = 1;
  private int S1627 = 1;
  
  private int[] ends = new int[14];
  private int[] tdone = new int[14];
  
  public void thread1802(int [] tdone, int [] ends){
        switch(S1728){
      case 0 : 
        active[13]=0;
        ends[13]=0;
        tdone[13]=1;
        break;
      
      case 1 : 
        switch(S1652){
          case 0 : 
            switch(S1627){
              case 0 : 
                if(ResetRC2_in.get_preempted()){//R1.sysj line: 135, column: 8
                  tutu__1623 = 0;//R1.sysj line: 135, column: 8
                  tutu__1623 = ResetRC2_in.get_preempted() ? ResetRC2_in.refresh() : 0;//R1.sysj line: 135, column: 8
                  S1627=1;
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
                else {
                  if(ResetRC2_in.get_r_s() > ResetRC2_in.get_r_r()){//R1.sysj line: 135, column: 8
                    tutu__1622 = 0;//R1.sysj line: 135, column: 8
                    tutu__1622 = ResetRC2_in.get_r_r();//R1.sysj line: 135, column: 8
                    tutu__1622++;//R1.sysj line: 135, column: 8
                    ResetRC2_in.set_r_r(tutu__1622);//R1.sysj line: 135, column: 8
                    ResetRC2_in.get_val();//R1.sysj line: 135, column: 8
                    ends[13]=2;
                    ;//R1.sysj line: 135, column: 8
                    val_thread_13 = (Boolean)ResetRC2_in.get_value();//R1.sysj line: 136, column: 8
                    if(val_thread_13.getprestatus()){//R1.sysj line: 137, column: 8
                      RRC_4.setPresent();//R1.sysj line: 138, column: 3
                      currsigs.addElement(RRC_4);
                      System.out.println("Emitted RRC_4");
                      S1652=1;
                      active[13]=1;
                      ends[13]=1;
                      tdone[13]=1;
                    }
                    else {
                      S1652=1;
                      active[13]=1;
                      ends[13]=1;
                      tdone[13]=1;
                    }
                  }
                  else {
                    active[13]=1;
                    ends[13]=1;
                    tdone[13]=1;
                  }
                }
                break;
              
              case 1 : 
                S1627=1;
                S1627=0;
                if(ResetRC2_in.get_preempted()){//R1.sysj line: 135, column: 8
                  tutu__1623 = 0;//R1.sysj line: 135, column: 8
                  tutu__1623 = ResetRC2_in.get_preempted() ? ResetRC2_in.refresh() : 0;//R1.sysj line: 135, column: 8
                  S1627=1;
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
                else {
                  if(ResetRC2_in.get_r_s() > ResetRC2_in.get_r_r()){//R1.sysj line: 135, column: 8
                    tutu__1622 = 0;//R1.sysj line: 135, column: 8
                    tutu__1622 = ResetRC2_in.get_r_r();//R1.sysj line: 135, column: 8
                    tutu__1622++;//R1.sysj line: 135, column: 8
                    ResetRC2_in.set_r_r(tutu__1622);//R1.sysj line: 135, column: 8
                    ResetRC2_in.get_val();//R1.sysj line: 135, column: 8
                    ends[13]=2;
                    ;//R1.sysj line: 135, column: 8
                    val_thread_13 = (Boolean)ResetRC2_in.get_value();//R1.sysj line: 136, column: 8
                    if(val_thread_13.getprestatus()){//R1.sysj line: 137, column: 8
                      RRC_4.setPresent();//R1.sysj line: 138, column: 3
                      currsigs.addElement(RRC_4);
                      System.out.println("Emitted RRC_4");
                      S1652=1;
                      active[13]=1;
                      ends[13]=1;
                      tdone[13]=1;
                    }
                    else {
                      S1652=1;
                      active[13]=1;
                      ends[13]=1;
                      tdone[13]=1;
                    }
                  }
                  else {
                    active[13]=1;
                    ends[13]=1;
                    tdone[13]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S1652=1;
            val_thread_13 = null;//R1.sysj line: 134, column: 8
            S1652=0;
            S1627=0;
            if(ResetRC2_in.get_preempted()){//R1.sysj line: 135, column: 8
              tutu__1708 = 0;//R1.sysj line: 135, column: 8
              tutu__1708 = ResetRC2_in.get_preempted() ? ResetRC2_in.refresh() : 0;//R1.sysj line: 135, column: 8
              S1627=1;
              active[13]=1;
              ends[13]=1;
              tdone[13]=1;
            }
            else {
              if(ResetRC2_in.get_r_s() > ResetRC2_in.get_r_r()){//R1.sysj line: 135, column: 8
                tutu__1707 = 0;//R1.sysj line: 135, column: 8
                tutu__1707 = ResetRC2_in.get_r_r();//R1.sysj line: 135, column: 8
                tutu__1707++;//R1.sysj line: 135, column: 8
                ResetRC2_in.set_r_r(tutu__1707);//R1.sysj line: 135, column: 8
                ResetRC2_in.get_val();//R1.sysj line: 135, column: 8
                ends[13]=2;
                ;//R1.sysj line: 135, column: 8
                val_thread_13 = (Boolean)ResetRC2_in.get_value();//R1.sysj line: 136, column: 8
                if(val_thread_13.getprestatus()){//R1.sysj line: 137, column: 8
                  RRC_4.setPresent();//R1.sysj line: 138, column: 3
                  currsigs.addElement(RRC_4);
                  System.out.println("Emitted RRC_4");
                  S1652=1;
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
                else {
                  S1652=1;
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
              }
              else {
                active[13]=1;
                ends[13]=1;
                tdone[13]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1801(int [] tdone, int [] ends){
        switch(S1621){
      case 0 : 
        active[12]=0;
        ends[12]=0;
        tdone[12]=1;
        break;
      
      case 1 : 
        switch(S1545){
          case 0 : 
            switch(S1520){
              case 0 : 
                if(DecreaseCount2_in.get_preempted()){//R1.sysj line: 123, column: 6
                  tutu__1516 = 0;//R1.sysj line: 123, column: 6
                  tutu__1516 = DecreaseCount2_in.get_preempted() ? DecreaseCount2_in.refresh() : 0;//R1.sysj line: 123, column: 6
                  S1520=1;
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
                else {
                  if(DecreaseCount2_in.get_r_s() > DecreaseCount2_in.get_r_r()){//R1.sysj line: 123, column: 6
                    tutu__1515 = 0;//R1.sysj line: 123, column: 6
                    tutu__1515 = DecreaseCount2_in.get_r_r();//R1.sysj line: 123, column: 6
                    tutu__1515++;//R1.sysj line: 123, column: 6
                    DecreaseCount2_in.set_r_r(tutu__1515);//R1.sysj line: 123, column: 6
                    DecreaseCount2_in.get_val();//R1.sysj line: 123, column: 6
                    ends[12]=2;
                    ;//R1.sysj line: 123, column: 6
                    val_thread_12 = (Boolean)DecreaseCount2_in.get_value();//R1.sysj line: 124, column: 6
                    if(val_thread_12.getprestatus()){//R1.sysj line: 125, column: 6
                      DEC_4.setPresent();//R1.sysj line: 126, column: 8
                      currsigs.addElement(DEC_4);
                      System.out.println("Emitted DEC_4");
                      S1545=1;
                      active[12]=1;
                      ends[12]=1;
                      tdone[12]=1;
                    }
                    else {
                      S1545=1;
                      active[12]=1;
                      ends[12]=1;
                      tdone[12]=1;
                    }
                  }
                  else {
                    active[12]=1;
                    ends[12]=1;
                    tdone[12]=1;
                  }
                }
                break;
              
              case 1 : 
                S1520=1;
                S1520=0;
                if(DecreaseCount2_in.get_preempted()){//R1.sysj line: 123, column: 6
                  tutu__1516 = 0;//R1.sysj line: 123, column: 6
                  tutu__1516 = DecreaseCount2_in.get_preempted() ? DecreaseCount2_in.refresh() : 0;//R1.sysj line: 123, column: 6
                  S1520=1;
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
                else {
                  if(DecreaseCount2_in.get_r_s() > DecreaseCount2_in.get_r_r()){//R1.sysj line: 123, column: 6
                    tutu__1515 = 0;//R1.sysj line: 123, column: 6
                    tutu__1515 = DecreaseCount2_in.get_r_r();//R1.sysj line: 123, column: 6
                    tutu__1515++;//R1.sysj line: 123, column: 6
                    DecreaseCount2_in.set_r_r(tutu__1515);//R1.sysj line: 123, column: 6
                    DecreaseCount2_in.get_val();//R1.sysj line: 123, column: 6
                    ends[12]=2;
                    ;//R1.sysj line: 123, column: 6
                    val_thread_12 = (Boolean)DecreaseCount2_in.get_value();//R1.sysj line: 124, column: 6
                    if(val_thread_12.getprestatus()){//R1.sysj line: 125, column: 6
                      DEC_4.setPresent();//R1.sysj line: 126, column: 8
                      currsigs.addElement(DEC_4);
                      System.out.println("Emitted DEC_4");
                      S1545=1;
                      active[12]=1;
                      ends[12]=1;
                      tdone[12]=1;
                    }
                    else {
                      S1545=1;
                      active[12]=1;
                      ends[12]=1;
                      tdone[12]=1;
                    }
                  }
                  else {
                    active[12]=1;
                    ends[12]=1;
                    tdone[12]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S1545=1;
            val_thread_12 = null;//R1.sysj line: 122, column: 6
            S1545=0;
            S1520=0;
            if(DecreaseCount2_in.get_preempted()){//R1.sysj line: 123, column: 6
              tutu__1601 = 0;//R1.sysj line: 123, column: 6
              tutu__1601 = DecreaseCount2_in.get_preempted() ? DecreaseCount2_in.refresh() : 0;//R1.sysj line: 123, column: 6
              S1520=1;
              active[12]=1;
              ends[12]=1;
              tdone[12]=1;
            }
            else {
              if(DecreaseCount2_in.get_r_s() > DecreaseCount2_in.get_r_r()){//R1.sysj line: 123, column: 6
                tutu__1600 = 0;//R1.sysj line: 123, column: 6
                tutu__1600 = DecreaseCount2_in.get_r_r();//R1.sysj line: 123, column: 6
                tutu__1600++;//R1.sysj line: 123, column: 6
                DecreaseCount2_in.set_r_r(tutu__1600);//R1.sysj line: 123, column: 6
                DecreaseCount2_in.get_val();//R1.sysj line: 123, column: 6
                ends[12]=2;
                ;//R1.sysj line: 123, column: 6
                val_thread_12 = (Boolean)DecreaseCount2_in.get_value();//R1.sysj line: 124, column: 6
                if(val_thread_12.getprestatus()){//R1.sysj line: 125, column: 6
                  DEC_4.setPresent();//R1.sysj line: 126, column: 8
                  currsigs.addElement(DEC_4);
                  System.out.println("Emitted DEC_4");
                  S1545=1;
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
                else {
                  S1545=1;
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
              }
              else {
                active[12]=1;
                ends[12]=1;
                tdone[12]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1800(int [] tdone, int [] ends){
        switch(S1514){
      case 0 : 
        active[11]=0;
        ends[11]=0;
        tdone[11]=1;
        break;
      
      case 1 : 
        switch(S1438){
          case 0 : 
            switch(S1413){
              case 0 : 
                if(IncreaseCount2_in.get_preempted()){//R1.sysj line: 111, column: 4
                  tutu__1409 = 0;//R1.sysj line: 111, column: 4
                  tutu__1409 = IncreaseCount2_in.get_preempted() ? IncreaseCount2_in.refresh() : 0;//R1.sysj line: 111, column: 4
                  S1413=1;
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
                else {
                  if(IncreaseCount2_in.get_r_s() > IncreaseCount2_in.get_r_r()){//R1.sysj line: 111, column: 4
                    tutu__1408 = 0;//R1.sysj line: 111, column: 4
                    tutu__1408 = IncreaseCount2_in.get_r_r();//R1.sysj line: 111, column: 4
                    tutu__1408++;//R1.sysj line: 111, column: 4
                    IncreaseCount2_in.set_r_r(tutu__1408);//R1.sysj line: 111, column: 4
                    IncreaseCount2_in.get_val();//R1.sysj line: 111, column: 4
                    ends[11]=2;
                    ;//R1.sysj line: 111, column: 4
                    val_thread_11 = (Boolean)IncreaseCount2_in.get_value();//R1.sysj line: 112, column: 4
                    if(val_thread_11.getprestatus()){//R1.sysj line: 113, column: 4
                      INC_4.setPresent();//R1.sysj line: 114, column: 6
                      currsigs.addElement(INC_4);
                      System.out.println("Emitted INC_4");
                      S1438=1;
                      active[11]=1;
                      ends[11]=1;
                      tdone[11]=1;
                    }
                    else {
                      S1438=1;
                      active[11]=1;
                      ends[11]=1;
                      tdone[11]=1;
                    }
                  }
                  else {
                    active[11]=1;
                    ends[11]=1;
                    tdone[11]=1;
                  }
                }
                break;
              
              case 1 : 
                S1413=1;
                S1413=0;
                if(IncreaseCount2_in.get_preempted()){//R1.sysj line: 111, column: 4
                  tutu__1409 = 0;//R1.sysj line: 111, column: 4
                  tutu__1409 = IncreaseCount2_in.get_preempted() ? IncreaseCount2_in.refresh() : 0;//R1.sysj line: 111, column: 4
                  S1413=1;
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
                else {
                  if(IncreaseCount2_in.get_r_s() > IncreaseCount2_in.get_r_r()){//R1.sysj line: 111, column: 4
                    tutu__1408 = 0;//R1.sysj line: 111, column: 4
                    tutu__1408 = IncreaseCount2_in.get_r_r();//R1.sysj line: 111, column: 4
                    tutu__1408++;//R1.sysj line: 111, column: 4
                    IncreaseCount2_in.set_r_r(tutu__1408);//R1.sysj line: 111, column: 4
                    IncreaseCount2_in.get_val();//R1.sysj line: 111, column: 4
                    ends[11]=2;
                    ;//R1.sysj line: 111, column: 4
                    val_thread_11 = (Boolean)IncreaseCount2_in.get_value();//R1.sysj line: 112, column: 4
                    if(val_thread_11.getprestatus()){//R1.sysj line: 113, column: 4
                      INC_4.setPresent();//R1.sysj line: 114, column: 6
                      currsigs.addElement(INC_4);
                      System.out.println("Emitted INC_4");
                      S1438=1;
                      active[11]=1;
                      ends[11]=1;
                      tdone[11]=1;
                    }
                    else {
                      S1438=1;
                      active[11]=1;
                      ends[11]=1;
                      tdone[11]=1;
                    }
                  }
                  else {
                    active[11]=1;
                    ends[11]=1;
                    tdone[11]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S1438=1;
            val_thread_11 = null;//R1.sysj line: 110, column: 4
            S1438=0;
            S1413=0;
            if(IncreaseCount2_in.get_preempted()){//R1.sysj line: 111, column: 4
              tutu__1494 = 0;//R1.sysj line: 111, column: 4
              tutu__1494 = IncreaseCount2_in.get_preempted() ? IncreaseCount2_in.refresh() : 0;//R1.sysj line: 111, column: 4
              S1413=1;
              active[11]=1;
              ends[11]=1;
              tdone[11]=1;
            }
            else {
              if(IncreaseCount2_in.get_r_s() > IncreaseCount2_in.get_r_r()){//R1.sysj line: 111, column: 4
                tutu__1493 = 0;//R1.sysj line: 111, column: 4
                tutu__1493 = IncreaseCount2_in.get_r_r();//R1.sysj line: 111, column: 4
                tutu__1493++;//R1.sysj line: 111, column: 4
                IncreaseCount2_in.set_r_r(tutu__1493);//R1.sysj line: 111, column: 4
                IncreaseCount2_in.get_val();//R1.sysj line: 111, column: 4
                ends[11]=2;
                ;//R1.sysj line: 111, column: 4
                val_thread_11 = (Boolean)IncreaseCount2_in.get_value();//R1.sysj line: 112, column: 4
                if(val_thread_11.getprestatus()){//R1.sysj line: 113, column: 4
                  INC_4.setPresent();//R1.sysj line: 114, column: 6
                  currsigs.addElement(INC_4);
                  System.out.println("Emitted INC_4");
                  S1438=1;
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
                else {
                  S1438=1;
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
              }
              else {
                active[11]=1;
                ends[11]=1;
                tdone[11]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1798(int [] tdone, int [] ends){
        S1199=1;
    S1168=0;
    active[10]=1;
    ends[10]=1;
    tdone[10]=1;
  }

  public void thread1797(int [] tdone, int [] ends){
        S1167=1;
    count_thread_9 = 0;//R1.sysj line: 68, column: 6
    S1110=0;
    if(count_thread_9 != 0){//R1.sysj line: 70, column: 11
      S1109=0;
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
    else {
      S1109=1;
      armed_4.setPresent();//R1.sysj line: 83, column: 5
      currsigs.addElement(armed_4);
      System.out.println("Emitted armed_4");
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
  }

  public void thread1795(int [] tdone, int [] ends){
        switch(S1199){
      case 0 : 
        active[10]=0;
        ends[10]=0;
        tdone[10]=1;
        break;
      
      case 1 : 
        switch(S1168){
          case 0 : 
            S1168=0;
            S1168=1;
            active[10]=1;
            ends[10]=1;
            tdone[10]=1;
            break;
          
          case 1 : 
            S1168=1;
            S1168=2;
            S1173=0;
            if(armed_4.getprestatus()){//R1.sysj line: 96, column: 19
              S1173=1;
              active[10]=1;
              ends[10]=1;
              tdone[10]=1;
            }
            else {
              disarmed_4.setPresent();//R1.sysj line: 97, column: 5
              currsigs.addElement(disarmed_4);
              System.out.println("Emitted disarmed_4");
              active[10]=1;
              ends[10]=1;
              tdone[10]=1;
            }
            break;
          
          case 2 : 
            switch(S1173){
              case 0 : 
                if(armed_4.getprestatus()){//R1.sysj line: 96, column: 19
                  S1173=1;
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                else {
                  disarmed_4.setPresent();//R1.sysj line: 97, column: 5
                  currsigs.addElement(disarmed_4);
                  System.out.println("Emitted disarmed_4");
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                break;
              
              case 1 : 
                S1173=1;
                S1173=0;
                if(armed_4.getprestatus()){//R1.sysj line: 96, column: 19
                  S1173=1;
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                else {
                  disarmed_4.setPresent();//R1.sysj line: 97, column: 5
                  currsigs.addElement(disarmed_4);
                  System.out.println("Emitted disarmed_4");
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                break;
              
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1794(int [] tdone, int [] ends){
        switch(S1167){
      case 0 : 
        active[9]=0;
        ends[9]=0;
        tdone[9]=1;
        break;
      
      case 1 : 
        switch(S1110){
          case 0 : 
            switch(S1109){
              case 0 : 
                if(INC_4.getprestatus() || DEC_4.getprestatus()){//R1.sysj line: 72, column: 3
                  if(INC_4.getprestatus()){//R1.sysj line: 73, column: 11
                    count_thread_9 = count_thread_9 + 1;//R1.sysj line: 74, column: 5
                    S1110=1;
                    active[9]=1;
                    ends[9]=1;
                    tdone[9]=1;
                  }
                  else {
                    count_thread_9 = count_thread_9 - 1;//R1.sysj line: 77, column: 5
                    S1110=1;
                    active[9]=1;
                    ends[9]=1;
                    tdone[9]=1;
                  }
                }
                else {
                  active[9]=1;
                  ends[9]=1;
                  tdone[9]=1;
                }
                break;
              
              case 1 : 
                if(INC_4.getprestatus()){//R1.sysj line: 82, column: 9
                  count_thread_9 = count_thread_9 + 1;//R1.sysj line: 85, column: 3
                  S1110=1;
                  active[9]=1;
                  ends[9]=1;
                  tdone[9]=1;
                }
                else {
                  armed_4.setPresent();//R1.sysj line: 83, column: 5
                  currsigs.addElement(armed_4);
                  System.out.println("Emitted armed_4");
                  active[9]=1;
                  ends[9]=1;
                  tdone[9]=1;
                }
                break;
              
            }
            break;
          
          case 1 : 
            S1110=1;
            S1110=0;
            if(count_thread_9 != 0){//R1.sysj line: 70, column: 11
              S1109=0;
              active[9]=1;
              ends[9]=1;
              tdone[9]=1;
            }
            else {
              S1109=1;
              armed_4.setPresent();//R1.sysj line: 83, column: 5
              currsigs.addElement(armed_4);
              System.out.println("Emitted armed_4");
              active[9]=1;
              ends[9]=1;
              tdone[9]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1793(int [] tdone, int [] ends){
        switch(S1407){
      case 0 : 
        active[8]=0;
        ends[8]=0;
        tdone[8]=1;
        break;
      
      case 1 : 
        switch(S1201){
          case 0 : 
            if(RRC_4.getprestatus()){//R1.sysj line: 66, column: 8
              S1201=1;
              active[8]=1;
              ends[8]=1;
              tdone[8]=1;
            }
            else {
              thread1794(tdone,ends);
              thread1795(tdone,ends);
              int biggest1796 = 0;
              if(ends[9]>=biggest1796){
                biggest1796=ends[9];
              }
              if(ends[10]>=biggest1796){
                biggest1796=ends[10];
              }
              if(biggest1796 == 1){
                active[8]=1;
                ends[8]=1;
                tdone[8]=1;
              }
              //FINXME code
              if(biggest1796 == 0){
                S1201=1;
                active[8]=1;
                ends[8]=1;
                tdone[8]=1;
              }
            }
            break;
          
          case 1 : 
            S1201=1;
            S1201=0;
            thread1797(tdone,ends);
            thread1798(tdone,ends);
            int biggest1799 = 0;
            if(ends[9]>=biggest1799){
              biggest1799=ends[9];
            }
            if(ends[10]>=biggest1799){
              biggest1799=ends[10];
            }
            if(biggest1799 == 1){
              active[8]=1;
              ends[8]=1;
              tdone[8]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1791(int [] tdone, int [] ends){
        switch(S1097){
      case 0 : 
        active[7]=0;
        ends[7]=0;
        tdone[7]=1;
        break;
      
      case 1 : 
        switch(S839){
          case 0 : 
            if(tempsigyo_4.getprestatus()){//R1.sysj line: 51, column: 10
              S839=1;
              S799=0;
              if(Emergency2_o.get_preempted()){//R1.sysj line: 53, column: 4
                tutu__816 = 0;//R1.sysj line: 53, column: 4
                tutu__816 = Emergency2_o.get_preempted() ? Emergency2_o.refresh() : 0;//R1.sysj line: 53, column: 4
                S799=1;
                active[7]=1;
                ends[7]=1;
                tdone[7]=1;
              }
              else {
                S794=0;
                if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                  tutu__815 = 0;//R1.sysj line: 53, column: 4
                  tutu__815 = Emergency2_o.get_w_s();//R1.sysj line: 53, column: 4
                  tutu__815++;//R1.sysj line: 53, column: 4
                                    Emergency2_o.set_w_s(tutu__815);//R1.sysj line: 53, column: 4
                                    Emergency2_o.set_value(true);//R1.sysj line: 53, column: 4
                  S794=1;
                  if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                    ends[7]=2;
                    ;//R1.sysj line: 53, column: 4
                    S839=2;
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                  else {
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                }
                else {
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
              }
            }
            else {
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            break;
          
          case 1 : 
            switch(S799){
              case 0 : 
                if(Emergency2_o.get_preempted()){//R1.sysj line: 53, column: 4
                  tutu__792 = 0;//R1.sysj line: 53, column: 4
                  tutu__792 = Emergency2_o.get_preempted() ? Emergency2_o.refresh() : 0;//R1.sysj line: 53, column: 4
                  S799=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  switch(S794){
                    case 0 : 
                      if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                        tutu__791 = 0;//R1.sysj line: 53, column: 4
                        tutu__791 = Emergency2_o.get_w_s();//R1.sysj line: 53, column: 4
                        tutu__791++;//R1.sysj line: 53, column: 4
                                                Emergency2_o.set_w_s(tutu__791);//R1.sysj line: 53, column: 4
                                                Emergency2_o.set_value(true);//R1.sysj line: 53, column: 4
                        S794=1;
                        if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                          ends[7]=2;
                          ;//R1.sysj line: 53, column: 4
                          S839=2;
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                        else {
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                        ends[7]=2;
                        ;//R1.sysj line: 53, column: 4
                        S839=2;
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S799=1;
                S799=0;
                if(Emergency2_o.get_preempted()){//R1.sysj line: 53, column: 4
                  tutu__792 = 0;//R1.sysj line: 53, column: 4
                  tutu__792 = Emergency2_o.get_preempted() ? Emergency2_o.refresh() : 0;//R1.sysj line: 53, column: 4
                  S799=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  S794=0;
                  if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                    tutu__791 = 0;//R1.sysj line: 53, column: 4
                    tutu__791 = Emergency2_o.get_w_s();//R1.sysj line: 53, column: 4
                    tutu__791++;//R1.sysj line: 53, column: 4
                                        Emergency2_o.set_w_s(tutu__791);//R1.sysj line: 53, column: 4
                                        Emergency2_o.set_value(true);//R1.sysj line: 53, column: 4
                    S794=1;
                    if(Emergency2_o.get_w_s() == Emergency2_o.get_w_r()){//R1.sysj line: 53, column: 4
                      ends[7]=2;
                      ;//R1.sysj line: 53, column: 4
                      S839=2;
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                    else {
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                  }
                  else {
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 2 : 
            S839=2;
            S839=3;
            S848=0;
            if(ActivateAlarm2_o.get_preempted()){//R1.sysj line: 55, column: 4
              tutu__865 = 0;//R1.sysj line: 55, column: 4
              tutu__865 = ActivateAlarm2_o.get_preempted() ? ActivateAlarm2_o.refresh() : 0;//R1.sysj line: 55, column: 4
              S848=1;
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            else {
              S843=0;
              if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                tutu__864 = 0;//R1.sysj line: 55, column: 4
                tutu__864 = ActivateAlarm2_o.get_w_s();//R1.sysj line: 55, column: 4
                tutu__864++;//R1.sysj line: 55, column: 4
                                ActivateAlarm2_o.set_w_s(tutu__864);//R1.sysj line: 55, column: 4
                                ActivateAlarm2_o.set_value(true);//R1.sysj line: 55, column: 4
                S843=1;
                if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                  ends[7]=2;
                  ;//R1.sysj line: 55, column: 4
                  S839=4;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
              }
              else {
                active[7]=1;
                ends[7]=1;
                tdone[7]=1;
              }
            }
            break;
          
          case 3 : 
            switch(S848){
              case 0 : 
                if(ActivateAlarm2_o.get_preempted()){//R1.sysj line: 55, column: 4
                  tutu__841 = 0;//R1.sysj line: 55, column: 4
                  tutu__841 = ActivateAlarm2_o.get_preempted() ? ActivateAlarm2_o.refresh() : 0;//R1.sysj line: 55, column: 4
                  S848=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  switch(S843){
                    case 0 : 
                      if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                        tutu__840 = 0;//R1.sysj line: 55, column: 4
                        tutu__840 = ActivateAlarm2_o.get_w_s();//R1.sysj line: 55, column: 4
                        tutu__840++;//R1.sysj line: 55, column: 4
                                                ActivateAlarm2_o.set_w_s(tutu__840);//R1.sysj line: 55, column: 4
                                                ActivateAlarm2_o.set_value(true);//R1.sysj line: 55, column: 4
                        S843=1;
                        if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                          ends[7]=2;
                          ;//R1.sysj line: 55, column: 4
                          S839=4;
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                        else {
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                        ends[7]=2;
                        ;//R1.sysj line: 55, column: 4
                        S839=4;
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S848=1;
                S848=0;
                if(ActivateAlarm2_o.get_preempted()){//R1.sysj line: 55, column: 4
                  tutu__841 = 0;//R1.sysj line: 55, column: 4
                  tutu__841 = ActivateAlarm2_o.get_preempted() ? ActivateAlarm2_o.refresh() : 0;//R1.sysj line: 55, column: 4
                  S848=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  S843=0;
                  if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                    tutu__840 = 0;//R1.sysj line: 55, column: 4
                    tutu__840 = ActivateAlarm2_o.get_w_s();//R1.sysj line: 55, column: 4
                    tutu__840++;//R1.sysj line: 55, column: 4
                                        ActivateAlarm2_o.set_w_s(tutu__840);//R1.sysj line: 55, column: 4
                                        ActivateAlarm2_o.set_value(true);//R1.sysj line: 55, column: 4
                    S843=1;
                    if(ActivateAlarm2_o.get_w_s() == ActivateAlarm2_o.get_w_r()){//R1.sysj line: 55, column: 4
                      ends[7]=2;
                      ;//R1.sysj line: 55, column: 4
                      S839=4;
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                    else {
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                  }
                  else {
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 4 : 
            if(RRC_4.getprestatus()){//R1.sysj line: 57, column: 10
              tempsigyo2_4.setPresent();//R1.sysj line: 58, column: 4
              currsigs.addElement(tempsigyo2_4);
              System.out.println("Emitted tempsigyo2_4");
              S839=0;
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            else {
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread1778(int [] tdone, int [] ends){
        switch(S789){
      case 0 : 
        active[6]=0;
        ends[6]=0;
        tdone[6]=1;
        break;
      
      case 1 : 
        switch(S160){
          case 0 : 
            switch(S74){
              case 0 : 
                if(Presence_1.getprestatus()){//R1.sysj line: 16, column: 12
                  S74=1;
                  if(armed_4.getprestatus()){//R1.sysj line: 17, column: 14
                    if(disarmed_4.getprestatus()){//R1.sysj line: 19, column: 24
                      S74=2;
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                    else {
                      i_thread_6 = 0;//R1.sysj line: 21, column: 3
                      if(i_thread_6 == 4){//R1.sysj line: 24, column: 10
                        ends[6]=3;
                        ;//R1.sysj line: 22, column: 3
                        ends[6]=2;
                        ;//R1.sysj line: 13, column: 2
                        tempsigyo_4.setPresent();//R1.sysj line: 43, column: 2
                        currsigs.addElement(tempsigyo_4);
                        System.out.println("Emitted tempsigyo_4");
                        S160=1;
                        if(tempsigyo2_4.getprestatus()){//R1.sysj line: 44, column: 18
                          S160=2;
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                        else {
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                      }
                      else {
                        i_thread_6 = i_thread_6 + 1;//R1.sysj line: 25, column: 7
                        active[6]=1;
                        ends[6]=1;
                        tdone[6]=1;
                      }
                    }
                  }
                  else {
                    S74=2;
                    active[6]=1;
                    ends[6]=1;
                    tdone[6]=1;
                  }
                }
                else {
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                break;
              
              case 1 : 
                if(disarmed_4.getprestatus()){//R1.sysj line: 19, column: 24
                  S74=2;
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                else {
                  if(i_thread_6 == 4){//R1.sysj line: 24, column: 10
                    ends[6]=3;
                    ;//R1.sysj line: 22, column: 3
                    ends[6]=2;
                    ;//R1.sysj line: 13, column: 2
                    tempsigyo_4.setPresent();//R1.sysj line: 43, column: 2
                    currsigs.addElement(tempsigyo_4);
                    System.out.println("Emitted tempsigyo_4");
                    S160=1;
                    if(tempsigyo2_4.getprestatus()){//R1.sysj line: 44, column: 18
                      S160=2;
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                    else {
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                  }
                  else {
                    i_thread_6 = i_thread_6 + 1;//R1.sysj line: 25, column: 7
                    active[6]=1;
                    ends[6]=1;
                    tdone[6]=1;
                  }
                }
                break;
              
              case 2 : 
                if(LightLevel_1.getprestatus()){//R1.sysj line: 36, column: 12
                  CurrentLightLevel_thread_6 = (Integer)LightLevel_1.getpreval();//R1.sysj line: 37, column: 6
                  SetValue_thread_6 = LightControl.adjustLight(CurrentLightLevel_thread_6);//R1.sysj line: 38, column: 6
                  SetIntensity_1.setPresent();//R1.sysj line: 39, column: 6
                  currsigs.addElement(SetIntensity_1);
                  SetIntensity_1.setValue(SetValue_thread_6);//R1.sysj line: 39, column: 6
                  System.out.println("Emitted SetIntensity_1");
                  S74=3;
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                else {
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                break;
              
              case 3 : 
                S74=3;
                S74=0;
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
                break;
              
            }
            break;
          
          case 1 : 
            if(tempsigyo2_4.getprestatus()){//R1.sysj line: 44, column: 18
              S160=2;
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            else {
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            break;
          
          case 2 : 
            S160=2;
            S160=0;
            S74=0;
            active[6]=1;
            ends[6]=1;
            tdone[6]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread1777(int [] tdone, int [] ends){
        switch(S1099){
      case 0 : 
        active[5]=0;
        ends[5]=0;
        tdone[5]=1;
        break;
      
      case 1 : 
        thread1778(tdone,ends);
        thread1791(tdone,ends);
        int biggest1792 = 0;
        if(ends[6]>=biggest1792){
          biggest1792=ends[6];
        }
        if(ends[7]>=biggest1792){
          biggest1792=ends[7];
        }
        if(biggest1792 == 1){
          active[5]=1;
          ends[5]=1;
          tdone[5]=1;
        }
        //FINXME code
        if(biggest1792 == 0){
          S1099=0;
          active[5]=0;
          ends[5]=0;
          tdone[5]=1;
        }
        break;
      
    }
  }

  public void thread1776(int [] tdone, int [] ends){
        switch(S1730){
      case 0 : 
        active[4]=0;
        ends[4]=0;
        tdone[4]=1;
        break;
      
      case 1 : 
        armed_4.setClear();//R1.sysj line: 6, column: 3
        disarmed_4.setClear();//R1.sysj line: 6, column: 3
        INC_4.setClear();//R1.sysj line: 6, column: 3
        DEC_4.setClear();//R1.sysj line: 6, column: 3
        RRC_4.setClear();//R1.sysj line: 6, column: 3
        tempsigyo_4.setClear();//R1.sysj line: 6, column: 3
        tempsigyo2_4.setClear();//R1.sysj line: 6, column: 3
        switch(S1729){
          case 0 : 
            thread1777(tdone,ends);
            thread1793(tdone,ends);
            thread1800(tdone,ends);
            thread1801(tdone,ends);
            thread1802(tdone,ends);
            int biggest1803 = 0;
            if(ends[5]>=biggest1803){
              biggest1803=ends[5];
            }
            if(ends[8]>=biggest1803){
              biggest1803=ends[8];
            }
            if(ends[11]>=biggest1803){
              biggest1803=ends[11];
            }
            if(ends[12]>=biggest1803){
              biggest1803=ends[12];
            }
            if(ends[13]>=biggest1803){
              biggest1803=ends[13];
            }
            if(biggest1803 == 1){
              active[4]=1;
              ends[4]=1;
              tdone[4]=1;
            }
            //FINXME code
            if(biggest1803 == 0){
              S1729=1;
              active[4]=1;
              ends[4]=1;
              tdone[4]=1;
            }
            break;
          
          case 1 : 
            S1729=1;
            S1730=0;
            active[4]=0;
            ends[4]=0;
            tdone[4]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread1763(int [] tdone, int [] ends){
        switch(S29){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        LightLevel_1.setPresent();//R1.sysj line: 167, column: 7
        currsigs.addElement(LightLevel_1);
        LightLevel_1.setValue(2);//R1.sysj line: 167, column: 7
        System.out.println("Emitted LightLevel_1");
        active[3]=1;
        ends[3]=1;
        tdone[3]=1;
        break;
      
    }
  }

  public void thread1762(int [] tdone, int [] ends){
        switch(S24){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        switch(S7){
          case 0 : 
            S7=0;
            S7=1;
            if(random_thread_2.nextInt(2) == 0){//R1.sysj line: 160, column: 5
              Presence_1.setPresent();//R1.sysj line: 161, column: 4
              currsigs.addElement(Presence_1);
              System.out.println("Emitted Presence_1");
              S7=2;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            else {
              S7=2;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            break;
          
          case 1 : 
            S7=0;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
          case 2 : 
            S7=2;
            S7=0;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread1759(int [] tdone, int [] ends){
        S1728=1;
    val_thread_13 = null;//R1.sysj line: 134, column: 8
    S1652=0;
    S1627=0;
    if(ResetRC2_in.get_preempted()){//R1.sysj line: 135, column: 8
      tutu__1638 = 0;//R1.sysj line: 135, column: 8
      tutu__1638 = ResetRC2_in.get_preempted() ? ResetRC2_in.refresh() : 0;//R1.sysj line: 135, column: 8
      S1627=1;
      active[13]=1;
      ends[13]=1;
      tdone[13]=1;
    }
    else {
      if(ResetRC2_in.get_r_s() > ResetRC2_in.get_r_r()){//R1.sysj line: 135, column: 8
        tutu__1637 = 0;//R1.sysj line: 135, column: 8
        tutu__1637 = ResetRC2_in.get_r_r();//R1.sysj line: 135, column: 8
        tutu__1637++;//R1.sysj line: 135, column: 8
        ResetRC2_in.set_r_r(tutu__1637);//R1.sysj line: 135, column: 8
        ResetRC2_in.get_val();//R1.sysj line: 135, column: 8
        ends[13]=2;
        ;//R1.sysj line: 135, column: 8
        val_thread_13 = (Boolean)ResetRC2_in.get_value();//R1.sysj line: 136, column: 8
        if(val_thread_13.getprestatus()){//R1.sysj line: 137, column: 8
          RRC_4.setPresent();//R1.sysj line: 138, column: 3
          currsigs.addElement(RRC_4);
          System.out.println("Emitted RRC_4");
          S1652=1;
          active[13]=1;
          ends[13]=1;
          tdone[13]=1;
        }
        else {
          S1652=1;
          active[13]=1;
          ends[13]=1;
          tdone[13]=1;
        }
      }
      else {
        active[13]=1;
        ends[13]=1;
        tdone[13]=1;
      }
    }
  }

  public void thread1758(int [] tdone, int [] ends){
        S1621=1;
    val_thread_12 = null;//R1.sysj line: 122, column: 6
    S1545=0;
    S1520=0;
    if(DecreaseCount2_in.get_preempted()){//R1.sysj line: 123, column: 6
      tutu__1531 = 0;//R1.sysj line: 123, column: 6
      tutu__1531 = DecreaseCount2_in.get_preempted() ? DecreaseCount2_in.refresh() : 0;//R1.sysj line: 123, column: 6
      S1520=1;
      active[12]=1;
      ends[12]=1;
      tdone[12]=1;
    }
    else {
      if(DecreaseCount2_in.get_r_s() > DecreaseCount2_in.get_r_r()){//R1.sysj line: 123, column: 6
        tutu__1530 = 0;//R1.sysj line: 123, column: 6
        tutu__1530 = DecreaseCount2_in.get_r_r();//R1.sysj line: 123, column: 6
        tutu__1530++;//R1.sysj line: 123, column: 6
        DecreaseCount2_in.set_r_r(tutu__1530);//R1.sysj line: 123, column: 6
        DecreaseCount2_in.get_val();//R1.sysj line: 123, column: 6
        ends[12]=2;
        ;//R1.sysj line: 123, column: 6
        val_thread_12 = (Boolean)DecreaseCount2_in.get_value();//R1.sysj line: 124, column: 6
        if(val_thread_12.getprestatus()){//R1.sysj line: 125, column: 6
          DEC_4.setPresent();//R1.sysj line: 126, column: 8
          currsigs.addElement(DEC_4);
          System.out.println("Emitted DEC_4");
          S1545=1;
          active[12]=1;
          ends[12]=1;
          tdone[12]=1;
        }
        else {
          S1545=1;
          active[12]=1;
          ends[12]=1;
          tdone[12]=1;
        }
      }
      else {
        active[12]=1;
        ends[12]=1;
        tdone[12]=1;
      }
    }
  }

  public void thread1757(int [] tdone, int [] ends){
        S1514=1;
    val_thread_11 = null;//R1.sysj line: 110, column: 4
    S1438=0;
    S1413=0;
    if(IncreaseCount2_in.get_preempted()){//R1.sysj line: 111, column: 4
      tutu__1424 = 0;//R1.sysj line: 111, column: 4
      tutu__1424 = IncreaseCount2_in.get_preempted() ? IncreaseCount2_in.refresh() : 0;//R1.sysj line: 111, column: 4
      S1413=1;
      active[11]=1;
      ends[11]=1;
      tdone[11]=1;
    }
    else {
      if(IncreaseCount2_in.get_r_s() > IncreaseCount2_in.get_r_r()){//R1.sysj line: 111, column: 4
        tutu__1423 = 0;//R1.sysj line: 111, column: 4
        tutu__1423 = IncreaseCount2_in.get_r_r();//R1.sysj line: 111, column: 4
        tutu__1423++;//R1.sysj line: 111, column: 4
        IncreaseCount2_in.set_r_r(tutu__1423);//R1.sysj line: 111, column: 4
        IncreaseCount2_in.get_val();//R1.sysj line: 111, column: 4
        ends[11]=2;
        ;//R1.sysj line: 111, column: 4
        val_thread_11 = (Boolean)IncreaseCount2_in.get_value();//R1.sysj line: 112, column: 4
        if(val_thread_11.getprestatus()){//R1.sysj line: 113, column: 4
          INC_4.setPresent();//R1.sysj line: 114, column: 6
          currsigs.addElement(INC_4);
          System.out.println("Emitted INC_4");
          S1438=1;
          active[11]=1;
          ends[11]=1;
          tdone[11]=1;
        }
        else {
          S1438=1;
          active[11]=1;
          ends[11]=1;
          tdone[11]=1;
        }
      }
      else {
        active[11]=1;
        ends[11]=1;
        tdone[11]=1;
      }
    }
  }

  public void thread1755(int [] tdone, int [] ends){
        S1199=1;
    S1168=0;
    active[10]=1;
    ends[10]=1;
    tdone[10]=1;
  }

  public void thread1754(int [] tdone, int [] ends){
        S1167=1;
    count_thread_9 = 0;//R1.sysj line: 68, column: 6
    S1110=0;
    if(count_thread_9 != 0){//R1.sysj line: 70, column: 11
      S1109=0;
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
    else {
      S1109=1;
      armed_4.setPresent();//R1.sysj line: 83, column: 5
      currsigs.addElement(armed_4);
      System.out.println("Emitted armed_4");
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
  }

  public void thread1753(int [] tdone, int [] ends){
        S1407=1;
    S1201=0;
    thread1754(tdone,ends);
    thread1755(tdone,ends);
    int biggest1756 = 0;
    if(ends[9]>=biggest1756){
      biggest1756=ends[9];
    }
    if(ends[10]>=biggest1756){
      biggest1756=ends[10];
    }
    if(biggest1756 == 1){
      active[8]=1;
      ends[8]=1;
      tdone[8]=1;
    }
  }

  public void thread1751(int [] tdone, int [] ends){
        S1097=1;
    S839=0;
    active[7]=1;
    ends[7]=1;
    tdone[7]=1;
  }

  public void thread1750(int [] tdone, int [] ends){
        S789=1;
    SetValue_thread_6 = 10;//R1.sysj line: 10, column: 7
    CurrentLightLevel_thread_6 = 0;//R1.sysj line: 11, column: 7
    S160=0;
    S74=0;
    active[6]=1;
    ends[6]=1;
    tdone[6]=1;
  }

  public void thread1749(int [] tdone, int [] ends){
        S1099=1;
    thread1750(tdone,ends);
    thread1751(tdone,ends);
    int biggest1752 = 0;
    if(ends[6]>=biggest1752){
      biggest1752=ends[6];
    }
    if(ends[7]>=biggest1752){
      biggest1752=ends[7];
    }
    if(biggest1752 == 1){
      active[5]=1;
      ends[5]=1;
      tdone[5]=1;
    }
  }

  public void thread1748(int [] tdone, int [] ends){
        S1730=1;
    armed_4.setClear();//R1.sysj line: 6, column: 3
    disarmed_4.setClear();//R1.sysj line: 6, column: 3
    INC_4.setClear();//R1.sysj line: 6, column: 3
    DEC_4.setClear();//R1.sysj line: 6, column: 3
    RRC_4.setClear();//R1.sysj line: 6, column: 3
    tempsigyo_4.setClear();//R1.sysj line: 6, column: 3
    tempsigyo2_4.setClear();//R1.sysj line: 6, column: 3
    S1729=0;
    thread1749(tdone,ends);
    thread1753(tdone,ends);
    thread1757(tdone,ends);
    thread1758(tdone,ends);
    thread1759(tdone,ends);
    int biggest1760 = 0;
    if(ends[5]>=biggest1760){
      biggest1760=ends[5];
    }
    if(ends[8]>=biggest1760){
      biggest1760=ends[8];
    }
    if(ends[11]>=biggest1760){
      biggest1760=ends[11];
    }
    if(ends[12]>=biggest1760){
      biggest1760=ends[12];
    }
    if(ends[13]>=biggest1760){
      biggest1760=ends[13];
    }
    if(biggest1760 == 1){
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread1735(int [] tdone, int [] ends){
        S29=1;
    LightLevel_1.setPresent();//R1.sysj line: 167, column: 7
    currsigs.addElement(LightLevel_1);
    LightLevel_1.setValue(2);//R1.sysj line: 167, column: 7
    System.out.println("Emitted LightLevel_1");
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread1734(int [] tdone, int [] ends){
        S24=1;
    random_thread_2 = new Random(320543853);//R1.sysj line: 157, column: 7
    S7=0;
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S1732){
        case 0 : 
          S1732=0;
          break RUN;
        
        case 1 : 
          S1732=2;
          S1732=2;
          Presence_1.setClear();//R1.sysj line: 150, column: 3
          TimeOutRC_1.setClear();//R1.sysj line: 150, column: 3
          TriggerRC_1.setClear();//R1.sysj line: 150, column: 3
          LightLevel_1.setClear();//R1.sysj line: 151, column: 3
          SetIntensity_1.setClear();//R1.sysj line: 151, column: 3
          thread1734(tdone,ends);
          thread1735(tdone,ends);
          thread1748(tdone,ends);
          int biggest1761 = 0;
          if(ends[2]>=biggest1761){
            biggest1761=ends[2];
          }
          if(ends[3]>=biggest1761){
            biggest1761=ends[3];
          }
          if(ends[4]>=biggest1761){
            biggest1761=ends[4];
          }
          if(biggest1761 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          if(biggest1761 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          Presence_1.setClear();//R1.sysj line: 150, column: 3
          TimeOutRC_1.setClear();//R1.sysj line: 150, column: 3
          TriggerRC_1.setClear();//R1.sysj line: 150, column: 3
          LightLevel_1.setClear();//R1.sysj line: 151, column: 3
          SetIntensity_1.setClear();//R1.sysj line: 151, column: 3
          thread1762(tdone,ends);
          thread1763(tdone,ends);
          thread1776(tdone,ends);
          int biggest1804 = 0;
          if(ends[2]>=biggest1804){
            biggest1804=ends[2];
          }
          if(ends[3]>=biggest1804){
            biggest1804=ends[3];
          }
          if(ends[4]>=biggest1804){
            biggest1804=ends[4];
          }
          if(biggest1804 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          //FINXME code
          if(biggest1804 == 0){
            S1732=0;
            active[1]=0;
            ends[1]=0;
            S1732=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    Presence_1 = new Signal();
    TimeOutRC_1 = new Signal();
    TriggerRC_1 = new Signal();
    LightLevel_1 = new Signal();
    SetIntensity_1 = new Signal();
    armed_4 = new Signal();
    disarmed_4 = new Signal();
    INC_4 = new Signal();
    DEC_4 = new Signal();
    RRC_4 = new Signal();
    tempsigyo_4 = new Signal();
    tempsigyo2_4 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        ResetRC2_in.update_r_s();
        IncreaseCount2_in.update_r_s();
        DecreaseCount2_in.update_r_s();
        Emergency2_o.update_w_r();
        ActivateAlarm2_o.update_w_r();
        if(!df){
          ResetRC2_in.gethook();
          IncreaseCount2_in.gethook();
          DecreaseCount2_in.gethook();
          Emergency2_o.gethook();
          ActivateAlarm2_o.gethook();
          df = true;
        }
        runClockDomain();
      }
      Presence_1.setpreclear();
      TimeOutRC_1.setpreclear();
      TriggerRC_1.setpreclear();
      LightLevel_1.setpreclear();
      SetIntensity_1.setpreclear();
      armed_4.setpreclear();
      disarmed_4.setpreclear();
      INC_4.setpreclear();
      DEC_4.setpreclear();
      RRC_4.setpreclear();
      tempsigyo_4.setpreclear();
      tempsigyo2_4.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      Presence_1.setClear();
      TimeOutRC_1.setClear();
      TriggerRC_1.setClear();
      LightLevel_1.setClear();
      SetIntensity_1.setClear();
      armed_4.setClear();
      disarmed_4.setClear();
      INC_4.setClear();
      DEC_4.setClear();
      RRC_4.setClear();
      tempsigyo_4.setClear();
      tempsigyo2_4.setClear();
      ResetRC2_in.sethook();
      IncreaseCount2_in.sethook();
      DecreaseCount2_in.sethook();
      Emergency2_o.sethook();
      ActivateAlarm2_o.sethook();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        ResetRC2_in.gethook();
        IncreaseCount2_in.gethook();
        DecreaseCount2_in.gethook();
        Emergency2_o.gethook();
        ActivateAlarm2_o.gethook();
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
