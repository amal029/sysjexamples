import java.util.*;
import com.systemj.ClockDomain;
import com.systemj.Signal;
import com.systemj.input_Channel;
import com.systemj.output_Channel;
import UtilA.*;//RoomControl.sysj line: 1, column: 1

public class RoomC extends ClockDomain{
  public RoomC(String name){super(name);}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  public input_Channel ResetRC2_in = new input_Channel();
  public input_Channel IncreaseCount2_in = new input_Channel();
  public input_Channel DecreaseCount2_in = new input_Channel();
  public output_Channel Emergency2_o = new output_Channel();
  public output_Channel ActivateAlarm2_o = new output_Channel();
  private Signal Presence_1;
  private Signal TimeOutRC_1;
  private Signal TriggerRC_1;
  private Signal LightLevel_1;
  private Signal SetIntensity_1;
  private Signal armed_4;
  private Signal disarmed_4;
  private Signal INC_4;
  private Signal DEC_4;
  private Signal RRC_4;
  private Signal tempsigyo_4;
  private Signal tempsigyo2_4;
  private Random random_thread_2;//RoomControl.sysj line: 307, column: 7
  private Integer SetValue_thread_6;//RoomControl.sysj line: 11, column: 7
  private Integer CurrentLightLevel_thread_6;//RoomControl.sysj line: 12, column: 7
  private int i_thread_6;//RoomControl.sysj line: 22, column: 3
  private int count_thread_9;//RoomControl.sysj line: 69, column: 6
  private Boolean val_thread_11;//RoomControl.sysj line: 111, column: 4
  private Boolean val_thread_12;//RoomControl.sysj line: 123, column: 6
  private Boolean val_thread_13;//RoomControl.sysj line: 135, column: 8
  private int S2494 = 1;
  private int S24 = 1;
  private int S7 = 1;
  private int S29 = 1;
  private int S2492 = 1;
  private int S2491 = 1;
  private int S1339 = 1;
  private int S789 = 1;
  private int S160 = 1;
  private int S74 = 1;
  private int S1337 = 1;
  private int S879 = 1;
  private int S797 = 1;
  private int S792 = 1;
  private int S886 = 1;
  private int S881 = 1;
  private int S1647 = 1;
  private int S1441 = 1;
  private int S1407 = 1;
  private int S1350 = 1;
  private int S1349 = 1;
  private int S1439 = 1;
  private int S1408 = 1;
  private int S1413 = 1;
  private int S1928 = 1;
  private int S1736 = 1;
  private int S1654 = 1;
  private int S1649 = 1;
  private int S2209 = 1;
  private int S2017 = 1;
  private int S1935 = 1;
  private int S1930 = 1;
  private int S2490 = 1;
  private int S2298 = 1;
  private int S2216 = 1;
  private int S2211 = 1;
  
  private int[] ends = new int[33];
  private int[] tdone = new int[33];
  
  public void thread26306(int [] tdone, int [] ends){
        switch(S2490){
      case 0 : 
        active[13]=0;
        ends[13]=0;
        tdone[13]=1;
        break;
      
      case 1 : 
        switch(S2298){
          case 0 : 
            switch(S2216){
              case 0 : 
                if(!ResetRC2_in.isPartnerPresent() || ResetRC2_in.isPartnerPreempted()){//RoomControl.sysj line: 136, column: 8
                  ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
                  S2216=1;
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
                else {
                  switch(S2211){
                    case 0 : 
                      if(!ResetRC2_in.isREQ()){//Unknown file line: 0, column: 0
                        ResetRC2_in.setACK(true);//RoomControl.sysj line: 136, column: 8
                        S2211=1;
                        if(ResetRC2_in.isREQ()){//RoomControl.sysj line: 136, column: 8
                          ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
                          ends[13]=2;
                          ;//RoomControl.sysj line: 136, column: 8
                          val_thread_13 = (Boolean)ResetRC2_in.getVal();//RoomControl.sysj line: 137, column: 8
                          if(val_thread_13){//RoomControl.sysj line: 138, column: 8
                            RRC_4.setPresent();//RoomControl.sysj line: 139, column: 3
                            currsigs.addElement(RRC_4);
                            System.out.println("Emitted RRC_4");
                            S2298=1;
                            active[13]=1;
                            ends[13]=1;
                            tdone[13]=1;
                          }
                          else {
                            S2298=1;
                            active[13]=1;
                            ends[13]=1;
                            tdone[13]=1;
                          }
                        }
                        else {
                          active[13]=1;
                          ends[13]=1;
                          tdone[13]=1;
                        }
                      }
                      else {
                        active[13]=1;
                        ends[13]=1;
                        tdone[13]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(ResetRC2_in.isREQ()){//RoomControl.sysj line: 136, column: 8
                        ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
                        ends[13]=2;
                        ;//RoomControl.sysj line: 136, column: 8
                        val_thread_13 = (Boolean)ResetRC2_in.getVal();//RoomControl.sysj line: 137, column: 8
                        if(val_thread_13){//RoomControl.sysj line: 138, column: 8
                          RRC_4.setPresent();//RoomControl.sysj line: 139, column: 3
                          currsigs.addElement(RRC_4);
                          System.out.println("Emitted RRC_4");
                          S2298=1;
                          active[13]=1;
                          ends[13]=1;
                          tdone[13]=1;
                        }
                        else {
                          S2298=1;
                          active[13]=1;
                          ends[13]=1;
                          tdone[13]=1;
                        }
                      }
                      else {
                        active[13]=1;
                        ends[13]=1;
                        tdone[13]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S2216=1;
                S2216=0;
                if(!ResetRC2_in.isPartnerPresent() || ResetRC2_in.isPartnerPreempted()){//RoomControl.sysj line: 136, column: 8
                  ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
                  S2216=1;
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
                else {
                  S2211=0;
                  if(!ResetRC2_in.isREQ()){//Unknown file line: 0, column: 0
                    ResetRC2_in.setACK(true);//RoomControl.sysj line: 136, column: 8
                    S2211=1;
                    if(ResetRC2_in.isREQ()){//RoomControl.sysj line: 136, column: 8
                      ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
                      ends[13]=2;
                      ;//RoomControl.sysj line: 136, column: 8
                      val_thread_13 = (Boolean)ResetRC2_in.getVal();//RoomControl.sysj line: 137, column: 8
                      if(val_thread_13){//RoomControl.sysj line: 138, column: 8
                        RRC_4.setPresent();//RoomControl.sysj line: 139, column: 3
                        currsigs.addElement(RRC_4);
                        System.out.println("Emitted RRC_4");
                        S2298=1;
                        active[13]=1;
                        ends[13]=1;
                        tdone[13]=1;
                      }
                      else {
                        S2298=1;
                        active[13]=1;
                        ends[13]=1;
                        tdone[13]=1;
                      }
                    }
                    else {
                      active[13]=1;
                      ends[13]=1;
                      tdone[13]=1;
                    }
                  }
                  else {
                    active[13]=1;
                    ends[13]=1;
                    tdone[13]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S2298=1;
            val_thread_13 = null;//RoomControl.sysj line: 135, column: 8
            S2298=0;
            ResetRC2_in.setPreempted(false);//RoomControl.sysj line: 136, column: 8
            S2216=0;
            if(!ResetRC2_in.isPartnerPresent() || ResetRC2_in.isPartnerPreempted()){//RoomControl.sysj line: 136, column: 8
              ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
              S2216=1;
              active[13]=1;
              ends[13]=1;
              tdone[13]=1;
            }
            else {
              S2211=0;
              if(!ResetRC2_in.isREQ()){//Unknown file line: 0, column: 0
                ResetRC2_in.setACK(true);//RoomControl.sysj line: 136, column: 8
                S2211=1;
                if(ResetRC2_in.isREQ()){//RoomControl.sysj line: 136, column: 8
                  ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
                  ends[13]=2;
                  ;//RoomControl.sysj line: 136, column: 8
                  val_thread_13 = (Boolean)ResetRC2_in.getVal();//RoomControl.sysj line: 137, column: 8
                  if(val_thread_13){//RoomControl.sysj line: 138, column: 8
                    RRC_4.setPresent();//RoomControl.sysj line: 139, column: 3
                    currsigs.addElement(RRC_4);
                    System.out.println("Emitted RRC_4");
                    S2298=1;
                    active[13]=1;
                    ends[13]=1;
                    tdone[13]=1;
                  }
                  else {
                    S2298=1;
                    active[13]=1;
                    ends[13]=1;
                    tdone[13]=1;
                  }
                }
                else {
                  active[13]=1;
                  ends[13]=1;
                  tdone[13]=1;
                }
              }
              else {
                active[13]=1;
                ends[13]=1;
                tdone[13]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26305(int [] tdone, int [] ends){
        switch(S2209){
      case 0 : 
        active[12]=0;
        ends[12]=0;
        tdone[12]=1;
        break;
      
      case 1 : 
        switch(S2017){
          case 0 : 
            switch(S1935){
              case 0 : 
                if(!DecreaseCount2_in.isPartnerPresent() || DecreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 124, column: 6
                  DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
                  S1935=1;
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
                else {
                  switch(S1930){
                    case 0 : 
                      if(!DecreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
                        DecreaseCount2_in.setACK(true);//RoomControl.sysj line: 124, column: 6
                        S1930=1;
                        if(DecreaseCount2_in.isREQ()){//RoomControl.sysj line: 124, column: 6
                          DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
                          ends[12]=2;
                          ;//RoomControl.sysj line: 124, column: 6
                          val_thread_12 = (Boolean)DecreaseCount2_in.getVal();//RoomControl.sysj line: 125, column: 6
                          if(val_thread_12){//RoomControl.sysj line: 126, column: 6
                            DEC_4.setPresent();//RoomControl.sysj line: 127, column: 8
                            currsigs.addElement(DEC_4);
                            System.out.println("Emitted DEC_4");
                            S2017=1;
                            active[12]=1;
                            ends[12]=1;
                            tdone[12]=1;
                          }
                          else {
                            S2017=1;
                            active[12]=1;
                            ends[12]=1;
                            tdone[12]=1;
                          }
                        }
                        else {
                          active[12]=1;
                          ends[12]=1;
                          tdone[12]=1;
                        }
                      }
                      else {
                        active[12]=1;
                        ends[12]=1;
                        tdone[12]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(DecreaseCount2_in.isREQ()){//RoomControl.sysj line: 124, column: 6
                        DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
                        ends[12]=2;
                        ;//RoomControl.sysj line: 124, column: 6
                        val_thread_12 = (Boolean)DecreaseCount2_in.getVal();//RoomControl.sysj line: 125, column: 6
                        if(val_thread_12){//RoomControl.sysj line: 126, column: 6
                          DEC_4.setPresent();//RoomControl.sysj line: 127, column: 8
                          currsigs.addElement(DEC_4);
                          System.out.println("Emitted DEC_4");
                          S2017=1;
                          active[12]=1;
                          ends[12]=1;
                          tdone[12]=1;
                        }
                        else {
                          S2017=1;
                          active[12]=1;
                          ends[12]=1;
                          tdone[12]=1;
                        }
                      }
                      else {
                        active[12]=1;
                        ends[12]=1;
                        tdone[12]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S1935=1;
                S1935=0;
                if(!DecreaseCount2_in.isPartnerPresent() || DecreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 124, column: 6
                  DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
                  S1935=1;
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
                else {
                  S1930=0;
                  if(!DecreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
                    DecreaseCount2_in.setACK(true);//RoomControl.sysj line: 124, column: 6
                    S1930=1;
                    if(DecreaseCount2_in.isREQ()){//RoomControl.sysj line: 124, column: 6
                      DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
                      ends[12]=2;
                      ;//RoomControl.sysj line: 124, column: 6
                      val_thread_12 = (Boolean)DecreaseCount2_in.getVal();//RoomControl.sysj line: 125, column: 6
                      if(val_thread_12){//RoomControl.sysj line: 126, column: 6
                        DEC_4.setPresent();//RoomControl.sysj line: 127, column: 8
                        currsigs.addElement(DEC_4);
                        System.out.println("Emitted DEC_4");
                        S2017=1;
                        active[12]=1;
                        ends[12]=1;
                        tdone[12]=1;
                      }
                      else {
                        S2017=1;
                        active[12]=1;
                        ends[12]=1;
                        tdone[12]=1;
                      }
                    }
                    else {
                      active[12]=1;
                      ends[12]=1;
                      tdone[12]=1;
                    }
                  }
                  else {
                    active[12]=1;
                    ends[12]=1;
                    tdone[12]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S2017=1;
            val_thread_12 = null;//RoomControl.sysj line: 123, column: 6
            S2017=0;
            DecreaseCount2_in.setPreempted(false);//RoomControl.sysj line: 124, column: 6
            S1935=0;
            if(!DecreaseCount2_in.isPartnerPresent() || DecreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 124, column: 6
              DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
              S1935=1;
              active[12]=1;
              ends[12]=1;
              tdone[12]=1;
            }
            else {
              S1930=0;
              if(!DecreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
                DecreaseCount2_in.setACK(true);//RoomControl.sysj line: 124, column: 6
                S1930=1;
                if(DecreaseCount2_in.isREQ()){//RoomControl.sysj line: 124, column: 6
                  DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
                  ends[12]=2;
                  ;//RoomControl.sysj line: 124, column: 6
                  val_thread_12 = (Boolean)DecreaseCount2_in.getVal();//RoomControl.sysj line: 125, column: 6
                  if(val_thread_12){//RoomControl.sysj line: 126, column: 6
                    DEC_4.setPresent();//RoomControl.sysj line: 127, column: 8
                    currsigs.addElement(DEC_4);
                    System.out.println("Emitted DEC_4");
                    S2017=1;
                    active[12]=1;
                    ends[12]=1;
                    tdone[12]=1;
                  }
                  else {
                    S2017=1;
                    active[12]=1;
                    ends[12]=1;
                    tdone[12]=1;
                  }
                }
                else {
                  active[12]=1;
                  ends[12]=1;
                  tdone[12]=1;
                }
              }
              else {
                active[12]=1;
                ends[12]=1;
                tdone[12]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26304(int [] tdone, int [] ends){
        switch(S1928){
      case 0 : 
        active[11]=0;
        ends[11]=0;
        tdone[11]=1;
        break;
      
      case 1 : 
        switch(S1736){
          case 0 : 
            switch(S1654){
              case 0 : 
                if(!IncreaseCount2_in.isPartnerPresent() || IncreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 112, column: 4
                  IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
                  S1654=1;
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
                else {
                  switch(S1649){
                    case 0 : 
                      if(!IncreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
                        IncreaseCount2_in.setACK(true);//RoomControl.sysj line: 112, column: 4
                        S1649=1;
                        if(IncreaseCount2_in.isREQ()){//RoomControl.sysj line: 112, column: 4
                          IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
                          ends[11]=2;
                          ;//RoomControl.sysj line: 112, column: 4
                          val_thread_11 = (Boolean)IncreaseCount2_in.getVal();//RoomControl.sysj line: 113, column: 4
                          if(val_thread_11){//RoomControl.sysj line: 114, column: 4
                            INC_4.setPresent();//RoomControl.sysj line: 115, column: 6
                            currsigs.addElement(INC_4);
                            System.out.println("Emitted INC_4");
                            S1736=1;
                            active[11]=1;
                            ends[11]=1;
                            tdone[11]=1;
                          }
                          else {
                            S1736=1;
                            active[11]=1;
                            ends[11]=1;
                            tdone[11]=1;
                          }
                        }
                        else {
                          active[11]=1;
                          ends[11]=1;
                          tdone[11]=1;
                        }
                      }
                      else {
                        active[11]=1;
                        ends[11]=1;
                        tdone[11]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(IncreaseCount2_in.isREQ()){//RoomControl.sysj line: 112, column: 4
                        IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
                        ends[11]=2;
                        ;//RoomControl.sysj line: 112, column: 4
                        val_thread_11 = (Boolean)IncreaseCount2_in.getVal();//RoomControl.sysj line: 113, column: 4
                        if(val_thread_11){//RoomControl.sysj line: 114, column: 4
                          INC_4.setPresent();//RoomControl.sysj line: 115, column: 6
                          currsigs.addElement(INC_4);
                          System.out.println("Emitted INC_4");
                          S1736=1;
                          active[11]=1;
                          ends[11]=1;
                          tdone[11]=1;
                        }
                        else {
                          S1736=1;
                          active[11]=1;
                          ends[11]=1;
                          tdone[11]=1;
                        }
                      }
                      else {
                        active[11]=1;
                        ends[11]=1;
                        tdone[11]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S1654=1;
                S1654=0;
                if(!IncreaseCount2_in.isPartnerPresent() || IncreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 112, column: 4
                  IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
                  S1654=1;
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
                else {
                  S1649=0;
                  if(!IncreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
                    IncreaseCount2_in.setACK(true);//RoomControl.sysj line: 112, column: 4
                    S1649=1;
                    if(IncreaseCount2_in.isREQ()){//RoomControl.sysj line: 112, column: 4
                      IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
                      ends[11]=2;
                      ;//RoomControl.sysj line: 112, column: 4
                      val_thread_11 = (Boolean)IncreaseCount2_in.getVal();//RoomControl.sysj line: 113, column: 4
                      if(val_thread_11){//RoomControl.sysj line: 114, column: 4
                        INC_4.setPresent();//RoomControl.sysj line: 115, column: 6
                        currsigs.addElement(INC_4);
                        System.out.println("Emitted INC_4");
                        S1736=1;
                        active[11]=1;
                        ends[11]=1;
                        tdone[11]=1;
                      }
                      else {
                        S1736=1;
                        active[11]=1;
                        ends[11]=1;
                        tdone[11]=1;
                      }
                    }
                    else {
                      active[11]=1;
                      ends[11]=1;
                      tdone[11]=1;
                    }
                  }
                  else {
                    active[11]=1;
                    ends[11]=1;
                    tdone[11]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S1736=1;
            val_thread_11 = null;//RoomControl.sysj line: 111, column: 4
            S1736=0;
            IncreaseCount2_in.setPreempted(false);//RoomControl.sysj line: 112, column: 4
            S1654=0;
            if(!IncreaseCount2_in.isPartnerPresent() || IncreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 112, column: 4
              IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
              S1654=1;
              active[11]=1;
              ends[11]=1;
              tdone[11]=1;
            }
            else {
              S1649=0;
              if(!IncreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
                IncreaseCount2_in.setACK(true);//RoomControl.sysj line: 112, column: 4
                S1649=1;
                if(IncreaseCount2_in.isREQ()){//RoomControl.sysj line: 112, column: 4
                  IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
                  ends[11]=2;
                  ;//RoomControl.sysj line: 112, column: 4
                  val_thread_11 = (Boolean)IncreaseCount2_in.getVal();//RoomControl.sysj line: 113, column: 4
                  if(val_thread_11){//RoomControl.sysj line: 114, column: 4
                    INC_4.setPresent();//RoomControl.sysj line: 115, column: 6
                    currsigs.addElement(INC_4);
                    System.out.println("Emitted INC_4");
                    S1736=1;
                    active[11]=1;
                    ends[11]=1;
                    tdone[11]=1;
                  }
                  else {
                    S1736=1;
                    active[11]=1;
                    ends[11]=1;
                    tdone[11]=1;
                  }
                }
                else {
                  active[11]=1;
                  ends[11]=1;
                  tdone[11]=1;
                }
              }
              else {
                active[11]=1;
                ends[11]=1;
                tdone[11]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26302(int [] tdone, int [] ends){
        S1439=1;
    S1408=0;
    active[10]=1;
    ends[10]=1;
    tdone[10]=1;
  }

  public void thread26301(int [] tdone, int [] ends){
        S1407=1;
    count_thread_9 = 0;//RoomControl.sysj line: 69, column: 6
    S1350=0;
    if(count_thread_9 != 0){//RoomControl.sysj line: 71, column: 11
      S1349=0;
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
    else {
      S1349=1;
      armed_4.setPresent();//RoomControl.sysj line: 84, column: 5
      currsigs.addElement(armed_4);
      System.out.println("Emitted armed_4");
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
  }

  public void thread26299(int [] tdone, int [] ends){
        switch(S1439){
      case 0 : 
        active[10]=0;
        ends[10]=0;
        tdone[10]=1;
        break;
      
      case 1 : 
        switch(S1408){
          case 0 : 
            S1408=0;
            S1408=1;
            active[10]=1;
            ends[10]=1;
            tdone[10]=1;
            break;
          
          case 1 : 
            S1408=1;
            S1408=2;
            S1413=0;
            if(armed_4.getprestatus()){//RoomControl.sysj line: 97, column: 19
              S1413=1;
              active[10]=1;
              ends[10]=1;
              tdone[10]=1;
            }
            else {
              disarmed_4.setPresent();//RoomControl.sysj line: 98, column: 5
              currsigs.addElement(disarmed_4);
              System.out.println("Emitted disarmed_4");
              active[10]=1;
              ends[10]=1;
              tdone[10]=1;
            }
            break;
          
          case 2 : 
            switch(S1413){
              case 0 : 
                if(armed_4.getprestatus()){//RoomControl.sysj line: 97, column: 19
                  S1413=1;
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                else {
                  disarmed_4.setPresent();//RoomControl.sysj line: 98, column: 5
                  currsigs.addElement(disarmed_4);
                  System.out.println("Emitted disarmed_4");
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                break;
              
              case 1 : 
                S1413=1;
                S1413=0;
                if(armed_4.getprestatus()){//RoomControl.sysj line: 97, column: 19
                  S1413=1;
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                else {
                  disarmed_4.setPresent();//RoomControl.sysj line: 98, column: 5
                  currsigs.addElement(disarmed_4);
                  System.out.println("Emitted disarmed_4");
                  active[10]=1;
                  ends[10]=1;
                  tdone[10]=1;
                }
                break;
              
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26298(int [] tdone, int [] ends){
        switch(S1407){
      case 0 : 
        active[9]=0;
        ends[9]=0;
        tdone[9]=1;
        break;
      
      case 1 : 
        switch(S1350){
          case 0 : 
            switch(S1349){
              case 0 : 
                if(INC_4.getprestatus() || DEC_4.getprestatus()){//RoomControl.sysj line: 73, column: 3
                  if(INC_4.getprestatus()){//RoomControl.sysj line: 74, column: 11
                    count_thread_9 = count_thread_9 + 1;//RoomControl.sysj line: 75, column: 5
                    System.out.println("current count " + count_thread_9);//RoomControl.sysj line: 88, column: 8
                    S1350=1;
                    active[9]=1;
                    ends[9]=1;
                    tdone[9]=1;
                  }
                  else {
                    count_thread_9 = count_thread_9 - 1;//RoomControl.sysj line: 78, column: 5
                    System.out.println("current count " + count_thread_9);//RoomControl.sysj line: 88, column: 8
                    S1350=1;
                    active[9]=1;
                    ends[9]=1;
                    tdone[9]=1;
                  }
                }
                else {
                  active[9]=1;
                  ends[9]=1;
                  tdone[9]=1;
                }
                break;
              
              case 1 : 
                if(INC_4.getprestatus()){//RoomControl.sysj line: 83, column: 9
                  count_thread_9 = count_thread_9 + 1;//RoomControl.sysj line: 86, column: 3
                  System.out.println("current count " + count_thread_9);//RoomControl.sysj line: 88, column: 8
                  S1350=1;
                  active[9]=1;
                  ends[9]=1;
                  tdone[9]=1;
                }
                else {
                  armed_4.setPresent();//RoomControl.sysj line: 84, column: 5
                  currsigs.addElement(armed_4);
                  System.out.println("Emitted armed_4");
                  active[9]=1;
                  ends[9]=1;
                  tdone[9]=1;
                }
                break;
              
            }
            break;
          
          case 1 : 
            S1350=1;
            S1350=0;
            if(count_thread_9 != 0){//RoomControl.sysj line: 71, column: 11
              S1349=0;
              active[9]=1;
              ends[9]=1;
              tdone[9]=1;
            }
            else {
              S1349=1;
              armed_4.setPresent();//RoomControl.sysj line: 84, column: 5
              currsigs.addElement(armed_4);
              System.out.println("Emitted armed_4");
              active[9]=1;
              ends[9]=1;
              tdone[9]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26297(int [] tdone, int [] ends){
        switch(S1647){
      case 0 : 
        active[8]=0;
        ends[8]=0;
        tdone[8]=1;
        break;
      
      case 1 : 
        switch(S1441){
          case 0 : 
            if(RRC_4.getprestatus()){//RoomControl.sysj line: 67, column: 8
              S1441=1;
              active[8]=1;
              ends[8]=1;
              tdone[8]=1;
            }
            else {
              thread26298(tdone,ends);
              thread26299(tdone,ends);
              int biggest26300 = 0;
              if(ends[9]>=biggest26300){
                biggest26300=ends[9];
              }
              if(ends[10]>=biggest26300){
                biggest26300=ends[10];
              }
              if(biggest26300 == 1){
                active[8]=1;
                ends[8]=1;
                tdone[8]=1;
              }
              //FINXME code
              if(biggest26300 == 0){
                S1441=1;
                active[8]=1;
                ends[8]=1;
                tdone[8]=1;
              }
            }
            break;
          
          case 1 : 
            S1441=1;
            S1441=0;
            thread26301(tdone,ends);
            thread26302(tdone,ends);
            int biggest26303 = 0;
            if(ends[9]>=biggest26303){
              biggest26303=ends[9];
            }
            if(ends[10]>=biggest26303){
              biggest26303=ends[10];
            }
            if(biggest26303 == 1){
              active[8]=1;
              ends[8]=1;
              tdone[8]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26295(int [] tdone, int [] ends){
        switch(S1337){
      case 0 : 
        active[7]=0;
        ends[7]=0;
        tdone[7]=1;
        break;
      
      case 1 : 
        switch(S879){
          case 0 : 
            if(tempsigyo_4.getprestatus()){//RoomControl.sysj line: 52, column: 10
              S879=1;
                            Emergency2_o.setPreempted(false);//RoomControl.sysj line: 54, column: 4
              S797=0;
              if(!Emergency2_o.isPartnerPresent() || Emergency2_o.isPartnerPreempted()){//RoomControl.sysj line: 54, column: 4
                                Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                S797=1;
                active[7]=1;
                ends[7]=1;
                tdone[7]=1;
              }
              else {
                S792=0;
                if(Emergency2_o.isACK()){//RoomControl.sysj line: 54, column: 4
                                    Emergency2_o.setVal(true);//RoomControl.sysj line: 54, column: 4
                  S792=1;
                  if(!Emergency2_o.isACK()){//Unknown file line: 0, column: 0
                                        Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                    ends[7]=2;
                    ;//RoomControl.sysj line: 54, column: 4
                    S879=2;
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                  else {
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                }
                else {
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
              }
            }
            else {
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            break;
          
          case 1 : 
            switch(S797){
              case 0 : 
                if(!Emergency2_o.isPartnerPresent() || Emergency2_o.isPartnerPreempted()){//RoomControl.sysj line: 54, column: 4
                                    Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                  S797=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  switch(S792){
                    case 0 : 
                      if(Emergency2_o.isACK()){//RoomControl.sysj line: 54, column: 4
                                                Emergency2_o.setVal(true);//RoomControl.sysj line: 54, column: 4
                        S792=1;
                        if(!Emergency2_o.isACK()){//Unknown file line: 0, column: 0
                                                    Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                          ends[7]=2;
                          ;//RoomControl.sysj line: 54, column: 4
                          S879=2;
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                        else {
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(!Emergency2_o.isACK()){//Unknown file line: 0, column: 0
                                                Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                        ends[7]=2;
                        ;//RoomControl.sysj line: 54, column: 4
                        S879=2;
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S797=1;
                S797=0;
                if(!Emergency2_o.isPartnerPresent() || Emergency2_o.isPartnerPreempted()){//RoomControl.sysj line: 54, column: 4
                                    Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                  S797=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  S792=0;
                  if(Emergency2_o.isACK()){//RoomControl.sysj line: 54, column: 4
                                        Emergency2_o.setVal(true);//RoomControl.sysj line: 54, column: 4
                    S792=1;
                    if(!Emergency2_o.isACK()){//Unknown file line: 0, column: 0
                                            Emergency2_o.setREQ(false);//RoomControl.sysj line: 54, column: 4
                      ends[7]=2;
                      ;//RoomControl.sysj line: 54, column: 4
                      S879=2;
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                    else {
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                  }
                  else {
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 2 : 
            S879=2;
            S879=3;
                        ActivateAlarm2_o.setPreempted(false);//RoomControl.sysj line: 56, column: 4
            S886=0;
            if(!ActivateAlarm2_o.isPartnerPresent() || ActivateAlarm2_o.isPartnerPreempted()){//RoomControl.sysj line: 56, column: 4
                            ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
              S886=1;
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            else {
              S881=0;
              if(ActivateAlarm2_o.isACK()){//RoomControl.sysj line: 56, column: 4
                                ActivateAlarm2_o.setVal(true);//RoomControl.sysj line: 56, column: 4
                S881=1;
                if(!ActivateAlarm2_o.isACK()){//Unknown file line: 0, column: 0
                                    ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
                  ends[7]=2;
                  ;//RoomControl.sysj line: 56, column: 4
                  S879=4;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
              }
              else {
                active[7]=1;
                ends[7]=1;
                tdone[7]=1;
              }
            }
            break;
          
          case 3 : 
            switch(S886){
              case 0 : 
                if(!ActivateAlarm2_o.isPartnerPresent() || ActivateAlarm2_o.isPartnerPreempted()){//RoomControl.sysj line: 56, column: 4
                                    ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
                  S886=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  switch(S881){
                    case 0 : 
                      if(ActivateAlarm2_o.isACK()){//RoomControl.sysj line: 56, column: 4
                                                ActivateAlarm2_o.setVal(true);//RoomControl.sysj line: 56, column: 4
                        S881=1;
                        if(!ActivateAlarm2_o.isACK()){//Unknown file line: 0, column: 0
                                                    ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
                          ends[7]=2;
                          ;//RoomControl.sysj line: 56, column: 4
                          S879=4;
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                        else {
                          active[7]=1;
                          ends[7]=1;
                          tdone[7]=1;
                        }
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(!ActivateAlarm2_o.isACK()){//Unknown file line: 0, column: 0
                                                ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
                        ends[7]=2;
                        ;//RoomControl.sysj line: 56, column: 4
                        S879=4;
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      else {
                        active[7]=1;
                        ends[7]=1;
                        tdone[7]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S886=1;
                S886=0;
                if(!ActivateAlarm2_o.isPartnerPresent() || ActivateAlarm2_o.isPartnerPreempted()){//RoomControl.sysj line: 56, column: 4
                                    ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
                  S886=1;
                  active[7]=1;
                  ends[7]=1;
                  tdone[7]=1;
                }
                else {
                  S881=0;
                  if(ActivateAlarm2_o.isACK()){//RoomControl.sysj line: 56, column: 4
                                        ActivateAlarm2_o.setVal(true);//RoomControl.sysj line: 56, column: 4
                    S881=1;
                    if(!ActivateAlarm2_o.isACK()){//Unknown file line: 0, column: 0
                                            ActivateAlarm2_o.setREQ(false);//RoomControl.sysj line: 56, column: 4
                      ends[7]=2;
                      ;//RoomControl.sysj line: 56, column: 4
                      S879=4;
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                    else {
                      active[7]=1;
                      ends[7]=1;
                      tdone[7]=1;
                    }
                  }
                  else {
                    active[7]=1;
                    ends[7]=1;
                    tdone[7]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 4 : 
            if(RRC_4.getprestatus()){//RoomControl.sysj line: 58, column: 10
              tempsigyo2_4.setPresent();//RoomControl.sysj line: 59, column: 4
              currsigs.addElement(tempsigyo2_4);
              System.out.println("Emitted tempsigyo2_4");
              S879=0;
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            else {
              active[7]=1;
              ends[7]=1;
              tdone[7]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26294(int [] tdone, int [] ends){
        switch(S789){
      case 0 : 
        active[6]=0;
        ends[6]=0;
        tdone[6]=1;
        break;
      
      case 1 : 
        switch(S160){
          case 0 : 
            switch(S74){
              case 0 : 
                if(Presence_1.getprestatus()){//RoomControl.sysj line: 17, column: 12
                  S74=1;
                  if(armed_4.getprestatus()){//RoomControl.sysj line: 18, column: 14
                    if(disarmed_4.getprestatus()){//RoomControl.sysj line: 20, column: 24
                      S74=2;
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                    else {
                      i_thread_6 = 0;//RoomControl.sysj line: 22, column: 3
                      if(i_thread_6 == 4){//RoomControl.sysj line: 25, column: 10
                        ends[6]=3;
                        ;//RoomControl.sysj line: 23, column: 3
                        ends[6]=2;
                        ;//RoomControl.sysj line: 14, column: 2
                        tempsigyo_4.setPresent();//RoomControl.sysj line: 44, column: 2
                        currsigs.addElement(tempsigyo_4);
                        System.out.println("Emitted tempsigyo_4");
                        S160=1;
                        if(tempsigyo2_4.getprestatus()){//RoomControl.sysj line: 45, column: 18
                          S160=2;
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                        else {
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                      }
                      else {
                        i_thread_6 = i_thread_6 + 1;//RoomControl.sysj line: 26, column: 7
                        active[6]=1;
                        ends[6]=1;
                        tdone[6]=1;
                      }
                    }
                  }
                  else {
                    S74=2;
                    active[6]=1;
                    ends[6]=1;
                    tdone[6]=1;
                  }
                }
                else {
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                break;
              
              case 1 : 
                if(disarmed_4.getprestatus()){//RoomControl.sysj line: 20, column: 24
                  S74=2;
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                else {
                  if(i_thread_6 == 4){//RoomControl.sysj line: 25, column: 10
                    ends[6]=3;
                    ;//RoomControl.sysj line: 23, column: 3
                    ends[6]=2;
                    ;//RoomControl.sysj line: 14, column: 2
                    tempsigyo_4.setPresent();//RoomControl.sysj line: 44, column: 2
                    currsigs.addElement(tempsigyo_4);
                    System.out.println("Emitted tempsigyo_4");
                    S160=1;
                    if(tempsigyo2_4.getprestatus()){//RoomControl.sysj line: 45, column: 18
                      S160=2;
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                    else {
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                  }
                  else {
                    i_thread_6 = i_thread_6 + 1;//RoomControl.sysj line: 26, column: 7
                    active[6]=1;
                    ends[6]=1;
                    tdone[6]=1;
                  }
                }
                break;
              
              case 2 : 
                if(LightLevel_1.getprestatus()){//RoomControl.sysj line: 37, column: 12
                  CurrentLightLevel_thread_6 = (Integer)LightLevel_1.getpreval();//RoomControl.sysj line: 38, column: 6
                  SetValue_thread_6 = LightControl.adjustLight(CurrentLightLevel_thread_6);//RoomControl.sysj line: 39, column: 6
                  SetIntensity_1.setPresent();//RoomControl.sysj line: 40, column: 6
                  currsigs.addElement(SetIntensity_1);
                  SetIntensity_1.setValue(SetValue_thread_6);//RoomControl.sysj line: 40, column: 6
                  System.out.println("Emitted SetIntensity_1");
                  S74=3;
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                else {
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                break;
              
              case 3 : 
                S74=3;
                S74=0;
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
                break;
              
            }
            break;
          
          case 1 : 
            if(tempsigyo2_4.getprestatus()){//RoomControl.sysj line: 45, column: 18
              S160=2;
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            else {
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            break;
          
          case 2 : 
            S160=2;
            S160=0;
            S74=0;
            active[6]=1;
            ends[6]=1;
            tdone[6]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26293(int [] tdone, int [] ends){
        switch(S1339){
      case 0 : 
        active[5]=0;
        ends[5]=0;
        tdone[5]=1;
        break;
      
      case 1 : 
        thread26294(tdone,ends);
        thread26295(tdone,ends);
        int biggest26296 = 0;
        if(ends[6]>=biggest26296){
          biggest26296=ends[6];
        }
        if(ends[7]>=biggest26296){
          biggest26296=ends[7];
        }
        if(biggest26296 == 1){
          active[5]=1;
          ends[5]=1;
          tdone[5]=1;
        }
        //FINXME code
        if(biggest26296 == 0){
          S1339=0;
          active[5]=0;
          ends[5]=0;
          tdone[5]=1;
        }
        break;
      
    }
  }

  public void thread26292(int [] tdone, int [] ends){
        switch(S2492){
      case 0 : 
        active[4]=0;
        ends[4]=0;
        tdone[4]=1;
        break;
      
      case 1 : 
        armed_4.setClear();//RoomControl.sysj line: 7, column: 3
        disarmed_4.setClear();//RoomControl.sysj line: 7, column: 3
        INC_4.setClear();//RoomControl.sysj line: 7, column: 3
        DEC_4.setClear();//RoomControl.sysj line: 7, column: 3
        RRC_4.setClear();//RoomControl.sysj line: 7, column: 3
        tempsigyo_4.setClear();//RoomControl.sysj line: 7, column: 3
        tempsigyo2_4.setClear();//RoomControl.sysj line: 7, column: 3
        switch(S2491){
          case 0 : 
            thread26293(tdone,ends);
            thread26297(tdone,ends);
            thread26304(tdone,ends);
            thread26305(tdone,ends);
            thread26306(tdone,ends);
            int biggest26307 = 0;
            if(ends[5]>=biggest26307){
              biggest26307=ends[5];
            }
            if(ends[8]>=biggest26307){
              biggest26307=ends[8];
            }
            if(ends[11]>=biggest26307){
              biggest26307=ends[11];
            }
            if(ends[12]>=biggest26307){
              biggest26307=ends[12];
            }
            if(ends[13]>=biggest26307){
              biggest26307=ends[13];
            }
            if(biggest26307 == 1){
              active[4]=1;
              ends[4]=1;
              tdone[4]=1;
            }
            //FINXME code
            if(biggest26307 == 0){
              S2491=1;
              active[4]=1;
              ends[4]=1;
              tdone[4]=1;
            }
            break;
          
          case 1 : 
            S2491=1;
            S2492=0;
            active[4]=0;
            ends[4]=0;
            tdone[4]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26291(int [] tdone, int [] ends){
        switch(S29){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        LightLevel_1.setPresent();//RoomControl.sysj line: 317, column: 7
        currsigs.addElement(LightLevel_1);
        LightLevel_1.setValue(2);//RoomControl.sysj line: 317, column: 7
        System.out.println("Emitted LightLevel_1");
        active[3]=1;
        ends[3]=1;
        tdone[3]=1;
        break;
      
    }
  }

  public void thread26290(int [] tdone, int [] ends){
        switch(S24){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        switch(S7){
          case 0 : 
            S7=0;
            S7=1;
            if(random_thread_2.nextInt(2) == 0){//RoomControl.sysj line: 310, column: 5
              Presence_1.setPresent();//RoomControl.sysj line: 311, column: 4
              currsigs.addElement(Presence_1);
              System.out.println("Emitted Presence_1");
              S7=2;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            else {
              S7=2;
              active[2]=1;
              ends[2]=1;
              tdone[2]=1;
            }
            break;
          
          case 1 : 
            S7=0;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
          case 2 : 
            S7=2;
            S7=0;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26287(int [] tdone, int [] ends){
        S2490=1;
    val_thread_13 = null;//RoomControl.sysj line: 135, column: 8
    S2298=0;
    ResetRC2_in.setPreempted(false);//RoomControl.sysj line: 136, column: 8
    S2216=0;
    if(!ResetRC2_in.isPartnerPresent() || ResetRC2_in.isPartnerPreempted()){//RoomControl.sysj line: 136, column: 8
      ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
      S2216=1;
      active[13]=1;
      ends[13]=1;
      tdone[13]=1;
    }
    else {
      S2211=0;
      if(!ResetRC2_in.isREQ()){//Unknown file line: 0, column: 0
        ResetRC2_in.setACK(true);//RoomControl.sysj line: 136, column: 8
        S2211=1;
        if(ResetRC2_in.isREQ()){//RoomControl.sysj line: 136, column: 8
          ResetRC2_in.setACK(false);//RoomControl.sysj line: 136, column: 8
          ends[13]=2;
          ;//RoomControl.sysj line: 136, column: 8
          val_thread_13 = (Boolean)ResetRC2_in.getVal();//RoomControl.sysj line: 137, column: 8
          if(val_thread_13){//RoomControl.sysj line: 138, column: 8
            RRC_4.setPresent();//RoomControl.sysj line: 139, column: 3
            currsigs.addElement(RRC_4);
            System.out.println("Emitted RRC_4");
            S2298=1;
            active[13]=1;
            ends[13]=1;
            tdone[13]=1;
          }
          else {
            S2298=1;
            active[13]=1;
            ends[13]=1;
            tdone[13]=1;
          }
        }
        else {
          active[13]=1;
          ends[13]=1;
          tdone[13]=1;
        }
      }
      else {
        active[13]=1;
        ends[13]=1;
        tdone[13]=1;
      }
    }
  }

  public void thread26286(int [] tdone, int [] ends){
        S2209=1;
    val_thread_12 = null;//RoomControl.sysj line: 123, column: 6
    S2017=0;
    DecreaseCount2_in.setPreempted(false);//RoomControl.sysj line: 124, column: 6
    S1935=0;
    if(!DecreaseCount2_in.isPartnerPresent() || DecreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 124, column: 6
      DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
      S1935=1;
      active[12]=1;
      ends[12]=1;
      tdone[12]=1;
    }
    else {
      S1930=0;
      if(!DecreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
        DecreaseCount2_in.setACK(true);//RoomControl.sysj line: 124, column: 6
        S1930=1;
        if(DecreaseCount2_in.isREQ()){//RoomControl.sysj line: 124, column: 6
          DecreaseCount2_in.setACK(false);//RoomControl.sysj line: 124, column: 6
          ends[12]=2;
          ;//RoomControl.sysj line: 124, column: 6
          val_thread_12 = (Boolean)DecreaseCount2_in.getVal();//RoomControl.sysj line: 125, column: 6
          if(val_thread_12){//RoomControl.sysj line: 126, column: 6
            DEC_4.setPresent();//RoomControl.sysj line: 127, column: 8
            currsigs.addElement(DEC_4);
            System.out.println("Emitted DEC_4");
            S2017=1;
            active[12]=1;
            ends[12]=1;
            tdone[12]=1;
          }
          else {
            S2017=1;
            active[12]=1;
            ends[12]=1;
            tdone[12]=1;
          }
        }
        else {
          active[12]=1;
          ends[12]=1;
          tdone[12]=1;
        }
      }
      else {
        active[12]=1;
        ends[12]=1;
        tdone[12]=1;
      }
    }
  }

  public void thread26285(int [] tdone, int [] ends){
        S1928=1;
    val_thread_11 = null;//RoomControl.sysj line: 111, column: 4
    S1736=0;
    IncreaseCount2_in.setPreempted(false);//RoomControl.sysj line: 112, column: 4
    S1654=0;
    if(!IncreaseCount2_in.isPartnerPresent() || IncreaseCount2_in.isPartnerPreempted()){//RoomControl.sysj line: 112, column: 4
      IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
      S1654=1;
      active[11]=1;
      ends[11]=1;
      tdone[11]=1;
    }
    else {
      S1649=0;
      if(!IncreaseCount2_in.isREQ()){//Unknown file line: 0, column: 0
        IncreaseCount2_in.setACK(true);//RoomControl.sysj line: 112, column: 4
        S1649=1;
        if(IncreaseCount2_in.isREQ()){//RoomControl.sysj line: 112, column: 4
          IncreaseCount2_in.setACK(false);//RoomControl.sysj line: 112, column: 4
          ends[11]=2;
          ;//RoomControl.sysj line: 112, column: 4
          val_thread_11 = (Boolean)IncreaseCount2_in.getVal();//RoomControl.sysj line: 113, column: 4
          if(val_thread_11){//RoomControl.sysj line: 114, column: 4
            INC_4.setPresent();//RoomControl.sysj line: 115, column: 6
            currsigs.addElement(INC_4);
            System.out.println("Emitted INC_4");
            S1736=1;
            active[11]=1;
            ends[11]=1;
            tdone[11]=1;
          }
          else {
            S1736=1;
            active[11]=1;
            ends[11]=1;
            tdone[11]=1;
          }
        }
        else {
          active[11]=1;
          ends[11]=1;
          tdone[11]=1;
        }
      }
      else {
        active[11]=1;
        ends[11]=1;
        tdone[11]=1;
      }
    }
  }

  public void thread26283(int [] tdone, int [] ends){
        S1439=1;
    S1408=0;
    active[10]=1;
    ends[10]=1;
    tdone[10]=1;
  }

  public void thread26282(int [] tdone, int [] ends){
        S1407=1;
    count_thread_9 = 0;//RoomControl.sysj line: 69, column: 6
    S1350=0;
    if(count_thread_9 != 0){//RoomControl.sysj line: 71, column: 11
      S1349=0;
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
    else {
      S1349=1;
      armed_4.setPresent();//RoomControl.sysj line: 84, column: 5
      currsigs.addElement(armed_4);
      System.out.println("Emitted armed_4");
      active[9]=1;
      ends[9]=1;
      tdone[9]=1;
    }
  }

  public void thread26281(int [] tdone, int [] ends){
        S1647=1;
    S1441=0;
    thread26282(tdone,ends);
    thread26283(tdone,ends);
    int biggest26284 = 0;
    if(ends[9]>=biggest26284){
      biggest26284=ends[9];
    }
    if(ends[10]>=biggest26284){
      biggest26284=ends[10];
    }
    if(biggest26284 == 1){
      active[8]=1;
      ends[8]=1;
      tdone[8]=1;
    }
  }

  public void thread26279(int [] tdone, int [] ends){
        S1337=1;
    S879=0;
    active[7]=1;
    ends[7]=1;
    tdone[7]=1;
  }

  public void thread26278(int [] tdone, int [] ends){
        S789=1;
    SetValue_thread_6 = 10;//RoomControl.sysj line: 11, column: 7
    CurrentLightLevel_thread_6 = 0;//RoomControl.sysj line: 12, column: 7
    S160=0;
    S74=0;
    active[6]=1;
    ends[6]=1;
    tdone[6]=1;
  }

  public void thread26277(int [] tdone, int [] ends){
        S1339=1;
    thread26278(tdone,ends);
    thread26279(tdone,ends);
    int biggest26280 = 0;
    if(ends[6]>=biggest26280){
      biggest26280=ends[6];
    }
    if(ends[7]>=biggest26280){
      biggest26280=ends[7];
    }
    if(biggest26280 == 1){
      active[5]=1;
      ends[5]=1;
      tdone[5]=1;
    }
  }

  public void thread26276(int [] tdone, int [] ends){
        S2492=1;
    armed_4.setClear();//RoomControl.sysj line: 7, column: 3
    disarmed_4.setClear();//RoomControl.sysj line: 7, column: 3
    INC_4.setClear();//RoomControl.sysj line: 7, column: 3
    DEC_4.setClear();//RoomControl.sysj line: 7, column: 3
    RRC_4.setClear();//RoomControl.sysj line: 7, column: 3
    tempsigyo_4.setClear();//RoomControl.sysj line: 7, column: 3
    tempsigyo2_4.setClear();//RoomControl.sysj line: 7, column: 3
    S2491=0;
    thread26277(tdone,ends);
    thread26281(tdone,ends);
    thread26285(tdone,ends);
    thread26286(tdone,ends);
    thread26287(tdone,ends);
    int biggest26288 = 0;
    if(ends[5]>=biggest26288){
      biggest26288=ends[5];
    }
    if(ends[8]>=biggest26288){
      biggest26288=ends[8];
    }
    if(ends[11]>=biggest26288){
      biggest26288=ends[11];
    }
    if(ends[12]>=biggest26288){
      biggest26288=ends[12];
    }
    if(ends[13]>=biggest26288){
      biggest26288=ends[13];
    }
    if(biggest26288 == 1){
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread26275(int [] tdone, int [] ends){
        S29=1;
    LightLevel_1.setPresent();//RoomControl.sysj line: 317, column: 7
    currsigs.addElement(LightLevel_1);
    LightLevel_1.setValue(2);//RoomControl.sysj line: 317, column: 7
    System.out.println("Emitted LightLevel_1");
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread26274(int [] tdone, int [] ends){
        S24=1;
    random_thread_2 = new Random(320543853);//RoomControl.sysj line: 307, column: 7
    S7=0;
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S2494){
        case 0 : 
          S2494=0;
          break RUN;
        
        case 1 : 
          S2494=2;
          S2494=2;
          Presence_1.setClear();//RoomControl.sysj line: 300, column: 3
          TimeOutRC_1.setClear();//RoomControl.sysj line: 300, column: 3
          TriggerRC_1.setClear();//RoomControl.sysj line: 300, column: 3
          LightLevel_1.setClear();//RoomControl.sysj line: 301, column: 3
          SetIntensity_1.setClear();//RoomControl.sysj line: 301, column: 3
          thread26274(tdone,ends);
          thread26275(tdone,ends);
          thread26276(tdone,ends);
          int biggest26289 = 0;
          if(ends[2]>=biggest26289){
            biggest26289=ends[2];
          }
          if(ends[3]>=biggest26289){
            biggest26289=ends[3];
          }
          if(ends[4]>=biggest26289){
            biggest26289=ends[4];
          }
          if(biggest26289 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          if(biggest26289 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          Presence_1.setClear();//RoomControl.sysj line: 300, column: 3
          TimeOutRC_1.setClear();//RoomControl.sysj line: 300, column: 3
          TriggerRC_1.setClear();//RoomControl.sysj line: 300, column: 3
          LightLevel_1.setClear();//RoomControl.sysj line: 301, column: 3
          SetIntensity_1.setClear();//RoomControl.sysj line: 301, column: 3
          thread26290(tdone,ends);
          thread26291(tdone,ends);
          thread26292(tdone,ends);
          int biggest26308 = 0;
          if(ends[2]>=biggest26308){
            biggest26308=ends[2];
          }
          if(ends[3]>=biggest26308){
            biggest26308=ends[3];
          }
          if(ends[4]>=biggest26308){
            biggest26308=ends[4];
          }
          if(biggest26308 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          //FINXME code
          if(biggest26308 == 0){
            S2494=0;
            active[1]=0;
            ends[1]=0;
            S2494=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    Presence_1 = new Signal();
    TimeOutRC_1 = new Signal();
    TriggerRC_1 = new Signal();
    LightLevel_1 = new Signal();
    SetIntensity_1 = new Signal();
    armed_4 = new Signal();
    disarmed_4 = new Signal();
    INC_4 = new Signal();
    DEC_4 = new Signal();
    RRC_4 = new Signal();
    tempsigyo_4 = new Signal();
    tempsigyo2_4 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        if(!df){
          ResetRC2_in.gethook();
          IncreaseCount2_in.gethook();
          DecreaseCount2_in.gethook();
          Emergency2_o.gethook();
          ActivateAlarm2_o.gethook();
          df = true;
        }
        runClockDomain();
      }
      Presence_1.setpreclear();
      TimeOutRC_1.setpreclear();
      TriggerRC_1.setpreclear();
      LightLevel_1.setpreclear();
      SetIntensity_1.setpreclear();
      armed_4.setpreclear();
      disarmed_4.setpreclear();
      INC_4.setpreclear();
      DEC_4.setpreclear();
      RRC_4.setpreclear();
      tempsigyo_4.setpreclear();
      tempsigyo2_4.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      Presence_1.setClear();
      TimeOutRC_1.setClear();
      TriggerRC_1.setClear();
      LightLevel_1.setClear();
      SetIntensity_1.setClear();
      armed_4.setClear();
      disarmed_4.setClear();
      INC_4.setClear();
      DEC_4.setClear();
      RRC_4.setClear();
      tempsigyo_4.setClear();
      tempsigyo2_4.setClear();
      ResetRC2_in.sethook();
      IncreaseCount2_in.sethook();
      DecreaseCount2_in.sethook();
      Emergency2_o.sethook();
      ActivateAlarm2_o.sethook();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        ResetRC2_in.gethook();
        IncreaseCount2_in.gethook();
        DecreaseCount2_in.gethook();
        Emergency2_o.gethook();
        ActivateAlarm2_o.gethook();
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
