import java.util.*;
import com.systemj.ClockDomain;
import com.systemj.Signal;
import com.systemj.input_Channel;
import com.systemj.output_Channel;
import UtilA.*;//RoomControl.sysj line: 1, column: 1

public class AlarmC extends ClockDomain{
  public AlarmC(String name){super(name);}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  public input_Channel ActivateAlarm2_in = new input_Channel();
  public output_Channel ResetAC2_o = new output_Channel();
  public output_Channel ResetRC2_o = new output_Channel();
  private Signal AlarmOn_28;
  private Signal AlarmOff_28;
  private Signal ResetAlarm_28;
  private Random random_thread_29;//RoomControl.sysj line: 410, column: 7
  private int S26272 = 1;
  private int S25594 = 1;
  private int S25482 = 1;
  private int S26270 = 1;
  private int S26131 = 1;
  private int S25685 = 1;
  private int S25603 = 1;
  private int S25598 = 1;
  private int S25692 = 1;
  private int S25687 = 1;
  private int S26268 = 1;
  private int S26176 = 1;
  private int S26138 = 1;
  private int S26133 = 1;
  
  private int[] ends = new int[33];
  private int[] tdone = new int[33];
  
  public void thread26354(int [] tdone, int [] ends){
        switch(S26268){
      case 0 : 
        active[32]=0;
        ends[32]=0;
        tdone[32]=1;
        break;
      
      case 1 : 
        switch(S26176){
          case 0 : 
            switch(S26138){
              case 0 : 
                if(!ActivateAlarm2_in.isPartnerPresent() || ActivateAlarm2_in.isPartnerPreempted()){//RoomControl.sysj line: 287, column: 2
                  ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
                  S26138=1;
                  active[32]=1;
                  ends[32]=1;
                  tdone[32]=1;
                }
                else {
                  switch(S26133){
                    case 0 : 
                      if(!ActivateAlarm2_in.isREQ()){//Unknown file line: 0, column: 0
                        ActivateAlarm2_in.setACK(true);//RoomControl.sysj line: 287, column: 2
                        S26133=1;
                        if(ActivateAlarm2_in.isREQ()){//RoomControl.sysj line: 287, column: 2
                          ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
                          ends[32]=2;
                          ;//RoomControl.sysj line: 287, column: 2
                          AlarmOn_28.setPresent();//RoomControl.sysj line: 288, column: 2
                          currsigs.addElement(AlarmOn_28);
                          System.out.println("Emitted AlarmOn_28");
                          S26176=1;
                          active[32]=1;
                          ends[32]=1;
                          tdone[32]=1;
                        }
                        else {
                          active[32]=1;
                          ends[32]=1;
                          tdone[32]=1;
                        }
                      }
                      else {
                        active[32]=1;
                        ends[32]=1;
                        tdone[32]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(ActivateAlarm2_in.isREQ()){//RoomControl.sysj line: 287, column: 2
                        ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
                        ends[32]=2;
                        ;//RoomControl.sysj line: 287, column: 2
                        AlarmOn_28.setPresent();//RoomControl.sysj line: 288, column: 2
                        currsigs.addElement(AlarmOn_28);
                        System.out.println("Emitted AlarmOn_28");
                        S26176=1;
                        active[32]=1;
                        ends[32]=1;
                        tdone[32]=1;
                      }
                      else {
                        active[32]=1;
                        ends[32]=1;
                        tdone[32]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S26138=1;
                S26138=0;
                if(!ActivateAlarm2_in.isPartnerPresent() || ActivateAlarm2_in.isPartnerPreempted()){//RoomControl.sysj line: 287, column: 2
                  ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
                  S26138=1;
                  active[32]=1;
                  ends[32]=1;
                  tdone[32]=1;
                }
                else {
                  S26133=0;
                  if(!ActivateAlarm2_in.isREQ()){//Unknown file line: 0, column: 0
                    ActivateAlarm2_in.setACK(true);//RoomControl.sysj line: 287, column: 2
                    S26133=1;
                    if(ActivateAlarm2_in.isREQ()){//RoomControl.sysj line: 287, column: 2
                      ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
                      ends[32]=2;
                      ;//RoomControl.sysj line: 287, column: 2
                      AlarmOn_28.setPresent();//RoomControl.sysj line: 288, column: 2
                      currsigs.addElement(AlarmOn_28);
                      System.out.println("Emitted AlarmOn_28");
                      S26176=1;
                      active[32]=1;
                      ends[32]=1;
                      tdone[32]=1;
                    }
                    else {
                      active[32]=1;
                      ends[32]=1;
                      tdone[32]=1;
                    }
                  }
                  else {
                    active[32]=1;
                    ends[32]=1;
                    tdone[32]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S26176=1;
            S26176=0;
            ActivateAlarm2_in.setPreempted(false);//RoomControl.sysj line: 287, column: 2
            S26138=0;
            if(!ActivateAlarm2_in.isPartnerPresent() || ActivateAlarm2_in.isPartnerPreempted()){//RoomControl.sysj line: 287, column: 2
              ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
              S26138=1;
              active[32]=1;
              ends[32]=1;
              tdone[32]=1;
            }
            else {
              S26133=0;
              if(!ActivateAlarm2_in.isREQ()){//Unknown file line: 0, column: 0
                ActivateAlarm2_in.setACK(true);//RoomControl.sysj line: 287, column: 2
                S26133=1;
                if(ActivateAlarm2_in.isREQ()){//RoomControl.sysj line: 287, column: 2
                  ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
                  ends[32]=2;
                  ;//RoomControl.sysj line: 287, column: 2
                  AlarmOn_28.setPresent();//RoomControl.sysj line: 288, column: 2
                  currsigs.addElement(AlarmOn_28);
                  System.out.println("Emitted AlarmOn_28");
                  S26176=1;
                  active[32]=1;
                  ends[32]=1;
                  tdone[32]=1;
                }
                else {
                  active[32]=1;
                  ends[32]=1;
                  tdone[32]=1;
                }
              }
              else {
                active[32]=1;
                ends[32]=1;
                tdone[32]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26353(int [] tdone, int [] ends){
        switch(S26131){
      case 0 : 
        active[31]=0;
        ends[31]=0;
        tdone[31]=1;
        break;
      
      case 1 : 
        switch(S25685){
          case 0 : 
            if(ResetAlarm_28.getprestatus()){//RoomControl.sysj line: 276, column: 13
              S25685=1;
                            ResetAC2_o.setPreempted(false);//RoomControl.sysj line: 277, column: 7
              S25603=0;
              if(!ResetAC2_o.isPartnerPresent() || ResetAC2_o.isPartnerPreempted()){//RoomControl.sysj line: 277, column: 7
                                ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                S25603=1;
                active[31]=1;
                ends[31]=1;
                tdone[31]=1;
              }
              else {
                S25598=0;
                if(ResetAC2_o.isACK()){//RoomControl.sysj line: 277, column: 7
                                    ResetAC2_o.setVal(true);//RoomControl.sysj line: 277, column: 7
                  S25598=1;
                  if(!ResetAC2_o.isACK()){//Unknown file line: 0, column: 0
                                        ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                    ends[31]=2;
                    ;//RoomControl.sysj line: 277, column: 7
                    S25685=2;
                    active[31]=1;
                    ends[31]=1;
                    tdone[31]=1;
                  }
                  else {
                    active[31]=1;
                    ends[31]=1;
                    tdone[31]=1;
                  }
                }
                else {
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
              }
            }
            else {
              active[31]=1;
              ends[31]=1;
              tdone[31]=1;
            }
            break;
          
          case 1 : 
            switch(S25603){
              case 0 : 
                if(!ResetAC2_o.isPartnerPresent() || ResetAC2_o.isPartnerPreempted()){//RoomControl.sysj line: 277, column: 7
                                    ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                  S25603=1;
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
                else {
                  switch(S25598){
                    case 0 : 
                      if(ResetAC2_o.isACK()){//RoomControl.sysj line: 277, column: 7
                                                ResetAC2_o.setVal(true);//RoomControl.sysj line: 277, column: 7
                        S25598=1;
                        if(!ResetAC2_o.isACK()){//Unknown file line: 0, column: 0
                                                    ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                          ends[31]=2;
                          ;//RoomControl.sysj line: 277, column: 7
                          S25685=2;
                          active[31]=1;
                          ends[31]=1;
                          tdone[31]=1;
                        }
                        else {
                          active[31]=1;
                          ends[31]=1;
                          tdone[31]=1;
                        }
                      }
                      else {
                        active[31]=1;
                        ends[31]=1;
                        tdone[31]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(!ResetAC2_o.isACK()){//Unknown file line: 0, column: 0
                                                ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                        ends[31]=2;
                        ;//RoomControl.sysj line: 277, column: 7
                        S25685=2;
                        active[31]=1;
                        ends[31]=1;
                        tdone[31]=1;
                      }
                      else {
                        active[31]=1;
                        ends[31]=1;
                        tdone[31]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S25603=1;
                S25603=0;
                if(!ResetAC2_o.isPartnerPresent() || ResetAC2_o.isPartnerPreempted()){//RoomControl.sysj line: 277, column: 7
                                    ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                  S25603=1;
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
                else {
                  S25598=0;
                  if(ResetAC2_o.isACK()){//RoomControl.sysj line: 277, column: 7
                                        ResetAC2_o.setVal(true);//RoomControl.sysj line: 277, column: 7
                    S25598=1;
                    if(!ResetAC2_o.isACK()){//Unknown file line: 0, column: 0
                                            ResetAC2_o.setREQ(false);//RoomControl.sysj line: 277, column: 7
                      ends[31]=2;
                      ;//RoomControl.sysj line: 277, column: 7
                      S25685=2;
                      active[31]=1;
                      ends[31]=1;
                      tdone[31]=1;
                    }
                    else {
                      active[31]=1;
                      ends[31]=1;
                      tdone[31]=1;
                    }
                  }
                  else {
                    active[31]=1;
                    ends[31]=1;
                    tdone[31]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 2 : 
            S25685=2;
            S25685=3;
                        ResetRC2_o.setPreempted(false);//RoomControl.sysj line: 279, column: 7
            S25692=0;
            if(!ResetRC2_o.isPartnerPresent() || ResetRC2_o.isPartnerPreempted()){//RoomControl.sysj line: 279, column: 7
                            ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
              S25692=1;
              active[31]=1;
              ends[31]=1;
              tdone[31]=1;
            }
            else {
              S25687=0;
              if(ResetRC2_o.isACK()){//RoomControl.sysj line: 279, column: 7
                                ResetRC2_o.setVal(true);//RoomControl.sysj line: 279, column: 7
                S25687=1;
                if(!ResetRC2_o.isACK()){//Unknown file line: 0, column: 0
                                    ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
                  ends[31]=2;
                  ;//RoomControl.sysj line: 279, column: 7
                  AlarmOff_28.setPresent();//RoomControl.sysj line: 280, column: 7
                  currsigs.addElement(AlarmOff_28);
                  System.out.println("Emitted AlarmOff_28");
                  S25685=4;
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
                else {
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
              }
              else {
                active[31]=1;
                ends[31]=1;
                tdone[31]=1;
              }
            }
            break;
          
          case 3 : 
            switch(S25692){
              case 0 : 
                if(!ResetRC2_o.isPartnerPresent() || ResetRC2_o.isPartnerPreempted()){//RoomControl.sysj line: 279, column: 7
                                    ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
                  S25692=1;
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
                else {
                  switch(S25687){
                    case 0 : 
                      if(ResetRC2_o.isACK()){//RoomControl.sysj line: 279, column: 7
                                                ResetRC2_o.setVal(true);//RoomControl.sysj line: 279, column: 7
                        S25687=1;
                        if(!ResetRC2_o.isACK()){//Unknown file line: 0, column: 0
                                                    ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
                          ends[31]=2;
                          ;//RoomControl.sysj line: 279, column: 7
                          AlarmOff_28.setPresent();//RoomControl.sysj line: 280, column: 7
                          currsigs.addElement(AlarmOff_28);
                          System.out.println("Emitted AlarmOff_28");
                          S25685=4;
                          active[31]=1;
                          ends[31]=1;
                          tdone[31]=1;
                        }
                        else {
                          active[31]=1;
                          ends[31]=1;
                          tdone[31]=1;
                        }
                      }
                      else {
                        active[31]=1;
                        ends[31]=1;
                        tdone[31]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(!ResetRC2_o.isACK()){//Unknown file line: 0, column: 0
                                                ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
                        ends[31]=2;
                        ;//RoomControl.sysj line: 279, column: 7
                        AlarmOff_28.setPresent();//RoomControl.sysj line: 280, column: 7
                        currsigs.addElement(AlarmOff_28);
                        System.out.println("Emitted AlarmOff_28");
                        S25685=4;
                        active[31]=1;
                        ends[31]=1;
                        tdone[31]=1;
                      }
                      else {
                        active[31]=1;
                        ends[31]=1;
                        tdone[31]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S25692=1;
                S25692=0;
                if(!ResetRC2_o.isPartnerPresent() || ResetRC2_o.isPartnerPreempted()){//RoomControl.sysj line: 279, column: 7
                                    ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
                  S25692=1;
                  active[31]=1;
                  ends[31]=1;
                  tdone[31]=1;
                }
                else {
                  S25687=0;
                  if(ResetRC2_o.isACK()){//RoomControl.sysj line: 279, column: 7
                                        ResetRC2_o.setVal(true);//RoomControl.sysj line: 279, column: 7
                    S25687=1;
                    if(!ResetRC2_o.isACK()){//Unknown file line: 0, column: 0
                                            ResetRC2_o.setREQ(false);//RoomControl.sysj line: 279, column: 7
                      ends[31]=2;
                      ;//RoomControl.sysj line: 279, column: 7
                      AlarmOff_28.setPresent();//RoomControl.sysj line: 280, column: 7
                      currsigs.addElement(AlarmOff_28);
                      System.out.println("Emitted AlarmOff_28");
                      S25685=4;
                      active[31]=1;
                      ends[31]=1;
                      tdone[31]=1;
                    }
                    else {
                      active[31]=1;
                      ends[31]=1;
                      tdone[31]=1;
                    }
                  }
                  else {
                    active[31]=1;
                    ends[31]=1;
                    tdone[31]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 4 : 
            S25685=4;
            S25685=0;
            active[31]=1;
            ends[31]=1;
            tdone[31]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26352(int [] tdone, int [] ends){
        switch(S26270){
      case 0 : 
        active[30]=0;
        ends[30]=0;
        tdone[30]=1;
        break;
      
      case 1 : 
        thread26353(tdone,ends);
        thread26354(tdone,ends);
        int biggest26355 = 0;
        if(ends[31]>=biggest26355){
          biggest26355=ends[31];
        }
        if(ends[32]>=biggest26355){
          biggest26355=ends[32];
        }
        if(biggest26355 == 1){
          active[30]=1;
          ends[30]=1;
          tdone[30]=1;
        }
        //FINXME code
        if(biggest26355 == 0){
          S26270=0;
          active[30]=0;
          ends[30]=0;
          tdone[30]=1;
        }
        break;
      
    }
  }

  public void thread26351(int [] tdone, int [] ends){
        switch(S25594){
      case 0 : 
        active[29]=0;
        ends[29]=0;
        tdone[29]=1;
        break;
      
      case 1 : 
        switch(S25482){
          case 0 : 
            if(AlarmOn_28.getprestatus()){//RoomControl.sysj line: 412, column: 8
              S25482=1;
              if(random_thread_29.nextInt(2) == 0){//RoomControl.sysj line: 415, column: 9
                ResetAlarm_28.setPresent();//RoomControl.sysj line: 416, column: 8
                currsigs.addElement(ResetAlarm_28);
                System.out.println("Emitted ResetAlarm_28");
                ends[29]=2;
                ;//RoomControl.sysj line: 413, column: 2
                S25482=2;
                active[29]=1;
                ends[29]=1;
                tdone[29]=1;
              }
              else {
                active[29]=1;
                ends[29]=1;
                tdone[29]=1;
              }
            }
            else {
              active[29]=1;
              ends[29]=1;
              tdone[29]=1;
            }
            break;
          
          case 1 : 
            if(random_thread_29.nextInt(2) == 0){//RoomControl.sysj line: 415, column: 9
              ResetAlarm_28.setPresent();//RoomControl.sysj line: 416, column: 8
              currsigs.addElement(ResetAlarm_28);
              System.out.println("Emitted ResetAlarm_28");
              ends[29]=2;
              ;//RoomControl.sysj line: 413, column: 2
              S25482=2;
              active[29]=1;
              ends[29]=1;
              tdone[29]=1;
            }
            else {
              active[29]=1;
              ends[29]=1;
              tdone[29]=1;
            }
            break;
          
          case 2 : 
            S25482=2;
            S25482=0;
            active[29]=1;
            ends[29]=1;
            tdone[29]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26348(int [] tdone, int [] ends){
        S26268=1;
    S26176=0;
    ActivateAlarm2_in.setPreempted(false);//RoomControl.sysj line: 287, column: 2
    S26138=0;
    if(!ActivateAlarm2_in.isPartnerPresent() || ActivateAlarm2_in.isPartnerPreempted()){//RoomControl.sysj line: 287, column: 2
      ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
      S26138=1;
      active[32]=1;
      ends[32]=1;
      tdone[32]=1;
    }
    else {
      S26133=0;
      if(!ActivateAlarm2_in.isREQ()){//Unknown file line: 0, column: 0
        ActivateAlarm2_in.setACK(true);//RoomControl.sysj line: 287, column: 2
        S26133=1;
        if(ActivateAlarm2_in.isREQ()){//RoomControl.sysj line: 287, column: 2
          ActivateAlarm2_in.setACK(false);//RoomControl.sysj line: 287, column: 2
          ends[32]=2;
          ;//RoomControl.sysj line: 287, column: 2
          AlarmOn_28.setPresent();//RoomControl.sysj line: 288, column: 2
          currsigs.addElement(AlarmOn_28);
          System.out.println("Emitted AlarmOn_28");
          S26176=1;
          active[32]=1;
          ends[32]=1;
          tdone[32]=1;
        }
        else {
          active[32]=1;
          ends[32]=1;
          tdone[32]=1;
        }
      }
      else {
        active[32]=1;
        ends[32]=1;
        tdone[32]=1;
      }
    }
  }

  public void thread26347(int [] tdone, int [] ends){
        S26131=1;
    S25685=0;
    active[31]=1;
    ends[31]=1;
    tdone[31]=1;
  }

  public void thread26346(int [] tdone, int [] ends){
        S26270=1;
    thread26347(tdone,ends);
    thread26348(tdone,ends);
    int biggest26349 = 0;
    if(ends[31]>=biggest26349){
      biggest26349=ends[31];
    }
    if(ends[32]>=biggest26349){
      biggest26349=ends[32];
    }
    if(biggest26349 == 1){
      active[30]=1;
      ends[30]=1;
      tdone[30]=1;
    }
  }

  public void thread26345(int [] tdone, int [] ends){
        S25594=1;
    random_thread_29 = new Random(466433123);//RoomControl.sysj line: 410, column: 7
    S25482=0;
    active[29]=1;
    ends[29]=1;
    tdone[29]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S26272){
        case 0 : 
          S26272=0;
          break RUN;
        
        case 1 : 
          S26272=2;
          S26272=2;
          AlarmOn_28.setClear();//RoomControl.sysj line: 405, column: 3
          AlarmOff_28.setClear();//RoomControl.sysj line: 405, column: 3
          ResetAlarm_28.setClear();//RoomControl.sysj line: 405, column: 3
          thread26345(tdone,ends);
          thread26346(tdone,ends);
          int biggest26350 = 0;
          if(ends[29]>=biggest26350){
            biggest26350=ends[29];
          }
          if(ends[30]>=biggest26350){
            biggest26350=ends[30];
          }
          if(biggest26350 == 1){
            active[28]=1;
            ends[28]=1;
            break RUN;
          }
          if(biggest26350 == 1){
            active[28]=1;
            ends[28]=1;
            break RUN;
          }
        
        case 2 : 
          AlarmOn_28.setClear();//RoomControl.sysj line: 405, column: 3
          AlarmOff_28.setClear();//RoomControl.sysj line: 405, column: 3
          ResetAlarm_28.setClear();//RoomControl.sysj line: 405, column: 3
          thread26351(tdone,ends);
          thread26352(tdone,ends);
          int biggest26356 = 0;
          if(ends[29]>=biggest26356){
            biggest26356=ends[29];
          }
          if(ends[30]>=biggest26356){
            biggest26356=ends[30];
          }
          if(biggest26356 == 1){
            active[28]=1;
            ends[28]=1;
            break RUN;
          }
          //FINXME code
          if(biggest26356 == 0){
            S26272=0;
            active[28]=0;
            ends[28]=0;
            S26272=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    AlarmOn_28 = new Signal();
    AlarmOff_28 = new Signal();
    ResetAlarm_28 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[28] != 0){
      int index = 28;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[28]!=0 || suspended[28]!=0 || active[28]!=1);
      else{
        if(!df){
          ActivateAlarm2_in.gethook();
          ResetAC2_o.gethook();
          ResetRC2_o.gethook();
          df = true;
        }
        runClockDomain();
      }
      AlarmOn_28.setpreclear();
      AlarmOff_28.setpreclear();
      ResetAlarm_28.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      AlarmOn_28.setClear();
      AlarmOff_28.setClear();
      ResetAlarm_28.setClear();
      ActivateAlarm2_in.sethook();
      ResetAC2_o.sethook();
      ResetRC2_o.sethook();
      if(paused[28]!=0 || suspended[28]!=0 || active[28]!=1);
      else{
        ActivateAlarm2_in.gethook();
        ResetAC2_o.gethook();
        ResetRC2_o.gethook();
      }
      if(active[28] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
