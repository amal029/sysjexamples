import java.util.*;
import com.systemj.ClockDomain;
import com.systemj.Signal;
import com.systemj.input_Channel;
import com.systemj.output_Channel;
import UtilA.*;//RoomControl.sysj line: 1, column: 1

public class AccessC extends ClockDomain{
  public AccessC(String name){super(name);}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  public input_Channel ResetAC2_in = new input_Channel();
  public input_Channel Emergency2_in = new input_Channel();
  public output_Channel IncreaseCount2_o = new output_Channel();
  public output_Channel DecreaseCount2_o = new output_Channel();
  private Signal ExitRequest_14;
  private Signal EntryRegistered_14;
  private Signal ExitRegistered_14;
  private Signal DoorClosed_14;
  private Signal DoorOpened_14;
  private Signal InvalidCard_14;
  private Signal UnlockDoor_14;
  private Signal LockDoor_14;
  private Signal TimeOutAC_14;
  private Signal TriggerAC_14;
  private Signal EntryRequest_14;
  private Signal s1_14;
  private Signal s2_14;
  private Signal beep_18;
  private Signal RAC_18;
  private Signal Preempt_18;
  private Signal sInc_18;
  private Signal sDec_18;
  private Random random_thread_16;//RoomControl.sysj line: 334, column: 2
  private int p_thread_17;//RoomControl.sysj line: 359, column: 4
  private boolean h_thread_17;//RoomControl.sysj line: 360, column: 4
  private Random random_thread_17;//RoomControl.sysj line: 361, column: 4
  private int i_thread_19;//RoomControl.sysj line: 175, column: 3
  private int i_thread_23;//RoomControl.sysj line: 197, column: 7
  private String id_thread_19;//RoomControl.sysj line: 157, column: 2
  private Boolean val_thread_24;//RoomControl.sysj line: 232, column: 2
  private Boolean val_thread_25;//RoomControl.sysj line: 245, column: 4
  private int S25457 = 1;
  private int S2753 = 1;
  private int S2593 = 1;
  private int S2521 = 1;
  private int S2520 = 1;
  private int S2508 = 1;
  private int S2509 = 1;
  private int S2514 = 1;
  private int S2510 = 1;
  private int S2511 = 1;
  private int S2751 = 1;
  private int S2620 = 1;
  private int S2607 = 1;
  private int S2596 = 1;
  private int S2605 = 1;
  private int S2597 = 1;
  private int S2604 = 1;
  private int S25455 = 1;
  private int S13000 = 1;
  private int S6140 = 1;
  private int S2764 = 1;
  private int S2855 = 1;
  private int S2788 = 1;
  private int S2783 = 1;
  private int S2786 = 1;
  private int S2816 = 1;
  private int S2803 = 1;
  private int S6145 = 1;
  private int S13281 = 1;
  private int S13089 = 1;
  private int S13007 = 1;
  private int S13002 = 1;
  private int S13562 = 1;
  private int S13370 = 1;
  private int S13288 = 1;
  private int S13283 = 1;
  private int S13834 = 1;
  private int S13652 = 1;
  private int S13570 = 1;
  private int S13565 = 1;
  private int S14106 = 1;
  private int S13924 = 1;
  private int S13842 = 1;
  private int S13837 = 1;
  
  private int[] ends = new int[33];
  private int[] tdone = new int[33];
  
  public void thread26342(int [] tdone, int [] ends){
        switch(S14106){
      case 0 : 
        active[27]=0;
        ends[27]=0;
        tdone[27]=1;
        break;
      
      case 1 : 
        switch(S13924){
          case 0 : 
            if(sDec_18.getprestatus()){//RoomControl.sysj line: 265, column: 14
              S13924=1;
                            DecreaseCount2_o.setPreempted(false);//RoomControl.sysj line: 266, column: 8
              S13842=0;
              if(!DecreaseCount2_o.isPartnerPresent() || DecreaseCount2_o.isPartnerPreempted()){//RoomControl.sysj line: 266, column: 8
                                DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                S13842=1;
                active[27]=1;
                ends[27]=1;
                tdone[27]=1;
              }
              else {
                S13837=0;
                if(DecreaseCount2_o.isACK()){//RoomControl.sysj line: 266, column: 8
                                    DecreaseCount2_o.setVal(true);//RoomControl.sysj line: 266, column: 8
                  S13837=1;
                  if(!DecreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                        DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                    ends[27]=2;
                    ;//RoomControl.sysj line: 266, column: 8
                    S13924=2;
                    active[27]=1;
                    ends[27]=1;
                    tdone[27]=1;
                  }
                  else {
                    active[27]=1;
                    ends[27]=1;
                    tdone[27]=1;
                  }
                }
                else {
                  active[27]=1;
                  ends[27]=1;
                  tdone[27]=1;
                }
              }
            }
            else {
              active[27]=1;
              ends[27]=1;
              tdone[27]=1;
            }
            break;
          
          case 1 : 
            switch(S13842){
              case 0 : 
                if(!DecreaseCount2_o.isPartnerPresent() || DecreaseCount2_o.isPartnerPreempted()){//RoomControl.sysj line: 266, column: 8
                                    DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                  S13842=1;
                  active[27]=1;
                  ends[27]=1;
                  tdone[27]=1;
                }
                else {
                  switch(S13837){
                    case 0 : 
                      if(DecreaseCount2_o.isACK()){//RoomControl.sysj line: 266, column: 8
                                                DecreaseCount2_o.setVal(true);//RoomControl.sysj line: 266, column: 8
                        S13837=1;
                        if(!DecreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                                    DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                          ends[27]=2;
                          ;//RoomControl.sysj line: 266, column: 8
                          S13924=2;
                          active[27]=1;
                          ends[27]=1;
                          tdone[27]=1;
                        }
                        else {
                          active[27]=1;
                          ends[27]=1;
                          tdone[27]=1;
                        }
                      }
                      else {
                        active[27]=1;
                        ends[27]=1;
                        tdone[27]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(!DecreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                                DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                        ends[27]=2;
                        ;//RoomControl.sysj line: 266, column: 8
                        S13924=2;
                        active[27]=1;
                        ends[27]=1;
                        tdone[27]=1;
                      }
                      else {
                        active[27]=1;
                        ends[27]=1;
                        tdone[27]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S13842=1;
                S13842=0;
                if(!DecreaseCount2_o.isPartnerPresent() || DecreaseCount2_o.isPartnerPreempted()){//RoomControl.sysj line: 266, column: 8
                                    DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                  S13842=1;
                  active[27]=1;
                  ends[27]=1;
                  tdone[27]=1;
                }
                else {
                  S13837=0;
                  if(DecreaseCount2_o.isACK()){//RoomControl.sysj line: 266, column: 8
                                        DecreaseCount2_o.setVal(true);//RoomControl.sysj line: 266, column: 8
                    S13837=1;
                    if(!DecreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                            DecreaseCount2_o.setREQ(false);//RoomControl.sysj line: 266, column: 8
                      ends[27]=2;
                      ;//RoomControl.sysj line: 266, column: 8
                      S13924=2;
                      active[27]=1;
                      ends[27]=1;
                      tdone[27]=1;
                    }
                    else {
                      active[27]=1;
                      ends[27]=1;
                      tdone[27]=1;
                    }
                  }
                  else {
                    active[27]=1;
                    ends[27]=1;
                    tdone[27]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 2 : 
            S13924=2;
            S13924=0;
            active[27]=1;
            ends[27]=1;
            tdone[27]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26341(int [] tdone, int [] ends){
        switch(S13834){
      case 0 : 
        active[26]=0;
        ends[26]=0;
        tdone[26]=1;
        break;
      
      case 1 : 
        switch(S13652){
          case 0 : 
            if(sInc_18.getprestatus()){//RoomControl.sysj line: 257, column: 12
              S13652=1;
                            IncreaseCount2_o.setPreempted(false);//RoomControl.sysj line: 258, column: 6
              S13570=0;
              if(!IncreaseCount2_o.isPartnerPresent() || IncreaseCount2_o.isPartnerPreempted()){//RoomControl.sysj line: 258, column: 6
                                IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                S13570=1;
                active[26]=1;
                ends[26]=1;
                tdone[26]=1;
              }
              else {
                S13565=0;
                if(IncreaseCount2_o.isACK()){//RoomControl.sysj line: 258, column: 6
                                    IncreaseCount2_o.setVal(true);//RoomControl.sysj line: 258, column: 6
                  S13565=1;
                  if(!IncreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                        IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                    ends[26]=2;
                    ;//RoomControl.sysj line: 258, column: 6
                    S13652=2;
                    active[26]=1;
                    ends[26]=1;
                    tdone[26]=1;
                  }
                  else {
                    active[26]=1;
                    ends[26]=1;
                    tdone[26]=1;
                  }
                }
                else {
                  active[26]=1;
                  ends[26]=1;
                  tdone[26]=1;
                }
              }
            }
            else {
              active[26]=1;
              ends[26]=1;
              tdone[26]=1;
            }
            break;
          
          case 1 : 
            switch(S13570){
              case 0 : 
                if(!IncreaseCount2_o.isPartnerPresent() || IncreaseCount2_o.isPartnerPreempted()){//RoomControl.sysj line: 258, column: 6
                                    IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                  S13570=1;
                  active[26]=1;
                  ends[26]=1;
                  tdone[26]=1;
                }
                else {
                  switch(S13565){
                    case 0 : 
                      if(IncreaseCount2_o.isACK()){//RoomControl.sysj line: 258, column: 6
                                                IncreaseCount2_o.setVal(true);//RoomControl.sysj line: 258, column: 6
                        S13565=1;
                        if(!IncreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                                    IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                          ends[26]=2;
                          ;//RoomControl.sysj line: 258, column: 6
                          S13652=2;
                          active[26]=1;
                          ends[26]=1;
                          tdone[26]=1;
                        }
                        else {
                          active[26]=1;
                          ends[26]=1;
                          tdone[26]=1;
                        }
                      }
                      else {
                        active[26]=1;
                        ends[26]=1;
                        tdone[26]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(!IncreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                                IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                        ends[26]=2;
                        ;//RoomControl.sysj line: 258, column: 6
                        S13652=2;
                        active[26]=1;
                        ends[26]=1;
                        tdone[26]=1;
                      }
                      else {
                        active[26]=1;
                        ends[26]=1;
                        tdone[26]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S13570=1;
                S13570=0;
                if(!IncreaseCount2_o.isPartnerPresent() || IncreaseCount2_o.isPartnerPreempted()){//RoomControl.sysj line: 258, column: 6
                                    IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                  S13570=1;
                  active[26]=1;
                  ends[26]=1;
                  tdone[26]=1;
                }
                else {
                  S13565=0;
                  if(IncreaseCount2_o.isACK()){//RoomControl.sysj line: 258, column: 6
                                        IncreaseCount2_o.setVal(true);//RoomControl.sysj line: 258, column: 6
                    S13565=1;
                    if(!IncreaseCount2_o.isACK()){//Unknown file line: 0, column: 0
                                            IncreaseCount2_o.setREQ(false);//RoomControl.sysj line: 258, column: 6
                      ends[26]=2;
                      ;//RoomControl.sysj line: 258, column: 6
                      S13652=2;
                      active[26]=1;
                      ends[26]=1;
                      tdone[26]=1;
                    }
                    else {
                      active[26]=1;
                      ends[26]=1;
                      tdone[26]=1;
                    }
                  }
                  else {
                    active[26]=1;
                    ends[26]=1;
                    tdone[26]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 2 : 
            S13652=2;
            S13652=0;
            active[26]=1;
            ends[26]=1;
            tdone[26]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26340(int [] tdone, int [] ends){
        switch(S13562){
      case 0 : 
        active[25]=0;
        ends[25]=0;
        tdone[25]=1;
        break;
      
      case 1 : 
        switch(S13370){
          case 0 : 
            switch(S13288){
              case 0 : 
                if(!ResetAC2_in.isPartnerPresent() || ResetAC2_in.isPartnerPreempted()){//RoomControl.sysj line: 246, column: 4
                  ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
                  S13288=1;
                  active[25]=1;
                  ends[25]=1;
                  tdone[25]=1;
                }
                else {
                  switch(S13283){
                    case 0 : 
                      if(!ResetAC2_in.isREQ()){//Unknown file line: 0, column: 0
                        ResetAC2_in.setACK(true);//RoomControl.sysj line: 246, column: 4
                        S13283=1;
                        if(ResetAC2_in.isREQ()){//RoomControl.sysj line: 246, column: 4
                          ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
                          ends[25]=2;
                          ;//RoomControl.sysj line: 246, column: 4
                          val_thread_25 = (Boolean)ResetAC2_in.getVal();//RoomControl.sysj line: 247, column: 4
                          if(val_thread_25){//RoomControl.sysj line: 248, column: 4
                            RAC_18.setPresent();//RoomControl.sysj line: 249, column: 6
                            currsigs.addElement(RAC_18);
                            System.out.println("Emitted RAC_18");
                            S13370=1;
                            active[25]=1;
                            ends[25]=1;
                            tdone[25]=1;
                          }
                          else {
                            S13370=1;
                            active[25]=1;
                            ends[25]=1;
                            tdone[25]=1;
                          }
                        }
                        else {
                          active[25]=1;
                          ends[25]=1;
                          tdone[25]=1;
                        }
                      }
                      else {
                        active[25]=1;
                        ends[25]=1;
                        tdone[25]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(ResetAC2_in.isREQ()){//RoomControl.sysj line: 246, column: 4
                        ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
                        ends[25]=2;
                        ;//RoomControl.sysj line: 246, column: 4
                        val_thread_25 = (Boolean)ResetAC2_in.getVal();//RoomControl.sysj line: 247, column: 4
                        if(val_thread_25){//RoomControl.sysj line: 248, column: 4
                          RAC_18.setPresent();//RoomControl.sysj line: 249, column: 6
                          currsigs.addElement(RAC_18);
                          System.out.println("Emitted RAC_18");
                          S13370=1;
                          active[25]=1;
                          ends[25]=1;
                          tdone[25]=1;
                        }
                        else {
                          S13370=1;
                          active[25]=1;
                          ends[25]=1;
                          tdone[25]=1;
                        }
                      }
                      else {
                        active[25]=1;
                        ends[25]=1;
                        tdone[25]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S13288=1;
                S13288=0;
                if(!ResetAC2_in.isPartnerPresent() || ResetAC2_in.isPartnerPreempted()){//RoomControl.sysj line: 246, column: 4
                  ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
                  S13288=1;
                  active[25]=1;
                  ends[25]=1;
                  tdone[25]=1;
                }
                else {
                  S13283=0;
                  if(!ResetAC2_in.isREQ()){//Unknown file line: 0, column: 0
                    ResetAC2_in.setACK(true);//RoomControl.sysj line: 246, column: 4
                    S13283=1;
                    if(ResetAC2_in.isREQ()){//RoomControl.sysj line: 246, column: 4
                      ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
                      ends[25]=2;
                      ;//RoomControl.sysj line: 246, column: 4
                      val_thread_25 = (Boolean)ResetAC2_in.getVal();//RoomControl.sysj line: 247, column: 4
                      if(val_thread_25){//RoomControl.sysj line: 248, column: 4
                        RAC_18.setPresent();//RoomControl.sysj line: 249, column: 6
                        currsigs.addElement(RAC_18);
                        System.out.println("Emitted RAC_18");
                        S13370=1;
                        active[25]=1;
                        ends[25]=1;
                        tdone[25]=1;
                      }
                      else {
                        S13370=1;
                        active[25]=1;
                        ends[25]=1;
                        tdone[25]=1;
                      }
                    }
                    else {
                      active[25]=1;
                      ends[25]=1;
                      tdone[25]=1;
                    }
                  }
                  else {
                    active[25]=1;
                    ends[25]=1;
                    tdone[25]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S13370=1;
            val_thread_25 = null;//RoomControl.sysj line: 245, column: 4
            S13370=0;
            ResetAC2_in.setPreempted(false);//RoomControl.sysj line: 246, column: 4
            S13288=0;
            if(!ResetAC2_in.isPartnerPresent() || ResetAC2_in.isPartnerPreempted()){//RoomControl.sysj line: 246, column: 4
              ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
              S13288=1;
              active[25]=1;
              ends[25]=1;
              tdone[25]=1;
            }
            else {
              S13283=0;
              if(!ResetAC2_in.isREQ()){//Unknown file line: 0, column: 0
                ResetAC2_in.setACK(true);//RoomControl.sysj line: 246, column: 4
                S13283=1;
                if(ResetAC2_in.isREQ()){//RoomControl.sysj line: 246, column: 4
                  ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
                  ends[25]=2;
                  ;//RoomControl.sysj line: 246, column: 4
                  val_thread_25 = (Boolean)ResetAC2_in.getVal();//RoomControl.sysj line: 247, column: 4
                  if(val_thread_25){//RoomControl.sysj line: 248, column: 4
                    RAC_18.setPresent();//RoomControl.sysj line: 249, column: 6
                    currsigs.addElement(RAC_18);
                    System.out.println("Emitted RAC_18");
                    S13370=1;
                    active[25]=1;
                    ends[25]=1;
                    tdone[25]=1;
                  }
                  else {
                    S13370=1;
                    active[25]=1;
                    ends[25]=1;
                    tdone[25]=1;
                  }
                }
                else {
                  active[25]=1;
                  ends[25]=1;
                  tdone[25]=1;
                }
              }
              else {
                active[25]=1;
                ends[25]=1;
                tdone[25]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26339(int [] tdone, int [] ends){
        switch(S13281){
      case 0 : 
        active[24]=0;
        ends[24]=0;
        tdone[24]=1;
        break;
      
      case 1 : 
        switch(S13089){
          case 0 : 
            switch(S13007){
              case 0 : 
                if(!Emergency2_in.isPartnerPresent() || Emergency2_in.isPartnerPreempted()){//RoomControl.sysj line: 233, column: 2
                  Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
                  S13007=1;
                  active[24]=1;
                  ends[24]=1;
                  tdone[24]=1;
                }
                else {
                  switch(S13002){
                    case 0 : 
                      if(!Emergency2_in.isREQ()){//Unknown file line: 0, column: 0
                        Emergency2_in.setACK(true);//RoomControl.sysj line: 233, column: 2
                        S13002=1;
                        if(Emergency2_in.isREQ()){//RoomControl.sysj line: 233, column: 2
                          Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
                          ends[24]=2;
                          ;//RoomControl.sysj line: 233, column: 2
                          val_thread_24 = (Boolean)Emergency2_in.getVal();//RoomControl.sysj line: 234, column: 2
                          if(val_thread_24){//RoomControl.sysj line: 235, column: 2
                            Preempt_18.setPresent();//RoomControl.sysj line: 236, column: 4
                            currsigs.addElement(Preempt_18);
                            System.out.println("Emitted Preempt_18");
                            S13089=1;
                            active[24]=1;
                            ends[24]=1;
                            tdone[24]=1;
                          }
                          else {
                            S13089=1;
                            active[24]=1;
                            ends[24]=1;
                            tdone[24]=1;
                          }
                        }
                        else {
                          active[24]=1;
                          ends[24]=1;
                          tdone[24]=1;
                        }
                      }
                      else {
                        active[24]=1;
                        ends[24]=1;
                        tdone[24]=1;
                      }
                      break;
                    
                    case 1 : 
                      if(Emergency2_in.isREQ()){//RoomControl.sysj line: 233, column: 2
                        Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
                        ends[24]=2;
                        ;//RoomControl.sysj line: 233, column: 2
                        val_thread_24 = (Boolean)Emergency2_in.getVal();//RoomControl.sysj line: 234, column: 2
                        if(val_thread_24){//RoomControl.sysj line: 235, column: 2
                          Preempt_18.setPresent();//RoomControl.sysj line: 236, column: 4
                          currsigs.addElement(Preempt_18);
                          System.out.println("Emitted Preempt_18");
                          S13089=1;
                          active[24]=1;
                          ends[24]=1;
                          tdone[24]=1;
                        }
                        else {
                          S13089=1;
                          active[24]=1;
                          ends[24]=1;
                          tdone[24]=1;
                        }
                      }
                      else {
                        active[24]=1;
                        ends[24]=1;
                        tdone[24]=1;
                      }
                      break;
                    
                  }
                }
                break;
              
              case 1 : 
                S13007=1;
                S13007=0;
                if(!Emergency2_in.isPartnerPresent() || Emergency2_in.isPartnerPreempted()){//RoomControl.sysj line: 233, column: 2
                  Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
                  S13007=1;
                  active[24]=1;
                  ends[24]=1;
                  tdone[24]=1;
                }
                else {
                  S13002=0;
                  if(!Emergency2_in.isREQ()){//Unknown file line: 0, column: 0
                    Emergency2_in.setACK(true);//RoomControl.sysj line: 233, column: 2
                    S13002=1;
                    if(Emergency2_in.isREQ()){//RoomControl.sysj line: 233, column: 2
                      Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
                      ends[24]=2;
                      ;//RoomControl.sysj line: 233, column: 2
                      val_thread_24 = (Boolean)Emergency2_in.getVal();//RoomControl.sysj line: 234, column: 2
                      if(val_thread_24){//RoomControl.sysj line: 235, column: 2
                        Preempt_18.setPresent();//RoomControl.sysj line: 236, column: 4
                        currsigs.addElement(Preempt_18);
                        System.out.println("Emitted Preempt_18");
                        S13089=1;
                        active[24]=1;
                        ends[24]=1;
                        tdone[24]=1;
                      }
                      else {
                        S13089=1;
                        active[24]=1;
                        ends[24]=1;
                        tdone[24]=1;
                      }
                    }
                    else {
                      active[24]=1;
                      ends[24]=1;
                      tdone[24]=1;
                    }
                  }
                  else {
                    active[24]=1;
                    ends[24]=1;
                    tdone[24]=1;
                  }
                }
                break;
              
            }
            break;
          
          case 1 : 
            S13089=1;
            val_thread_24 = null;//RoomControl.sysj line: 232, column: 2
            S13089=0;
            Emergency2_in.setPreempted(false);//RoomControl.sysj line: 233, column: 2
            S13007=0;
            if(!Emergency2_in.isPartnerPresent() || Emergency2_in.isPartnerPreempted()){//RoomControl.sysj line: 233, column: 2
              Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
              S13007=1;
              active[24]=1;
              ends[24]=1;
              tdone[24]=1;
            }
            else {
              S13002=0;
              if(!Emergency2_in.isREQ()){//Unknown file line: 0, column: 0
                Emergency2_in.setACK(true);//RoomControl.sysj line: 233, column: 2
                S13002=1;
                if(Emergency2_in.isREQ()){//RoomControl.sysj line: 233, column: 2
                  Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
                  ends[24]=2;
                  ;//RoomControl.sysj line: 233, column: 2
                  val_thread_24 = (Boolean)Emergency2_in.getVal();//RoomControl.sysj line: 234, column: 2
                  if(val_thread_24){//RoomControl.sysj line: 235, column: 2
                    Preempt_18.setPresent();//RoomControl.sysj line: 236, column: 4
                    currsigs.addElement(Preempt_18);
                    System.out.println("Emitted Preempt_18");
                    S13089=1;
                    active[24]=1;
                    ends[24]=1;
                    tdone[24]=1;
                  }
                  else {
                    S13089=1;
                    active[24]=1;
                    ends[24]=1;
                    tdone[24]=1;
                  }
                }
                else {
                  active[24]=1;
                  ends[24]=1;
                  tdone[24]=1;
                }
              }
              else {
                active[24]=1;
                ends[24]=1;
                tdone[24]=1;
              }
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26337(int [] tdone, int [] ends){
        switch(S2816){
      case 0 : 
        active[23]=0;
        ends[23]=0;
        tdone[23]=1;
        break;
      
      case 1 : 
        switch(S2803){
          case 0 : 
            if(i_thread_23 == 4){//RoomControl.sysj line: 200, column: 7
              ends[23]=3;
              ;//RoomControl.sysj line: 198, column: 7
              S2803=1;
              beep_18.setPresent();//RoomControl.sysj line: 205, column: 7
              currsigs.addElement(beep_18);
              System.out.println("Emitted beep_18");
              active[23]=1;
              ends[23]=1;
              tdone[23]=1;
            }
            else {
              i_thread_23 = i_thread_23 + 1;//RoomControl.sysj line: 201, column: 4
              active[23]=1;
              ends[23]=1;
              tdone[23]=1;
            }
            break;
          
          case 1 : 
            beep_18.setPresent();//RoomControl.sysj line: 205, column: 7
            currsigs.addElement(beep_18);
            System.out.println("Emitted beep_18");
            active[23]=1;
            ends[23]=1;
            tdone[23]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26335(int [] tdone, int [] ends){
        switch(S2786){
      case 0 : 
        active[22]=0;
        ends[22]=0;
        tdone[22]=1;
        break;
      
      case 1 : 
        if(ExitRegistered_14.getprestatus()){//RoomControl.sysj line: 190, column: 55
          sDec_18.setPresent();//RoomControl.sysj line: 190, column: 72
          currsigs.addElement(sDec_18);
          System.out.println("Emitted sDec_18");
          S2786=0;
          active[22]=0;
          ends[22]=0;
          tdone[22]=1;
        }
        else {
          active[22]=1;
          ends[22]=1;
          tdone[22]=1;
        }
        break;
      
    }
  }

  public void thread26334(int [] tdone, int [] ends){
        switch(S2783){
      case 0 : 
        active[21]=0;
        ends[21]=0;
        tdone[21]=1;
        break;
      
      case 1 : 
        if(EntryRegistered_14.getprestatus()){//RoomControl.sysj line: 188, column: 54
          sInc_18.setPresent();//RoomControl.sysj line: 188, column: 72
          currsigs.addElement(sInc_18);
          System.out.println("Emitted sInc_18");
          S2783=0;
          active[21]=0;
          ends[21]=0;
          tdone[21]=1;
        }
        else {
          active[21]=1;
          ends[21]=1;
          tdone[21]=1;
        }
        break;
      
    }
  }

  public void thread26333(int [] tdone, int [] ends){
        switch(S2788){
      case 0 : 
        active[20]=0;
        ends[20]=0;
        tdone[20]=1;
        break;
      
      case 1 : 
        thread26334(tdone,ends);
        thread26335(tdone,ends);
        int biggest26336 = 0;
        if(ends[21]>=biggest26336){
          biggest26336=ends[21];
        }
        if(ends[22]>=biggest26336){
          biggest26336=ends[22];
        }
        if(biggest26336 == 1){
          active[20]=1;
          ends[20]=1;
          tdone[20]=1;
        }
        //FINXME code
        if(biggest26336 == 0){
          S2788=0;
          active[20]=0;
          ends[20]=0;
          tdone[20]=1;
        }
        break;
      
    }
  }

  public void thread26331(int [] tdone, int [] ends){
        S2816=1;
    i_thread_23 = 0;//RoomControl.sysj line: 197, column: 7
    S2803=0;
    if(i_thread_23 == 4){//RoomControl.sysj line: 200, column: 7
      ends[23]=3;
      ;//RoomControl.sysj line: 198, column: 7
      S2803=1;
      beep_18.setPresent();//RoomControl.sysj line: 205, column: 7
      currsigs.addElement(beep_18);
      System.out.println("Emitted beep_18");
      active[23]=1;
      ends[23]=1;
      tdone[23]=1;
    }
    else {
      i_thread_23 = i_thread_23 + 1;//RoomControl.sysj line: 201, column: 4
      active[23]=1;
      ends[23]=1;
      tdone[23]=1;
    }
  }

  public void thread26329(int [] tdone, int [] ends){
        S2786=1;
    active[22]=1;
    ends[22]=1;
    tdone[22]=1;
  }

  public void thread26328(int [] tdone, int [] ends){
        S2783=1;
    active[21]=1;
    ends[21]=1;
    tdone[21]=1;
  }

  public void thread26327(int [] tdone, int [] ends){
        S2788=1;
    thread26328(tdone,ends);
    thread26329(tdone,ends);
    int biggest26330 = 0;
    if(ends[21]>=biggest26330){
      biggest26330=ends[21];
    }
    if(ends[22]>=biggest26330){
      biggest26330=ends[22];
    }
    if(biggest26330 == 1){
      active[20]=1;
      ends[20]=1;
      tdone[20]=1;
    }
  }

  public void thread26326(int [] tdone, int [] ends){
        switch(S13000){
      case 0 : 
        active[19]=0;
        ends[19]=0;
        tdone[19]=1;
        break;
      
      case 1 : 
        switch(S6140){
          case 0 : 
            if(Preempt_18.getprestatus()){//RoomControl.sysj line: 156, column: 13
              S6140=1;
              active[19]=1;
              ends[19]=1;
              tdone[19]=1;
            }
            else {
              switch(S2764){
                case 0 : 
                  if(EntryRequest_14.getprestatus() && ExitRequest_14.getprestatus()){//RoomControl.sysj line: 159, column: 4
                    active[19]=1;
                    ends[19]=1;
                    tdone[19]=1;
                  }
                  else {
                    if(EntryRequest_14.getprestatus() || ExitRequest_14.getprestatus()){//RoomControl.sysj line: 161, column: 6
                      if(EntryRequest_14.getprestatus()){//RoomControl.sysj line: 163, column: 12
                        id_thread_19 = ((String)EntryRequest_14.getpreval()).trim();//RoomControl.sysj line: 164, column: 6
                        S2764=1;
                        if(Entries.isValidRequest(id_thread_19)){//RoomControl.sysj line: 169, column: 4
                          UnlockDoor_14.setPresent();//RoomControl.sysj line: 170, column: 6
                          currsigs.addElement(UnlockDoor_14);
                          System.out.println("Emitted UnlockDoor_14");
                          S2855=0;
                          i_thread_19 = 0;//RoomControl.sysj line: 175, column: 3
                          if(i_thread_19 == 4){//RoomControl.sysj line: 178, column: 10
                            ends[19]=4;
                            ;//RoomControl.sysj line: 176, column: 3
                            ends[19]=2;
                            ;//RoomControl.sysj line: 172, column: 6
                            LockDoor_14.setPresent();//RoomControl.sysj line: 209, column: 6
                            currsigs.addElement(LockDoor_14);
                            System.out.println("Emitted LockDoor_14");
                            S2764=2;
                            active[19]=1;
                            ends[19]=1;
                            tdone[19]=1;
                          }
                          else {
                            i_thread_19 = i_thread_19 + 1;//RoomControl.sysj line: 179, column: 7
                            active[19]=1;
                            ends[19]=1;
                            tdone[19]=1;
                          }
                        }
                        else {
                          InvalidCard_14.setPresent();//RoomControl.sysj line: 212, column: 6
                          currsigs.addElement(InvalidCard_14);
                          System.out.println("Emitted InvalidCard_14");
                          S2764=2;
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                      }
                      else {
                        id_thread_19 = ((String)ExitRequest_14.getpreval()).trim();//RoomControl.sysj line: 167, column: 6
                        S2764=1;
                        if(Entries.isValidRequest(id_thread_19)){//RoomControl.sysj line: 169, column: 4
                          UnlockDoor_14.setPresent();//RoomControl.sysj line: 170, column: 6
                          currsigs.addElement(UnlockDoor_14);
                          System.out.println("Emitted UnlockDoor_14");
                          S2855=0;
                          i_thread_19 = 0;//RoomControl.sysj line: 175, column: 3
                          if(i_thread_19 == 4){//RoomControl.sysj line: 178, column: 10
                            ends[19]=4;
                            ;//RoomControl.sysj line: 176, column: 3
                            ends[19]=2;
                            ;//RoomControl.sysj line: 172, column: 6
                            LockDoor_14.setPresent();//RoomControl.sysj line: 209, column: 6
                            currsigs.addElement(LockDoor_14);
                            System.out.println("Emitted LockDoor_14");
                            S2764=2;
                            active[19]=1;
                            ends[19]=1;
                            tdone[19]=1;
                          }
                          else {
                            i_thread_19 = i_thread_19 + 1;//RoomControl.sysj line: 179, column: 7
                            active[19]=1;
                            ends[19]=1;
                            tdone[19]=1;
                          }
                        }
                        else {
                          InvalidCard_14.setPresent();//RoomControl.sysj line: 212, column: 6
                          currsigs.addElement(InvalidCard_14);
                          System.out.println("Emitted InvalidCard_14");
                          S2764=2;
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                      }
                    }
                    else {
                      active[19]=1;
                      ends[19]=1;
                      tdone[19]=1;
                    }
                  }
                  break;
                
                case 1 : 
                  switch(S2855){
                    case 0 : 
                      if(DoorOpened_14.getprestatus()){//RoomControl.sysj line: 173, column: 14
                        S2855=1;
                        thread26327(tdone,ends);
                        thread26331(tdone,ends);
                        int biggest26332 = 0;
                        if(ends[20]>=biggest26332){
                          biggest26332=ends[20];
                        }
                        if(ends[23]>=biggest26332){
                          biggest26332=ends[23];
                        }
                        if(biggest26332 == 1){
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                      }
                      else {
                        if(i_thread_19 == 4){//RoomControl.sysj line: 178, column: 10
                          ends[19]=4;
                          ;//RoomControl.sysj line: 176, column: 3
                          ends[19]=2;
                          ;//RoomControl.sysj line: 172, column: 6
                          LockDoor_14.setPresent();//RoomControl.sysj line: 209, column: 6
                          currsigs.addElement(LockDoor_14);
                          System.out.println("Emitted LockDoor_14");
                          S2764=2;
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                        else {
                          i_thread_19 = i_thread_19 + 1;//RoomControl.sysj line: 179, column: 7
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                      }
                      break;
                    
                    case 1 : 
                      if(DoorClosed_14.getprestatus()){//RoomControl.sysj line: 186, column: 14
                        LockDoor_14.setPresent();//RoomControl.sysj line: 209, column: 6
                        currsigs.addElement(LockDoor_14);
                        System.out.println("Emitted LockDoor_14");
                        S2764=2;
                        active[19]=1;
                        ends[19]=1;
                        tdone[19]=1;
                      }
                      else {
                        thread26333(tdone,ends);
                        thread26337(tdone,ends);
                        int biggest26338 = 0;
                        if(ends[20]>=biggest26338){
                          biggest26338=ends[20];
                        }
                        if(ends[23]>=biggest26338){
                          biggest26338=ends[23];
                        }
                        if(biggest26338 == 1){
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                        //FINXME code
                        if(biggest26338 == 0){
                          LockDoor_14.setPresent();//RoomControl.sysj line: 209, column: 6
                          currsigs.addElement(LockDoor_14);
                          System.out.println("Emitted LockDoor_14");
                          S2764=2;
                          active[19]=1;
                          ends[19]=1;
                          tdone[19]=1;
                        }
                      }
                      break;
                    
                  }
                  break;
                
                case 2 : 
                  S2764=2;
                  S2764=0;
                  active[19]=1;
                  ends[19]=1;
                  tdone[19]=1;
                  break;
                
              }
            }
            break;
          
          case 1 : 
            S6140=1;
            S6140=2;
            S6145=0;
            if(DoorClosed_14.getprestatus()){//RoomControl.sysj line: 220, column: 12
              LockDoor_14.setPresent();//RoomControl.sysj line: 221, column: 6
              currsigs.addElement(LockDoor_14);
              System.out.println("Emitted LockDoor_14");
              active[19]=1;
              ends[19]=1;
              tdone[19]=1;
            }
            else {
              S6145=1;
              active[19]=1;
              ends[19]=1;
              tdone[19]=1;
            }
            break;
          
          case 2 : 
            if(RAC_18.getprestatus()){//RoomControl.sysj line: 218, column: 13
              S6140=3;
              active[19]=1;
              ends[19]=1;
              tdone[19]=1;
            }
            else {
              switch(S6145){
                case 0 : 
                  LockDoor_14.setPresent();//RoomControl.sysj line: 221, column: 6
                  currsigs.addElement(LockDoor_14);
                  System.out.println("Emitted LockDoor_14");
                  active[19]=1;
                  ends[19]=1;
                  tdone[19]=1;
                  break;
                
                case 1 : 
                  S6145=1;
                  S6145=0;
                  if(DoorClosed_14.getprestatus()){//RoomControl.sysj line: 220, column: 12
                    LockDoor_14.setPresent();//RoomControl.sysj line: 221, column: 6
                    currsigs.addElement(LockDoor_14);
                    System.out.println("Emitted LockDoor_14");
                    active[19]=1;
                    ends[19]=1;
                    tdone[19]=1;
                  }
                  else {
                    S6145=1;
                    active[19]=1;
                    ends[19]=1;
                    tdone[19]=1;
                  }
                  break;
                
              }
            }
            break;
          
          case 3 : 
            S6140=3;
            S6140=0;
            id_thread_19 = null;//RoomControl.sysj line: 157, column: 2
            S2764=0;
            active[19]=1;
            ends[19]=1;
            tdone[19]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26325(int [] tdone, int [] ends){
        switch(S25455){
      case 0 : 
        active[18]=0;
        ends[18]=0;
        tdone[18]=1;
        break;
      
      case 1 : 
        beep_18.setClear();//RoomControl.sysj line: 152, column: 3
        RAC_18.setClear();//RoomControl.sysj line: 152, column: 3
        Preempt_18.setClear();//RoomControl.sysj line: 152, column: 3
        sInc_18.setClear();//RoomControl.sysj line: 152, column: 3
        sDec_18.setClear();//RoomControl.sysj line: 152, column: 3
        thread26326(tdone,ends);
        thread26339(tdone,ends);
        thread26340(tdone,ends);
        thread26341(tdone,ends);
        thread26342(tdone,ends);
        int biggest26343 = 0;
        if(ends[19]>=biggest26343){
          biggest26343=ends[19];
        }
        if(ends[24]>=biggest26343){
          biggest26343=ends[24];
        }
        if(ends[25]>=biggest26343){
          biggest26343=ends[25];
        }
        if(ends[26]>=biggest26343){
          biggest26343=ends[26];
        }
        if(ends[27]>=biggest26343){
          biggest26343=ends[27];
        }
        if(biggest26343 == 1){
          active[18]=1;
          ends[18]=1;
          tdone[18]=1;
        }
        //FINXME code
        if(biggest26343 == 0){
          S25455=0;
          active[18]=0;
          ends[18]=0;
          tdone[18]=1;
        }
        break;
      
    }
  }

  public void thread26323(int [] tdone, int [] ends){
        switch(S2751){
      case 0 : 
        active[17]=0;
        ends[17]=0;
        tdone[17]=1;
        break;
      
      case 1 : 
        switch(S2620){
          case 0 : 
            if(s1_14.getprestatus()){//RoomControl.sysj line: 363, column: 12
              S2620=1;
              if(random_thread_17.nextInt(2) == 0){//RoomControl.sysj line: 364, column: 9
                S2607=0;
                S2596=0;
                active[17]=1;
                ends[17]=1;
                tdone[17]=1;
              }
              else {
                S2607=1;
                if(random_thread_17.nextInt(2) == 0){//RoomControl.sysj line: 369, column: 11
                  S2605=0;
                  S2597=0;
                  active[17]=1;
                  ends[17]=1;
                  tdone[17]=1;
                }
                else {
                  S2605=1;
                  if(p_thread_17 > 20) {//RoomControl.sysj line: 375, column: 3
                    h_thread_17 = false;//RoomControl.sysj line: 376, column: 5
                  }
                  else {//RoomControl.sysj line: 375, column: 3
                    if(p_thread_17 == 0) {//RoomControl.sysj line: 377, column: 8
                      h_thread_17 = true;//RoomControl.sysj line: 378, column: 5
                    }
                  }
                  if(h_thread_17){//RoomControl.sysj line: 379, column: 3
                    p_thread_17 = p_thread_17 + 1;//RoomControl.sysj line: 380, column: 5
                    EntryRegistered_14.setPresent();//RoomControl.sysj line: 381, column: 5
                    currsigs.addElement(EntryRegistered_14);
                    System.out.println("Emitted EntryRegistered_14");
                    S2604=0;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                  }
                  else {
                    p_thread_17 = p_thread_17 - 1;//RoomControl.sysj line: 384, column: 5
                    ExitRegistered_14.setPresent();//RoomControl.sysj line: 385, column: 5
                    currsigs.addElement(ExitRegistered_14);
                    System.out.println("Emitted ExitRegistered_14");
                    S2604=0;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                  }
                }
              }
            }
            else {
              active[17]=1;
              ends[17]=1;
              tdone[17]=1;
            }
            break;
          
          case 1 : 
            switch(S2607){
              case 0 : 
                switch(S2596){
                  case 0 : 
                    S2596=0;
                    S2596=1;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                    break;
                  
                  case 1 : 
                    S2596=1;
                    S2596=2;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                    break;
                  
                  case 2 : 
                    S2596=2;
                    S2596=3;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                    break;
                  
                  case 3 : 
                    S2596=3;
                    S2596=4;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                    break;
                  
                  case 4 : 
                    S2596=4;
                    S2596=5;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                    break;
                  
                  case 5 : 
                    S2596=5;
                    s2_14.setPresent();//RoomControl.sysj line: 392, column: 6
                    currsigs.addElement(s2_14);
                    System.out.println("Emitted s2_14");
                    S2620=2;
                    active[17]=1;
                    ends[17]=1;
                    tdone[17]=1;
                    break;
                  
                }
                break;
              
              case 1 : 
                switch(S2605){
                  case 0 : 
                    switch(S2597){
                      case 0 : 
                        S2597=0;
                        S2597=1;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 1 : 
                        S2597=1;
                        S2597=2;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 2 : 
                        S2597=2;
                        S2597=3;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 3 : 
                        S2597=3;
                        S2597=4;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 4 : 
                        S2597=4;
                        S2597=5;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 5 : 
                        S2597=5;
                        S2597=6;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 6 : 
                        S2597=6;
                        S2597=7;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 7 : 
                        S2597=7;
                        S2597=8;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 8 : 
                        S2597=8;
                        DoorClosed_14.setPresent();//RoomControl.sysj line: 390, column: 8
                        currsigs.addElement(DoorClosed_14);
                        System.out.println("Emitted DoorClosed_14");
                        s2_14.setPresent();//RoomControl.sysj line: 392, column: 6
                        currsigs.addElement(s2_14);
                        System.out.println("Emitted s2_14");
                        S2620=2;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                    }
                    break;
                  
                  case 1 : 
                    switch(S2604){
                      case 0 : 
                        S2604=0;
                        S2604=1;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 1 : 
                        S2604=1;
                        S2604=2;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 2 : 
                        S2604=2;
                        S2604=3;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 3 : 
                        S2604=3;
                        S2604=4;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 4 : 
                        S2604=4;
                        S2604=5;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                      case 5 : 
                        S2604=5;
                        DoorClosed_14.setPresent();//RoomControl.sysj line: 390, column: 8
                        currsigs.addElement(DoorClosed_14);
                        System.out.println("Emitted DoorClosed_14");
                        s2_14.setPresent();//RoomControl.sysj line: 392, column: 6
                        currsigs.addElement(s2_14);
                        System.out.println("Emitted s2_14");
                        S2620=2;
                        active[17]=1;
                        ends[17]=1;
                        tdone[17]=1;
                        break;
                      
                    }
                    break;
                  
                }
                break;
              
            }
            break;
          
          case 2 : 
            S2620=2;
            S2620=0;
            active[17]=1;
            ends[17]=1;
            tdone[17]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread26322(int [] tdone, int [] ends){
        switch(S2593){
      case 0 : 
        active[16]=0;
        ends[16]=0;
        tdone[16]=1;
        break;
      
      case 1 : 
        switch(S2521){
          case 0 : 
            switch(S2520){
              case 0 : 
                switch(S2508){
                  case 0 : 
                    S2508=0;
                    S2508=1;
                    active[16]=1;
                    ends[16]=1;
                    tdone[16]=1;
                    break;
                  
                  case 1 : 
                    S2508=1;
                    S2508=2;
                    active[16]=1;
                    ends[16]=1;
                    tdone[16]=1;
                    break;
                  
                  case 2 : 
                    S2508=2;
                    S2521=1;
                    active[16]=1;
                    ends[16]=1;
                    tdone[16]=1;
                    break;
                  
                }
                break;
              
              case 1 : 
                switch(S2509){
                  case 0 : 
                    S2509=0;
                    S2509=1;
                    active[16]=1;
                    ends[16]=1;
                    tdone[16]=1;
                    break;
                  
                  case 1 : 
                    S2509=1;
                    S2509=2;
                    active[16]=1;
                    ends[16]=1;
                    tdone[16]=1;
                    break;
                  
                  case 2 : 
                    S2509=2;
                    S2509=3;
                    if(random_thread_16.nextInt(2) == 0){//RoomControl.sysj line: 343, column: 9
                      S2514=0;
                      S2510=0;
                      active[16]=1;
                      ends[16]=1;
                      tdone[16]=1;
                    }
                    else {
                      S2514=1;
                      DoorOpened_14.setPresent();//RoomControl.sysj line: 348, column: 8
                      currsigs.addElement(DoorOpened_14);
                      System.out.println("Emitted DoorOpened_14");
                      S2511=0;
                      active[16]=1;
                      ends[16]=1;
                      tdone[16]=1;
                    }
                    break;
                  
                  case 3 : 
                    switch(S2514){
                      case 0 : 
                        switch(S2510){
                          case 0 : 
                            S2510=0;
                            S2510=1;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 1 : 
                            S2510=1;
                            S2510=2;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 2 : 
                            S2510=2;
                            S2510=3;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 3 : 
                            S2510=3;
                            S2510=4;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 4 : 
                            S2510=4;
                            S2510=5;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 5 : 
                            S2510=5;
                            S2521=1;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                        }
                        break;
                      
                      case 1 : 
                        switch(S2511){
                          case 0 : 
                            S2511=0;
                            S2511=1;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 1 : 
                            S2511=1;
                            S2511=2;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 2 : 
                            S2511=2;
                            s1_14.setPresent();//RoomControl.sysj line: 350, column: 8
                            currsigs.addElement(s1_14);
                            System.out.println("Emitted s1_14");
                            S2511=3;
                            active[16]=1;
                            ends[16]=1;
                            tdone[16]=1;
                            break;
                          
                          case 3 : 
                            if(s2_14.getprestatus()){//RoomControl.sysj line: 351, column: 14
                              S2521=1;
                              active[16]=1;
                              ends[16]=1;
                              tdone[16]=1;
                            }
                            else {
                              active[16]=1;
                              ends[16]=1;
                              tdone[16]=1;
                            }
                            break;
                          
                        }
                        break;
                      
                    }
                    break;
                  
                }
                break;
              
            }
            break;
          
          case 1 : 
            S2521=1;
            S2521=0;
            if(random_thread_16.nextInt(3) > 0){//RoomControl.sysj line: 336, column: 7
              S2520=0;
              EntryRequest_14.setPresent();//RoomControl.sysj line: 337, column: 6
              currsigs.addElement(EntryRequest_14);
              EntryRequest_14.setValue("whoisthis");//RoomControl.sysj line: 337, column: 6
              System.out.println("Emitted EntryRequest_14");
              S2508=0;
              active[16]=1;
              ends[16]=1;
              tdone[16]=1;
            }
            else {
              S2520=1;
              EntryRequest_14.setPresent();//RoomControl.sysj line: 341, column: 6
              currsigs.addElement(EntryRequest_14);
              EntryRequest_14.setValue("paul");//RoomControl.sysj line: 341, column: 6
              System.out.println("Emitted EntryRequest_14");
              S2509=0;
              active[16]=1;
              ends[16]=1;
              tdone[16]=1;
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread26321(int [] tdone, int [] ends){
        switch(S2753){
      case 0 : 
        active[15]=0;
        ends[15]=0;
        tdone[15]=1;
        break;
      
      case 1 : 
        s1_14.setClear();//RoomControl.sysj line: 332, column: 7
        s2_14.setClear();//RoomControl.sysj line: 332, column: 7
        thread26322(tdone,ends);
        thread26323(tdone,ends);
        int biggest26324 = 0;
        if(ends[16]>=biggest26324){
          biggest26324=ends[16];
        }
        if(ends[17]>=biggest26324){
          biggest26324=ends[17];
        }
        if(biggest26324 == 1){
          active[15]=1;
          ends[15]=1;
          tdone[15]=1;
        }
        //FINXME code
        if(biggest26324 == 0){
          S2753=0;
          active[15]=0;
          ends[15]=0;
          tdone[15]=1;
        }
        break;
      
    }
  }

  public void thread26318(int [] tdone, int [] ends){
        S14106=1;
    S13924=0;
    active[27]=1;
    ends[27]=1;
    tdone[27]=1;
  }

  public void thread26317(int [] tdone, int [] ends){
        S13834=1;
    S13652=0;
    active[26]=1;
    ends[26]=1;
    tdone[26]=1;
  }

  public void thread26316(int [] tdone, int [] ends){
        S13562=1;
    val_thread_25 = null;//RoomControl.sysj line: 245, column: 4
    S13370=0;
    ResetAC2_in.setPreempted(false);//RoomControl.sysj line: 246, column: 4
    S13288=0;
    if(!ResetAC2_in.isPartnerPresent() || ResetAC2_in.isPartnerPreempted()){//RoomControl.sysj line: 246, column: 4
      ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
      S13288=1;
      active[25]=1;
      ends[25]=1;
      tdone[25]=1;
    }
    else {
      S13283=0;
      if(!ResetAC2_in.isREQ()){//Unknown file line: 0, column: 0
        ResetAC2_in.setACK(true);//RoomControl.sysj line: 246, column: 4
        S13283=1;
        if(ResetAC2_in.isREQ()){//RoomControl.sysj line: 246, column: 4
          ResetAC2_in.setACK(false);//RoomControl.sysj line: 246, column: 4
          ends[25]=2;
          ;//RoomControl.sysj line: 246, column: 4
          val_thread_25 = (Boolean)ResetAC2_in.getVal();//RoomControl.sysj line: 247, column: 4
          if(val_thread_25){//RoomControl.sysj line: 248, column: 4
            RAC_18.setPresent();//RoomControl.sysj line: 249, column: 6
            currsigs.addElement(RAC_18);
            System.out.println("Emitted RAC_18");
            S13370=1;
            active[25]=1;
            ends[25]=1;
            tdone[25]=1;
          }
          else {
            S13370=1;
            active[25]=1;
            ends[25]=1;
            tdone[25]=1;
          }
        }
        else {
          active[25]=1;
          ends[25]=1;
          tdone[25]=1;
        }
      }
      else {
        active[25]=1;
        ends[25]=1;
        tdone[25]=1;
      }
    }
  }

  public void thread26315(int [] tdone, int [] ends){
        S13281=1;
    val_thread_24 = null;//RoomControl.sysj line: 232, column: 2
    S13089=0;
    Emergency2_in.setPreempted(false);//RoomControl.sysj line: 233, column: 2
    S13007=0;
    if(!Emergency2_in.isPartnerPresent() || Emergency2_in.isPartnerPreempted()){//RoomControl.sysj line: 233, column: 2
      Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
      S13007=1;
      active[24]=1;
      ends[24]=1;
      tdone[24]=1;
    }
    else {
      S13002=0;
      if(!Emergency2_in.isREQ()){//Unknown file line: 0, column: 0
        Emergency2_in.setACK(true);//RoomControl.sysj line: 233, column: 2
        S13002=1;
        if(Emergency2_in.isREQ()){//RoomControl.sysj line: 233, column: 2
          Emergency2_in.setACK(false);//RoomControl.sysj line: 233, column: 2
          ends[24]=2;
          ;//RoomControl.sysj line: 233, column: 2
          val_thread_24 = (Boolean)Emergency2_in.getVal();//RoomControl.sysj line: 234, column: 2
          if(val_thread_24){//RoomControl.sysj line: 235, column: 2
            Preempt_18.setPresent();//RoomControl.sysj line: 236, column: 4
            currsigs.addElement(Preempt_18);
            System.out.println("Emitted Preempt_18");
            S13089=1;
            active[24]=1;
            ends[24]=1;
            tdone[24]=1;
          }
          else {
            S13089=1;
            active[24]=1;
            ends[24]=1;
            tdone[24]=1;
          }
        }
        else {
          active[24]=1;
          ends[24]=1;
          tdone[24]=1;
        }
      }
      else {
        active[24]=1;
        ends[24]=1;
        tdone[24]=1;
      }
    }
  }

  public void thread26314(int [] tdone, int [] ends){
        S13000=1;
    S6140=0;
    id_thread_19 = null;//RoomControl.sysj line: 157, column: 2
    S2764=0;
    active[19]=1;
    ends[19]=1;
    tdone[19]=1;
  }

  public void thread26313(int [] tdone, int [] ends){
        S25455=1;
    beep_18.setClear();//RoomControl.sysj line: 152, column: 3
    RAC_18.setClear();//RoomControl.sysj line: 152, column: 3
    Preempt_18.setClear();//RoomControl.sysj line: 152, column: 3
    sInc_18.setClear();//RoomControl.sysj line: 152, column: 3
    sDec_18.setClear();//RoomControl.sysj line: 152, column: 3
    Debug.setEnabled();//RoomControl.sysj line: 153, column: 3
    thread26314(tdone,ends);
    thread26315(tdone,ends);
    thread26316(tdone,ends);
    thread26317(tdone,ends);
    thread26318(tdone,ends);
    int biggest26319 = 0;
    if(ends[19]>=biggest26319){
      biggest26319=ends[19];
    }
    if(ends[24]>=biggest26319){
      biggest26319=ends[24];
    }
    if(ends[25]>=biggest26319){
      biggest26319=ends[25];
    }
    if(ends[26]>=biggest26319){
      biggest26319=ends[26];
    }
    if(ends[27]>=biggest26319){
      biggest26319=ends[27];
    }
    if(biggest26319 == 1){
      active[18]=1;
      ends[18]=1;
      tdone[18]=1;
    }
  }

  public void thread26311(int [] tdone, int [] ends){
        S2751=1;
    p_thread_17 = 1;//RoomControl.sysj line: 359, column: 4
    h_thread_17 = true;//RoomControl.sysj line: 360, column: 4
    random_thread_17 = new Random(22323123);//RoomControl.sysj line: 361, column: 4
    S2620=0;
    active[17]=1;
    ends[17]=1;
    tdone[17]=1;
  }

  public void thread26310(int [] tdone, int [] ends){
        S2593=1;
    random_thread_16 = new Random(234123123);//RoomControl.sysj line: 334, column: 2
    S2521=0;
    if(random_thread_16.nextInt(3) > 0){//RoomControl.sysj line: 336, column: 7
      S2520=0;
      EntryRequest_14.setPresent();//RoomControl.sysj line: 337, column: 6
      currsigs.addElement(EntryRequest_14);
      EntryRequest_14.setValue("whoisthis");//RoomControl.sysj line: 337, column: 6
      System.out.println("Emitted EntryRequest_14");
      S2508=0;
      active[16]=1;
      ends[16]=1;
      tdone[16]=1;
    }
    else {
      S2520=1;
      EntryRequest_14.setPresent();//RoomControl.sysj line: 341, column: 6
      currsigs.addElement(EntryRequest_14);
      EntryRequest_14.setValue("paul");//RoomControl.sysj line: 341, column: 6
      System.out.println("Emitted EntryRequest_14");
      S2509=0;
      active[16]=1;
      ends[16]=1;
      tdone[16]=1;
    }
  }

  public void thread26309(int [] tdone, int [] ends){
        S2753=1;
    s1_14.setClear();//RoomControl.sysj line: 332, column: 7
    s2_14.setClear();//RoomControl.sysj line: 332, column: 7
    thread26310(tdone,ends);
    thread26311(tdone,ends);
    int biggest26312 = 0;
    if(ends[16]>=biggest26312){
      biggest26312=ends[16];
    }
    if(ends[17]>=biggest26312){
      biggest26312=ends[17];
    }
    if(biggest26312 == 1){
      active[15]=1;
      ends[15]=1;
      tdone[15]=1;
    }
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S25457){
        case 0 : 
          S25457=0;
          break RUN;
        
        case 1 : 
          S25457=2;
          S25457=2;
          ExitRequest_14.setClear();//RoomControl.sysj line: 324, column: 3
          EntryRegistered_14.setClear();//RoomControl.sysj line: 324, column: 3
          ExitRegistered_14.setClear();//RoomControl.sysj line: 324, column: 3
          DoorClosed_14.setClear();//RoomControl.sysj line: 324, column: 3
          DoorOpened_14.setClear();//RoomControl.sysj line: 324, column: 3
          InvalidCard_14.setClear();//RoomControl.sysj line: 324, column: 3
          UnlockDoor_14.setClear();//RoomControl.sysj line: 324, column: 3
          LockDoor_14.setClear();//RoomControl.sysj line: 324, column: 3
          TimeOutAC_14.setClear();//RoomControl.sysj line: 325, column: 3
          TriggerAC_14.setClear();//RoomControl.sysj line: 325, column: 3
          EntryRequest_14.setClear();//RoomControl.sysj line: 326, column: 3
          thread26309(tdone,ends);
          thread26313(tdone,ends);
          int biggest26320 = 0;
          if(ends[15]>=biggest26320){
            biggest26320=ends[15];
          }
          if(ends[18]>=biggest26320){
            biggest26320=ends[18];
          }
          if(biggest26320 == 1){
            active[14]=1;
            ends[14]=1;
            break RUN;
          }
          if(biggest26320 == 1){
            active[14]=1;
            ends[14]=1;
            break RUN;
          }
        
        case 2 : 
          ExitRequest_14.setClear();//RoomControl.sysj line: 324, column: 3
          EntryRegistered_14.setClear();//RoomControl.sysj line: 324, column: 3
          ExitRegistered_14.setClear();//RoomControl.sysj line: 324, column: 3
          DoorClosed_14.setClear();//RoomControl.sysj line: 324, column: 3
          DoorOpened_14.setClear();//RoomControl.sysj line: 324, column: 3
          InvalidCard_14.setClear();//RoomControl.sysj line: 324, column: 3
          UnlockDoor_14.setClear();//RoomControl.sysj line: 324, column: 3
          LockDoor_14.setClear();//RoomControl.sysj line: 324, column: 3
          TimeOutAC_14.setClear();//RoomControl.sysj line: 325, column: 3
          TriggerAC_14.setClear();//RoomControl.sysj line: 325, column: 3
          EntryRequest_14.setClear();//RoomControl.sysj line: 326, column: 3
          thread26321(tdone,ends);
          thread26325(tdone,ends);
          int biggest26344 = 0;
          if(ends[15]>=biggest26344){
            biggest26344=ends[15];
          }
          if(ends[18]>=biggest26344){
            biggest26344=ends[18];
          }
          if(biggest26344 == 1){
            active[14]=1;
            ends[14]=1;
            break RUN;
          }
          //FINXME code
          if(biggest26344 == 0){
            S25457=0;
            active[14]=0;
            ends[14]=0;
            S25457=0;
            break RUN;
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    ExitRequest_14 = new Signal();
    EntryRegistered_14 = new Signal();
    ExitRegistered_14 = new Signal();
    DoorClosed_14 = new Signal();
    DoorOpened_14 = new Signal();
    InvalidCard_14 = new Signal();
    UnlockDoor_14 = new Signal();
    LockDoor_14 = new Signal();
    TimeOutAC_14 = new Signal();
    TriggerAC_14 = new Signal();
    EntryRequest_14 = new Signal();
    s1_14 = new Signal();
    s2_14 = new Signal();
    beep_18 = new Signal();
    RAC_18 = new Signal();
    Preempt_18 = new Signal();
    sInc_18 = new Signal();
    sDec_18 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[14] != 0){
      int index = 14;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[14]!=0 || suspended[14]!=0 || active[14]!=1);
      else{
        if(!df){
          ResetAC2_in.gethook();
          Emergency2_in.gethook();
          IncreaseCount2_o.gethook();
          DecreaseCount2_o.gethook();
          df = true;
        }
        runClockDomain();
      }
      ExitRequest_14.setpreclear();
      EntryRegistered_14.setpreclear();
      ExitRegistered_14.setpreclear();
      DoorClosed_14.setpreclear();
      DoorOpened_14.setpreclear();
      InvalidCard_14.setpreclear();
      UnlockDoor_14.setpreclear();
      LockDoor_14.setpreclear();
      TimeOutAC_14.setpreclear();
      TriggerAC_14.setpreclear();
      EntryRequest_14.setpreclear();
      s1_14.setpreclear();
      s2_14.setpreclear();
      beep_18.setpreclear();
      RAC_18.setpreclear();
      Preempt_18.setpreclear();
      sInc_18.setpreclear();
      sDec_18.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      ExitRequest_14.setClear();
      EntryRegistered_14.setClear();
      ExitRegistered_14.setClear();
      DoorClosed_14.setClear();
      DoorOpened_14.setClear();
      InvalidCard_14.setClear();
      UnlockDoor_14.setClear();
      LockDoor_14.setClear();
      TimeOutAC_14.setClear();
      TriggerAC_14.setClear();
      EntryRequest_14.setClear();
      s1_14.setClear();
      s2_14.setClear();
      beep_18.setClear();
      RAC_18.setClear();
      Preempt_18.setClear();
      sInc_18.setClear();
      sDec_18.setClear();
      ResetAC2_in.sethook();
      Emergency2_in.sethook();
      IncreaseCount2_o.sethook();
      DecreaseCount2_o.sethook();
      if(paused[14]!=0 || suspended[14]!=0 || active[14]!=1);
      else{
        ResetAC2_in.gethook();
        Emergency2_in.gethook();
        IncreaseCount2_o.gethook();
        DecreaseCount2_o.gethook();
      }
      if(active[14] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
