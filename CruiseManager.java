import java.util.*;
import systemj.bootstrap.ClockDomain;
import systemj.lib.*;

public class CruiseManager extends ClockDomain{
  public CruiseManager(){super(); init();}
  Vector currsigs = new Vector();
  private boolean df = false;
  private char [] active;
  private char [] paused;
  private char [] suspended;
  private Signal set_1;
  private Signal off_1;
  private Signal resume_1;
  private Signal quickAccel_1;
  private Signal quickDecel_1;
  private Signal brakePressed_1;
  private Signal clock_1;
  private Signal speed_1;
  private Signal zregulOff_1;
  private Signal regulStdby_1;
  private Signal regulSet_1;
  private Signal regulResume_1;
  private Signal speedSet_1;
  private Signal cruiseSpeed_1;
  private Signal testt1_1;
  private short t_thread_2;//CruiseManager.sysj line: 288, column: 2
  private int stateVar_thread_6;//CruiseManager.sysj line: 32, column: 7
  private short sysjtmpspeedInDataWITH1_thread_5;//CruiseManager.sysj line: 24, column: 2
  private short sysjtmpspeedtrans00_thread_6;//CruiseManager.sysj line: 42, column: 3
  private short sysjtmpspeedtrans11_thread_6;//CruiseManager.sysj line: 66, column: 7
  private short sysjtmpspeedtrans21_thread_6;//CruiseManager.sysj line: 109, column: 9
  private short sysjtmpspeedtrans31_thread_6;//CruiseManager.sysj line: 176, column: 4
  private short sysjtmpspeedtrans41_thread_6;//CruiseManager.sysj line: 219, column: 6
  private short sysjtmpcruiseSpeedInDataWITH1_thread_7;//CruiseManager.sysj line: 260, column: 6
  private int S634732 = 1;
  private int S634731 = 1;
  private int S16 = 1;
  private int S15 = 1;
  private int S18 = 1;
  private int S17 = 1;
  private int S634729 = 1;
  private int S41 = 1;
  private int S634705 = 1;
  private int S11794 = 1;
  private int S5917 = 1;
  private int S42 = 1;
  private int S62 = 1;
  private int S5916 = 1;
  private int S165 = 1;
  private int S209 = 1;
  private int S5915 = 1;
  private int S432 = 1;
  private int S1256 = 1;
  private int S440 = 1;
  private int S484 = 1;
  private int S5914 = 1;
  private int S5379 = 1;
  private int S5423 = 1;
  private int S5646 = 1;
  private int S5690 = 1;
  private int S634727 = 1;
  
  private int[] ends = new int[8];
  private int[] tdone = new int[8];
  
  public void thread634777(int [] tdone, int [] ends){
        switch(S634727){
      case 0 : 
        active[7]=0;
        ends[7]=0;
        tdone[7]=1;
        break;
      
      case 1 : 
        if(zregulOff_1.getprestatus() || regulStdby_1.getprestatus() || regulSet_1.getprestatus() || regulResume_1.getprestatus() || speedSet_1.getprestatus()){//CruiseManager.sysj line: 259, column: 4
          sysjtmpcruiseSpeedInDataWITH1_thread_7 = 0;//CruiseManager.sysj line: 260, column: 6
          sysjtmpcruiseSpeedInDataWITH1_thread_7 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 261, column: 6
          cruiseSpeed_1.setPresent();//CruiseManager.sysj line: 262, column: 6
          currsigs.addElement(cruiseSpeed_1);
          cruiseSpeed_1.setValue(new Integer(sysjtmpcruiseSpeedInDataWITH1_thread_7));//CruiseManager.sysj line: 262, column: 6
          System.out.println("Emitted cruiseSpeed_1");
          active[7]=1;
          ends[7]=1;
          tdone[7]=1;
        }
        else {
          active[7]=1;
          ends[7]=1;
          tdone[7]=1;
        }
        break;
      
    }
  }

  public void thread634776(int [] tdone, int [] ends){
        switch(S634705){
      case 0 : 
        active[6]=0;
        ends[6]=0;
        tdone[6]=1;
        break;
      
      case 1 : 
        switch(S11794){
          case 0 : 
            S11794=0;
            S11794=1;
            if(stateVar_thread_6 == 0){//CruiseManager.sysj line: 35, column: 5
              S5917=0;
              zregulOff_1.setPresent();//CruiseManager.sysj line: 36, column: 4
              currsigs.addElement(zregulOff_1);
              System.out.println("Emitted zregulOff_1");
              S42=0;
              active[6]=1;
              ends[6]=1;
              tdone[6]=1;
            }
            else {
              S5917=1;
              if(stateVar_thread_6 == 1){//CruiseManager.sysj line: 54, column: 7
                S5916=0;
                regulSet_1.setPresent();//CruiseManager.sysj line: 55, column: 6
                currsigs.addElement(regulSet_1);
                System.out.println("Emitted regulSet_1");
                S165=0;
                active[6]=1;
                ends[6]=1;
                tdone[6]=1;
              }
              else {
                S5916=1;
                if(stateVar_thread_6 == 2){//CruiseManager.sysj line: 97, column: 9
                  S5915=0;
                  regulStdby_1.setPresent();//CruiseManager.sysj line: 98, column: 8
                  currsigs.addElement(regulStdby_1);
                  System.out.println("Emitted regulStdby_1");
                  S432=0;
                  active[6]=1;
                  ends[6]=1;
                  tdone[6]=1;
                }
                else {
                  S5915=1;
                  if(stateVar_thread_6 == 3){//CruiseManager.sysj line: 164, column: 11
                    S5914=0;
                    speedSet_1.setPresent();//CruiseManager.sysj line: 165, column: 3
                    currsigs.addElement(speedSet_1);
                    System.out.println("Emitted speedSet_1");
                    S5379=0;
                    active[6]=1;
                    ends[6]=1;
                    tdone[6]=1;
                  }
                  else {
                    S5914=1;
                    if(stateVar_thread_6 == 4){//CruiseManager.sysj line: 207, column: 6
                      speedSet_1.setPresent();//CruiseManager.sysj line: 208, column: 5
                      currsigs.addElement(speedSet_1);
                      System.out.println("Emitted speedSet_1");
                      S5646=0;
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                    else {
                      S11794=0;
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                  }
                }
              }
            }
            break;
          
          case 1 : 
            switch(S5917){
              case 0 : 
                switch(S42){
                  case 0 : 
                    S42=0;
                    S42=1;
                    S62=0;
                    if(off_1.getprestatus()){//CruiseManager.sysj line: 40, column: 24
                      if(off_1.getprestatus()){//CruiseManager.sysj line: 41, column: 16
                        sysjtmpspeedtrans00_thread_6 = 0;//CruiseManager.sysj line: 42, column: 3
                        sysjtmpspeedtrans00_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 43, column: 3
                        if(sysjtmpspeedtrans00_thread_6 > 50 && sysjtmpspeedtrans00_thread_6 < 170){//CruiseManager.sysj line: 44, column: 6
                          stateVar_thread_6 = 1;//CruiseManager.sysj line: 45, column: 5
                          ends[6]=2;
                          ;//CruiseManager.sysj line: 38, column: 4
                          S11794=0;
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                        else {
                          S62=1;
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                      }
                      else {
                        S62=1;
                        active[6]=1;
                        ends[6]=1;
                        tdone[6]=1;
                      }
                    }
                    else {
                      active[6]=1;
                      ends[6]=1;
                      tdone[6]=1;
                    }
                    break;
                  
                  case 1 : 
                    switch(S62){
                      case 0 : 
                        if(off_1.getprestatus()){//CruiseManager.sysj line: 40, column: 24
                          if(off_1.getprestatus()){//CruiseManager.sysj line: 41, column: 16
                            sysjtmpspeedtrans00_thread_6 = 0;//CruiseManager.sysj line: 42, column: 3
                            sysjtmpspeedtrans00_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 43, column: 3
                            if(sysjtmpspeedtrans00_thread_6 > 50 && sysjtmpspeedtrans00_thread_6 < 170){//CruiseManager.sysj line: 44, column: 6
                              stateVar_thread_6 = 1;//CruiseManager.sysj line: 45, column: 5
                              ends[6]=2;
                              ;//CruiseManager.sysj line: 38, column: 4
                              S11794=0;
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                            else {
                              S62=1;
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                          }
                          else {
                            S62=1;
                            active[6]=1;
                            ends[6]=1;
                            tdone[6]=1;
                          }
                        }
                        else {
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                        break;
                      
                      case 1 : 
                        S62=1;
                        S62=0;
                        if(off_1.getprestatus()){//CruiseManager.sysj line: 40, column: 24
                          if(off_1.getprestatus()){//CruiseManager.sysj line: 41, column: 16
                            sysjtmpspeedtrans00_thread_6 = 0;//CruiseManager.sysj line: 42, column: 3
                            sysjtmpspeedtrans00_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 43, column: 3
                            if(sysjtmpspeedtrans00_thread_6 > 50 && sysjtmpspeedtrans00_thread_6 < 170){//CruiseManager.sysj line: 44, column: 6
                              stateVar_thread_6 = 1;//CruiseManager.sysj line: 45, column: 5
                              ends[6]=2;
                              ;//CruiseManager.sysj line: 38, column: 4
                              S11794=0;
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                            else {
                              S62=1;
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                          }
                          else {
                            S62=1;
                            active[6]=1;
                            ends[6]=1;
                            tdone[6]=1;
                          }
                        }
                        else {
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                        break;
                      
                    }
                    break;
                  
                }
                break;
              
              case 1 : 
                switch(S5916){
                  case 0 : 
                    switch(S165){
                      case 0 : 
                        S165=0;
                        S165=1;
                        S209=0;
                        if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 59, column: 3
                          if(set_1.getprestatus()){//CruiseManager.sysj line: 60, column: 11
                            stateVar_thread_6 = 0;//CruiseManager.sysj line: 61, column: 5
                            ends[6]=2;
                            ;//CruiseManager.sysj line: 57, column: 6
                            S11794=0;
                            active[6]=1;
                            ends[6]=1;
                            tdone[6]=1;
                          }
                          else {
                            if(off_1.getprestatus()){//CruiseManager.sysj line: 65, column: 13
                              sysjtmpspeedtrans11_thread_6 = 0;//CruiseManager.sysj line: 66, column: 7
                              sysjtmpspeedtrans11_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 67, column: 7
                              if(sysjtmpspeedtrans11_thread_6 > 50 && sysjtmpspeedtrans11_thread_6 < 170){//CruiseManager.sysj line: 68, column: 10
                                stateVar_thread_6 = 1;//CruiseManager.sysj line: 69, column: 9
                                ends[6]=2;
                                ;//CruiseManager.sysj line: 57, column: 6
                                S11794=0;
                                active[6]=1;
                                ends[6]=1;
                                tdone[6]=1;
                              }
                              else {
                                S209=1;
                                active[6]=1;
                                ends[6]=1;
                                tdone[6]=1;
                              }
                            }
                            else {
                              if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 74, column: 15
                                stateVar_thread_6 = 2;//CruiseManager.sysj line: 75, column: 9
                                ends[6]=2;
                                ;//CruiseManager.sysj line: 57, column: 6
                                S11794=0;
                                active[6]=1;
                                ends[6]=1;
                                tdone[6]=1;
                              }
                              else {
                                if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 79, column: 17
                                  stateVar_thread_6 = 3;//CruiseManager.sysj line: 80, column: 4
                                  ends[6]=2;
                                  ;//CruiseManager.sysj line: 57, column: 6
                                  S11794=0;
                                  active[6]=1;
                                  ends[6]=1;
                                  tdone[6]=1;
                                }
                                else {
                                  if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 84, column: 12
                                    stateVar_thread_6 = 4;//CruiseManager.sysj line: 85, column: 6
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 57, column: 6
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    S209=1;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                }
                              }
                            }
                          }
                        }
                        else {
                          active[6]=1;
                          ends[6]=1;
                          tdone[6]=1;
                        }
                        break;
                      
                      case 1 : 
                        switch(S209){
                          case 0 : 
                            if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 59, column: 3
                              if(set_1.getprestatus()){//CruiseManager.sysj line: 60, column: 11
                                stateVar_thread_6 = 0;//CruiseManager.sysj line: 61, column: 5
                                ends[6]=2;
                                ;//CruiseManager.sysj line: 57, column: 6
                                S11794=0;
                                active[6]=1;
                                ends[6]=1;
                                tdone[6]=1;
                              }
                              else {
                                if(off_1.getprestatus()){//CruiseManager.sysj line: 65, column: 13
                                  sysjtmpspeedtrans11_thread_6 = 0;//CruiseManager.sysj line: 66, column: 7
                                  sysjtmpspeedtrans11_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 67, column: 7
                                  if(sysjtmpspeedtrans11_thread_6 > 50 && sysjtmpspeedtrans11_thread_6 < 170){//CruiseManager.sysj line: 68, column: 10
                                    stateVar_thread_6 = 1;//CruiseManager.sysj line: 69, column: 9
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 57, column: 6
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    S209=1;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                }
                                else {
                                  if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 74, column: 15
                                    stateVar_thread_6 = 2;//CruiseManager.sysj line: 75, column: 9
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 57, column: 6
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 79, column: 17
                                      stateVar_thread_6 = 3;//CruiseManager.sysj line: 80, column: 4
                                      ends[6]=2;
                                      ;//CruiseManager.sysj line: 57, column: 6
                                      S11794=0;
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    else {
                                      if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 84, column: 12
                                        stateVar_thread_6 = 4;//CruiseManager.sysj line: 85, column: 6
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 57, column: 6
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S209=1;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            else {
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                            break;
                          
                          case 1 : 
                            S209=1;
                            S209=0;
                            if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 59, column: 3
                              if(set_1.getprestatus()){//CruiseManager.sysj line: 60, column: 11
                                stateVar_thread_6 = 0;//CruiseManager.sysj line: 61, column: 5
                                ends[6]=2;
                                ;//CruiseManager.sysj line: 57, column: 6
                                S11794=0;
                                active[6]=1;
                                ends[6]=1;
                                tdone[6]=1;
                              }
                              else {
                                if(off_1.getprestatus()){//CruiseManager.sysj line: 65, column: 13
                                  sysjtmpspeedtrans11_thread_6 = 0;//CruiseManager.sysj line: 66, column: 7
                                  sysjtmpspeedtrans11_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 67, column: 7
                                  if(sysjtmpspeedtrans11_thread_6 > 50 && sysjtmpspeedtrans11_thread_6 < 170){//CruiseManager.sysj line: 68, column: 10
                                    stateVar_thread_6 = 1;//CruiseManager.sysj line: 69, column: 9
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 57, column: 6
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    S209=1;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                }
                                else {
                                  if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 74, column: 15
                                    stateVar_thread_6 = 2;//CruiseManager.sysj line: 75, column: 9
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 57, column: 6
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 79, column: 17
                                      stateVar_thread_6 = 3;//CruiseManager.sysj line: 80, column: 4
                                      ends[6]=2;
                                      ;//CruiseManager.sysj line: 57, column: 6
                                      S11794=0;
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    else {
                                      if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 84, column: 12
                                        stateVar_thread_6 = 4;//CruiseManager.sysj line: 85, column: 6
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 57, column: 6
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S209=1;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            else {
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                            break;
                          
                        }
                        break;
                      
                    }
                    break;
                  
                  case 1 : 
                    switch(S5915){
                      case 0 : 
                        switch(S432){
                          case 0 : 
                            S432=0;
                            S432=1;
                            S1256=0;
                            if((set_1.getprestatus() || off_1.getprestatus() || resume_1.getprestatus())){//CruiseManager.sysj line: 102, column: 5
                              S1256=1;
                              if(set_1.getprestatus()){//CruiseManager.sysj line: 103, column: 13
                                stateVar_thread_6 = 0;//CruiseManager.sysj line: 104, column: 7
                                ends[6]=2;
                                ;//CruiseManager.sysj line: 100, column: 8
                                S11794=0;
                                active[6]=1;
                                ends[6]=1;
                                tdone[6]=1;
                              }
                              else {
                                if(off_1.getprestatus()){//CruiseManager.sysj line: 108, column: 15
                                  sysjtmpspeedtrans21_thread_6 = 0;//CruiseManager.sysj line: 109, column: 9
                                  sysjtmpspeedtrans21_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 110, column: 9
                                  if(sysjtmpspeedtrans21_thread_6 > 50 && sysjtmpspeedtrans21_thread_6 < 170){//CruiseManager.sysj line: 111, column: 12
                                    stateVar_thread_6 = 1;//CruiseManager.sysj line: 112, column: 4
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 100, column: 8
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    S1256=2;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                }
                                else {
                                  if(resume_1.getprestatus()){//CruiseManager.sysj line: 117, column: 17
                                    regulResume_1.setPresent();//CruiseManager.sysj line: 118, column: 4
                                    currsigs.addElement(regulResume_1);
                                    System.out.println("Emitted regulResume_1");
                                    S440=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    S1256=2;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                }
                              }
                            }
                            else {
                              active[6]=1;
                              ends[6]=1;
                              tdone[6]=1;
                            }
                            break;
                          
                          case 1 : 
                            switch(S1256){
                              case 0 : 
                                if((set_1.getprestatus() || off_1.getprestatus() || resume_1.getprestatus())){//CruiseManager.sysj line: 102, column: 5
                                  S1256=1;
                                  if(set_1.getprestatus()){//CruiseManager.sysj line: 103, column: 13
                                    stateVar_thread_6 = 0;//CruiseManager.sysj line: 104, column: 7
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 100, column: 8
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    if(off_1.getprestatus()){//CruiseManager.sysj line: 108, column: 15
                                      sysjtmpspeedtrans21_thread_6 = 0;//CruiseManager.sysj line: 109, column: 9
                                      sysjtmpspeedtrans21_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 110, column: 9
                                      if(sysjtmpspeedtrans21_thread_6 > 50 && sysjtmpspeedtrans21_thread_6 < 170){//CruiseManager.sysj line: 111, column: 12
                                        stateVar_thread_6 = 1;//CruiseManager.sysj line: 112, column: 4
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 100, column: 8
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S1256=2;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                    else {
                                      if(resume_1.getprestatus()){//CruiseManager.sysj line: 117, column: 17
                                        regulResume_1.setPresent();//CruiseManager.sysj line: 118, column: 4
                                        currsigs.addElement(regulResume_1);
                                        System.out.println("Emitted regulResume_1");
                                        S440=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S1256=2;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                  }
                                }
                                else {
                                  active[6]=1;
                                  ends[6]=1;
                                  tdone[6]=1;
                                }
                                break;
                              
                              case 1 : 
                                switch(S440){
                                  case 0 : 
                                    S440=0;
                                    S440=1;
                                    S484=0;
                                    if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 121, column: 6
                                      if(set_1.getprestatus()){//CruiseManager.sysj line: 122, column: 14
                                        stateVar_thread_6 = 0;//CruiseManager.sysj line: 123, column: 8
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 100, column: 8
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(off_1.getprestatus()){//CruiseManager.sysj line: 127, column: 16
                                          sysjtmpspeedtrans11_thread_6 = 0;//CruiseManager.sysj line: 128, column: 10
                                          sysjtmpspeedtrans11_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 129, column: 10
                                          if(sysjtmpspeedtrans11_thread_6 > 50 && sysjtmpspeedtrans11_thread_6 < 170){//CruiseManager.sysj line: 130, column: 13
                                            stateVar_thread_6 = 1;//CruiseManager.sysj line: 131, column: 5
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 100, column: 8
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S484=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                        else {
                                          if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 136, column: 18
                                            stateVar_thread_6 = 2;//CruiseManager.sysj line: 137, column: 5
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 100, column: 8
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 141, column: 13
                                              stateVar_thread_6 = 3;//CruiseManager.sysj line: 142, column: 7
                                              ends[6]=2;
                                              ;//CruiseManager.sysj line: 100, column: 8
                                              S11794=0;
                                              active[6]=1;
                                              ends[6]=1;
                                              tdone[6]=1;
                                            }
                                            else {
                                              if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 146, column: 15
                                                stateVar_thread_6 = 4;//CruiseManager.sysj line: 147, column: 9
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 100, column: 8
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S484=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                    else {
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    break;
                                  
                                  case 1 : 
                                    switch(S484){
                                      case 0 : 
                                        if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 121, column: 6
                                          if(set_1.getprestatus()){//CruiseManager.sysj line: 122, column: 14
                                            stateVar_thread_6 = 0;//CruiseManager.sysj line: 123, column: 8
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 100, column: 8
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(off_1.getprestatus()){//CruiseManager.sysj line: 127, column: 16
                                              sysjtmpspeedtrans11_thread_6 = 0;//CruiseManager.sysj line: 128, column: 10
                                              sysjtmpspeedtrans11_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 129, column: 10
                                              if(sysjtmpspeedtrans11_thread_6 > 50 && sysjtmpspeedtrans11_thread_6 < 170){//CruiseManager.sysj line: 130, column: 13
                                                stateVar_thread_6 = 1;//CruiseManager.sysj line: 131, column: 5
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 100, column: 8
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S484=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                            else {
                                              if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 136, column: 18
                                                stateVar_thread_6 = 2;//CruiseManager.sysj line: 137, column: 5
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 100, column: 8
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 141, column: 13
                                                  stateVar_thread_6 = 3;//CruiseManager.sysj line: 142, column: 7
                                                  ends[6]=2;
                                                  ;//CruiseManager.sysj line: 100, column: 8
                                                  S11794=0;
                                                  active[6]=1;
                                                  ends[6]=1;
                                                  tdone[6]=1;
                                                }
                                                else {
                                                  if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 146, column: 15
                                                    stateVar_thread_6 = 4;//CruiseManager.sysj line: 147, column: 9
                                                    ends[6]=2;
                                                    ;//CruiseManager.sysj line: 100, column: 8
                                                    S11794=0;
                                                    active[6]=1;
                                                    ends[6]=1;
                                                    tdone[6]=1;
                                                  }
                                                  else {
                                                    S484=1;
                                                    active[6]=1;
                                                    ends[6]=1;
                                                    tdone[6]=1;
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                        else {
                                          active[6]=1;
                                          ends[6]=1;
                                          tdone[6]=1;
                                        }
                                        break;
                                      
                                      case 1 : 
                                        S484=1;
                                        S484=0;
                                        if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 121, column: 6
                                          if(set_1.getprestatus()){//CruiseManager.sysj line: 122, column: 14
                                            stateVar_thread_6 = 0;//CruiseManager.sysj line: 123, column: 8
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 100, column: 8
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(off_1.getprestatus()){//CruiseManager.sysj line: 127, column: 16
                                              sysjtmpspeedtrans11_thread_6 = 0;//CruiseManager.sysj line: 128, column: 10
                                              sysjtmpspeedtrans11_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 129, column: 10
                                              if(sysjtmpspeedtrans11_thread_6 > 50 && sysjtmpspeedtrans11_thread_6 < 170){//CruiseManager.sysj line: 130, column: 13
                                                stateVar_thread_6 = 1;//CruiseManager.sysj line: 131, column: 5
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 100, column: 8
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S484=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                            else {
                                              if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 136, column: 18
                                                stateVar_thread_6 = 2;//CruiseManager.sysj line: 137, column: 5
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 100, column: 8
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 141, column: 13
                                                  stateVar_thread_6 = 3;//CruiseManager.sysj line: 142, column: 7
                                                  ends[6]=2;
                                                  ;//CruiseManager.sysj line: 100, column: 8
                                                  S11794=0;
                                                  active[6]=1;
                                                  ends[6]=1;
                                                  tdone[6]=1;
                                                }
                                                else {
                                                  if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 146, column: 15
                                                    stateVar_thread_6 = 4;//CruiseManager.sysj line: 147, column: 9
                                                    ends[6]=2;
                                                    ;//CruiseManager.sysj line: 100, column: 8
                                                    S11794=0;
                                                    active[6]=1;
                                                    ends[6]=1;
                                                    tdone[6]=1;
                                                  }
                                                  else {
                                                    S484=1;
                                                    active[6]=1;
                                                    ends[6]=1;
                                                    tdone[6]=1;
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                        else {
                                          active[6]=1;
                                          ends[6]=1;
                                          tdone[6]=1;
                                        }
                                        break;
                                      
                                    }
                                    break;
                                  
                                }
                                break;
                              
                              case 2 : 
                                S1256=2;
                                S1256=0;
                                if((set_1.getprestatus() || off_1.getprestatus() || resume_1.getprestatus())){//CruiseManager.sysj line: 102, column: 5
                                  S1256=1;
                                  if(set_1.getprestatus()){//CruiseManager.sysj line: 103, column: 13
                                    stateVar_thread_6 = 0;//CruiseManager.sysj line: 104, column: 7
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 100, column: 8
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    if(off_1.getprestatus()){//CruiseManager.sysj line: 108, column: 15
                                      sysjtmpspeedtrans21_thread_6 = 0;//CruiseManager.sysj line: 109, column: 9
                                      sysjtmpspeedtrans21_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 110, column: 9
                                      if(sysjtmpspeedtrans21_thread_6 > 50 && sysjtmpspeedtrans21_thread_6 < 170){//CruiseManager.sysj line: 111, column: 12
                                        stateVar_thread_6 = 1;//CruiseManager.sysj line: 112, column: 4
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 100, column: 8
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S1256=2;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                    else {
                                      if(resume_1.getprestatus()){//CruiseManager.sysj line: 117, column: 17
                                        regulResume_1.setPresent();//CruiseManager.sysj line: 118, column: 4
                                        currsigs.addElement(regulResume_1);
                                        System.out.println("Emitted regulResume_1");
                                        S440=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S1256=2;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                  }
                                }
                                else {
                                  active[6]=1;
                                  ends[6]=1;
                                  tdone[6]=1;
                                }
                                break;
                              
                            }
                            break;
                          
                        }
                        break;
                      
                      case 1 : 
                        switch(S5914){
                          case 0 : 
                            switch(S5379){
                              case 0 : 
                                S5379=0;
                                S5379=1;
                                S5423=0;
                                if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 169, column: 7
                                  if(set_1.getprestatus()){//CruiseManager.sysj line: 170, column: 15
                                    stateVar_thread_6 = 0;//CruiseManager.sysj line: 171, column: 9
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 167, column: 3
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    if(off_1.getprestatus()){//CruiseManager.sysj line: 175, column: 17
                                      sysjtmpspeedtrans31_thread_6 = 0;//CruiseManager.sysj line: 176, column: 4
                                      sysjtmpspeedtrans31_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 177, column: 4
                                      if(sysjtmpspeedtrans31_thread_6 > 50 && sysjtmpspeedtrans31_thread_6 < 170){//CruiseManager.sysj line: 178, column: 7
                                        stateVar_thread_6 = 1;//CruiseManager.sysj line: 179, column: 6
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 167, column: 3
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S5423=1;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                    else {
                                      if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 184, column: 12
                                        stateVar_thread_6 = 2;//CruiseManager.sysj line: 185, column: 6
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 167, column: 3
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 189, column: 14
                                          stateVar_thread_6 = 3;//CruiseManager.sysj line: 190, column: 8
                                          ends[6]=2;
                                          ;//CruiseManager.sysj line: 167, column: 3
                                          S11794=0;
                                          active[6]=1;
                                          ends[6]=1;
                                          tdone[6]=1;
                                        }
                                        else {
                                          if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 194, column: 16
                                            stateVar_thread_6 = 4;//CruiseManager.sysj line: 195, column: 10
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 167, column: 3
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S5423=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                                else {
                                  active[6]=1;
                                  ends[6]=1;
                                  tdone[6]=1;
                                }
                                break;
                              
                              case 1 : 
                                switch(S5423){
                                  case 0 : 
                                    if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 169, column: 7
                                      if(set_1.getprestatus()){//CruiseManager.sysj line: 170, column: 15
                                        stateVar_thread_6 = 0;//CruiseManager.sysj line: 171, column: 9
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 167, column: 3
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(off_1.getprestatus()){//CruiseManager.sysj line: 175, column: 17
                                          sysjtmpspeedtrans31_thread_6 = 0;//CruiseManager.sysj line: 176, column: 4
                                          sysjtmpspeedtrans31_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 177, column: 4
                                          if(sysjtmpspeedtrans31_thread_6 > 50 && sysjtmpspeedtrans31_thread_6 < 170){//CruiseManager.sysj line: 178, column: 7
                                            stateVar_thread_6 = 1;//CruiseManager.sysj line: 179, column: 6
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 167, column: 3
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S5423=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                        else {
                                          if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 184, column: 12
                                            stateVar_thread_6 = 2;//CruiseManager.sysj line: 185, column: 6
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 167, column: 3
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 189, column: 14
                                              stateVar_thread_6 = 3;//CruiseManager.sysj line: 190, column: 8
                                              ends[6]=2;
                                              ;//CruiseManager.sysj line: 167, column: 3
                                              S11794=0;
                                              active[6]=1;
                                              ends[6]=1;
                                              tdone[6]=1;
                                            }
                                            else {
                                              if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 194, column: 16
                                                stateVar_thread_6 = 4;//CruiseManager.sysj line: 195, column: 10
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 167, column: 3
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S5423=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                    else {
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    break;
                                  
                                  case 1 : 
                                    S5423=1;
                                    S5423=0;
                                    if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 169, column: 7
                                      if(set_1.getprestatus()){//CruiseManager.sysj line: 170, column: 15
                                        stateVar_thread_6 = 0;//CruiseManager.sysj line: 171, column: 9
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 167, column: 3
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(off_1.getprestatus()){//CruiseManager.sysj line: 175, column: 17
                                          sysjtmpspeedtrans31_thread_6 = 0;//CruiseManager.sysj line: 176, column: 4
                                          sysjtmpspeedtrans31_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 177, column: 4
                                          if(sysjtmpspeedtrans31_thread_6 > 50 && sysjtmpspeedtrans31_thread_6 < 170){//CruiseManager.sysj line: 178, column: 7
                                            stateVar_thread_6 = 1;//CruiseManager.sysj line: 179, column: 6
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 167, column: 3
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S5423=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                        else {
                                          if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 184, column: 12
                                            stateVar_thread_6 = 2;//CruiseManager.sysj line: 185, column: 6
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 167, column: 3
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 189, column: 14
                                              stateVar_thread_6 = 3;//CruiseManager.sysj line: 190, column: 8
                                              ends[6]=2;
                                              ;//CruiseManager.sysj line: 167, column: 3
                                              S11794=0;
                                              active[6]=1;
                                              ends[6]=1;
                                              tdone[6]=1;
                                            }
                                            else {
                                              if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 194, column: 16
                                                stateVar_thread_6 = 4;//CruiseManager.sysj line: 195, column: 10
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 167, column: 3
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S5423=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                    else {
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    break;
                                  
                                }
                                break;
                              
                            }
                            break;
                          
                          case 1 : 
                            switch(S5646){
                              case 0 : 
                                S5646=0;
                                S5646=1;
                                S5690=0;
                                if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 212, column: 9
                                  if(set_1.getprestatus()){//CruiseManager.sysj line: 213, column: 17
                                    stateVar_thread_6 = 0;//CruiseManager.sysj line: 214, column: 4
                                    ends[6]=2;
                                    ;//CruiseManager.sysj line: 210, column: 5
                                    S11794=0;
                                    active[6]=1;
                                    ends[6]=1;
                                    tdone[6]=1;
                                  }
                                  else {
                                    if(off_1.getprestatus()){//CruiseManager.sysj line: 218, column: 12
                                      sysjtmpspeedtrans41_thread_6 = 0;//CruiseManager.sysj line: 219, column: 6
                                      sysjtmpspeedtrans41_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 220, column: 6
                                      if(sysjtmpspeedtrans41_thread_6 > 50 && sysjtmpspeedtrans41_thread_6 < 170){//CruiseManager.sysj line: 221, column: 9
                                        stateVar_thread_6 = 1;//CruiseManager.sysj line: 222, column: 8
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 210, column: 5
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        S5690=1;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                    }
                                    else {
                                      if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 227, column: 14
                                        stateVar_thread_6 = 2;//CruiseManager.sysj line: 228, column: 8
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 210, column: 5
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 232, column: 16
                                          stateVar_thread_6 = 3;//CruiseManager.sysj line: 233, column: 10
                                          ends[6]=2;
                                          ;//CruiseManager.sysj line: 210, column: 5
                                          S11794=0;
                                          active[6]=1;
                                          ends[6]=1;
                                          tdone[6]=1;
                                        }
                                        else {
                                          if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 237, column: 18
                                            stateVar_thread_6 = 4;//CruiseManager.sysj line: 238, column: 5
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 210, column: 5
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S5690=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                                else {
                                  active[6]=1;
                                  ends[6]=1;
                                  tdone[6]=1;
                                }
                                break;
                              
                              case 1 : 
                                switch(S5690){
                                  case 0 : 
                                    if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 212, column: 9
                                      if(set_1.getprestatus()){//CruiseManager.sysj line: 213, column: 17
                                        stateVar_thread_6 = 0;//CruiseManager.sysj line: 214, column: 4
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 210, column: 5
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(off_1.getprestatus()){//CruiseManager.sysj line: 218, column: 12
                                          sysjtmpspeedtrans41_thread_6 = 0;//CruiseManager.sysj line: 219, column: 6
                                          sysjtmpspeedtrans41_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 220, column: 6
                                          if(sysjtmpspeedtrans41_thread_6 > 50 && sysjtmpspeedtrans41_thread_6 < 170){//CruiseManager.sysj line: 221, column: 9
                                            stateVar_thread_6 = 1;//CruiseManager.sysj line: 222, column: 8
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 210, column: 5
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S5690=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                        else {
                                          if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 227, column: 14
                                            stateVar_thread_6 = 2;//CruiseManager.sysj line: 228, column: 8
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 210, column: 5
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 232, column: 16
                                              stateVar_thread_6 = 3;//CruiseManager.sysj line: 233, column: 10
                                              ends[6]=2;
                                              ;//CruiseManager.sysj line: 210, column: 5
                                              S11794=0;
                                              active[6]=1;
                                              ends[6]=1;
                                              tdone[6]=1;
                                            }
                                            else {
                                              if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 237, column: 18
                                                stateVar_thread_6 = 4;//CruiseManager.sysj line: 238, column: 5
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 210, column: 5
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S5690=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                    else {
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    break;
                                  
                                  case 1 : 
                                    S5690=1;
                                    S5690=0;
                                    if((set_1.getprestatus() || off_1.getprestatus() || brakePressed_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus())){//CruiseManager.sysj line: 212, column: 9
                                      if(set_1.getprestatus()){//CruiseManager.sysj line: 213, column: 17
                                        stateVar_thread_6 = 0;//CruiseManager.sysj line: 214, column: 4
                                        ends[6]=2;
                                        ;//CruiseManager.sysj line: 210, column: 5
                                        S11794=0;
                                        active[6]=1;
                                        ends[6]=1;
                                        tdone[6]=1;
                                      }
                                      else {
                                        if(off_1.getprestatus()){//CruiseManager.sysj line: 218, column: 12
                                          sysjtmpspeedtrans41_thread_6 = 0;//CruiseManager.sysj line: 219, column: 6
                                          sysjtmpspeedtrans41_thread_6 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 220, column: 6
                                          if(sysjtmpspeedtrans41_thread_6 > 50 && sysjtmpspeedtrans41_thread_6 < 170){//CruiseManager.sysj line: 221, column: 9
                                            stateVar_thread_6 = 1;//CruiseManager.sysj line: 222, column: 8
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 210, column: 5
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            S5690=1;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                        }
                                        else {
                                          if(brakePressed_1.getprestatus()){//CruiseManager.sysj line: 227, column: 14
                                            stateVar_thread_6 = 2;//CruiseManager.sysj line: 228, column: 8
                                            ends[6]=2;
                                            ;//CruiseManager.sysj line: 210, column: 5
                                            S11794=0;
                                            active[6]=1;
                                            ends[6]=1;
                                            tdone[6]=1;
                                          }
                                          else {
                                            if(quickAccel_1.getprestatus()){//CruiseManager.sysj line: 232, column: 16
                                              stateVar_thread_6 = 3;//CruiseManager.sysj line: 233, column: 10
                                              ends[6]=2;
                                              ;//CruiseManager.sysj line: 210, column: 5
                                              S11794=0;
                                              active[6]=1;
                                              ends[6]=1;
                                              tdone[6]=1;
                                            }
                                            else {
                                              if(quickDecel_1.getprestatus()){//CruiseManager.sysj line: 237, column: 18
                                                stateVar_thread_6 = 4;//CruiseManager.sysj line: 238, column: 5
                                                ends[6]=2;
                                                ;//CruiseManager.sysj line: 210, column: 5
                                                S11794=0;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                              else {
                                                S5690=1;
                                                active[6]=1;
                                                ends[6]=1;
                                                tdone[6]=1;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                    else {
                                      active[6]=1;
                                      ends[6]=1;
                                      tdone[6]=1;
                                    }
                                    break;
                                  
                                }
                                break;
                              
                            }
                            break;
                          
                        }
                        break;
                      
                    }
                    break;
                  
                }
                break;
              
            }
            break;
          
        }
        break;
      
    }
  }

  public void thread634760(int [] tdone, int [] ends){
        switch(S41){
      case 0 : 
        active[5]=0;
        ends[5]=0;
        tdone[5]=1;
        break;
      
      case 1 : 
        if(off_1.getprestatus() || set_1.getprestatus() || resume_1.getprestatus() || quickAccel_1.getprestatus() || quickDecel_1.getprestatus() || brakePressed_1.getprestatus() || clock_1.getprestatus()){//CruiseManager.sysj line: 23, column: 7
          sysjtmpspeedInDataWITH1_thread_5 = 0;//CruiseManager.sysj line: 24, column: 2
          sysjtmpspeedInDataWITH1_thread_5 = ((Short)speed_1.getpreval()).shortValue();//CruiseManager.sysj line: 25, column: 2
          speed_1.setPresent();//CruiseManager.sysj line: 26, column: 2
          currsigs.addElement(speed_1);
          speed_1.setValue(new Short(sysjtmpspeedInDataWITH1_thread_5));//CruiseManager.sysj line: 26, column: 2
          System.out.println("Emitted speed_1");
          active[5]=1;
          ends[5]=1;
          tdone[5]=1;
        }
        else {
          active[5]=1;
          ends[5]=1;
          tdone[5]=1;
        }
        break;
      
    }
  }

  public void thread634759(int [] tdone, int [] ends){
        switch(S634729){
      case 0 : 
        active[4]=0;
        ends[4]=0;
        tdone[4]=1;
        break;
      
      case 1 : 
        thread634760(tdone,ends);
        thread634776(tdone,ends);
        thread634777(tdone,ends);
        int biggest634793 = 0;
        if(ends[5]>=biggest634793){
          biggest634793=ends[5];
        }
        if(ends[6]>=biggest634793){
          biggest634793=ends[6];
        }
        if(ends[7]>=biggest634793){
          biggest634793=ends[7];
        }
        if(biggest634793 == 1){
          active[4]=1;
          ends[4]=1;
          tdone[4]=1;
        }
        //FINXME code
        if(biggest634793 == 0){
          S634729=0;
          active[4]=0;
          ends[4]=0;
          tdone[4]=1;
        }
        break;
      
    }
  }

  public void thread634758(int [] tdone, int [] ends){
        switch(S18){
      case 0 : 
        active[3]=0;
        ends[3]=0;
        tdone[3]=1;
        break;
      
      case 1 : 
        switch(S17){
          case 0 : 
            S17=0;
            System.out.println("tick 1-B");//CruiseManager.sysj line: 310, column: 6
            S17=1;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
            break;
          
          case 1 : 
            S17=1;
            System.out.println("tick 2-B");//CruiseManager.sysj line: 312, column: 6
            S17=2;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
            break;
          
          case 2 : 
            S17=2;
            System.out.println("tick 3-B");//CruiseManager.sysj line: 314, column: 6
            S17=3;
            active[3]=1;
            ends[3]=1;
            tdone[3]=1;
            break;
          
          case 3 : 
            S17=3;
            System.out.println("tick 4-B");//CruiseManager.sysj line: 316, column: 6
            S18=0;
            active[3]=0;
            ends[3]=0;
            tdone[3]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread634757(int [] tdone, int [] ends){
        switch(S16){
      case 0 : 
        active[2]=0;
        ends[2]=0;
        tdone[2]=1;
        break;
      
      case 1 : 
        switch(S15){
          case 0 : 
            S15=0;
            System.out.println("tick 1-A");//CruiseManager.sysj line: 293, column: 2
            S15=1;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
          case 1 : 
            S15=1;
            System.out.println("tick 2-A");//CruiseManager.sysj line: 295, column: 2
            S15=2;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
          case 2 : 
            S15=2;
            System.out.println("tick 3-A");//CruiseManager.sysj line: 297, column: 2
            S15=3;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
          case 3 : 
            S15=3;
            System.out.println("tick 4-A");//CruiseManager.sysj line: 299, column: 2
            testt1_1.setPresent();//CruiseManager.sysj line: 300, column: 2
            currsigs.addElement(testt1_1);
            System.out.println("Emitted testt1_1");
            S15=4;
            active[2]=1;
            ends[2]=1;
            tdone[2]=1;
            break;
          
          case 4 : 
            S15=4;
            S16=0;
            active[2]=0;
            ends[2]=0;
            tdone[2]=1;
            break;
          
        }
        break;
      
    }
  }

  public void thread634754(int [] tdone, int [] ends){
        S634727=1;
    active[7]=1;
    ends[7]=1;
    tdone[7]=1;
  }

  public void thread634753(int [] tdone, int [] ends){
        S634705=1;
    stateVar_thread_6 = 0;//CruiseManager.sysj line: 32, column: 7
    S11794=0;
    active[6]=1;
    ends[6]=1;
    tdone[6]=1;
  }

  public void thread634752(int [] tdone, int [] ends){
        S41=1;
    active[5]=1;
    ends[5]=1;
    tdone[5]=1;
  }

  public void thread634751(int [] tdone, int [] ends){
        S634729=1;
    thread634752(tdone,ends);
    thread634753(tdone,ends);
    thread634754(tdone,ends);
    int biggest634755 = 0;
    if(ends[5]>=biggest634755){
      biggest634755=ends[5];
    }
    if(ends[6]>=biggest634755){
      biggest634755=ends[6];
    }
    if(ends[7]>=biggest634755){
      biggest634755=ends[7];
    }
    if(biggest634755 == 1){
      active[4]=1;
      ends[4]=1;
      tdone[4]=1;
    }
  }

  public void thread634750(int [] tdone, int [] ends){
        S18=1;
    System.out.println("tick 0-B");//CruiseManager.sysj line: 308, column: 6
    S17=0;
    active[3]=1;
    ends[3]=1;
    tdone[3]=1;
  }

  public void thread634734(int [] tdone, int [] ends){
        S16=1;
    off_1.setPresent();//CruiseManager.sysj line: 287, column: 2
    currsigs.addElement(off_1);
    System.out.println("Emitted off_1");
    t_thread_2 = 9;//CruiseManager.sysj line: 288, column: 2
    speed_1.setPresent();//CruiseManager.sysj line: 289, column: 2
    currsigs.addElement(speed_1);
    speed_1.setValue(new Short(t_thread_2));//CruiseManager.sysj line: 289, column: 2
    System.out.println("Emitted speed_1");
    set_1.setPresent();//CruiseManager.sysj line: 290, column: 2
    currsigs.addElement(set_1);
    System.out.println("Emitted set_1");
    System.out.println("tick 0-A");//CruiseManager.sysj line: 291, column: 2
    S15=0;
    active[2]=1;
    ends[2]=1;
    tdone[2]=1;
  }

  public void runClockDomain(){
    for(int i=0;i<ends.length;i++){
      ends[i] = 0;
      tdone[i] = 0;
    }
    
    RUN: while(true){
      switch(S634732){
        case 0 : 
          S634732=0;
          break RUN;
        
        case 1 : 
          S634732=2;
          S634732=2;
          set_1.setClear();//CruiseManager.sysj line: 273, column: 3
          off_1.setClear();//CruiseManager.sysj line: 273, column: 3
          resume_1.setClear();//CruiseManager.sysj line: 273, column: 3
          quickAccel_1.setClear();//CruiseManager.sysj line: 274, column: 3
          quickDecel_1.setClear();//CruiseManager.sysj line: 274, column: 3
          brakePressed_1.setClear();//CruiseManager.sysj line: 274, column: 3
          clock_1.setClear();//CruiseManager.sysj line: 275, column: 3
          speed_1.setClear();//CruiseManager.sysj line: 276, column: 3
          zregulOff_1.setClear();//CruiseManager.sysj line: 279, column: 3
          regulStdby_1.setClear();//CruiseManager.sysj line: 279, column: 3
          regulSet_1.setClear();//CruiseManager.sysj line: 279, column: 3
          regulResume_1.setClear();//CruiseManager.sysj line: 280, column: 3
          speedSet_1.setClear();//CruiseManager.sysj line: 280, column: 3
          cruiseSpeed_1.setClear();//CruiseManager.sysj line: 281, column: 3
          testt1_1.setClear();//CruiseManager.sysj line: 282, column: 3
          S634731=0;
          thread634734(tdone,ends);
          thread634750(tdone,ends);
          thread634751(tdone,ends);
          int biggest634756 = 0;
          if(ends[2]>=biggest634756){
            biggest634756=ends[2];
          }
          if(ends[3]>=biggest634756){
            biggest634756=ends[3];
          }
          if(ends[4]>=biggest634756){
            biggest634756=ends[4];
          }
          if(biggest634756 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
          if(biggest634756 == 1){
            active[1]=1;
            ends[1]=1;
            break RUN;
          }
        
        case 2 : 
          set_1.setClear();//CruiseManager.sysj line: 273, column: 3
          off_1.setClear();//CruiseManager.sysj line: 273, column: 3
          resume_1.setClear();//CruiseManager.sysj line: 273, column: 3
          quickAccel_1.setClear();//CruiseManager.sysj line: 274, column: 3
          quickDecel_1.setClear();//CruiseManager.sysj line: 274, column: 3
          brakePressed_1.setClear();//CruiseManager.sysj line: 274, column: 3
          clock_1.setClear();//CruiseManager.sysj line: 275, column: 3
          speed_1.setClear();//CruiseManager.sysj line: 276, column: 3
          zregulOff_1.setClear();//CruiseManager.sysj line: 279, column: 3
          regulStdby_1.setClear();//CruiseManager.sysj line: 279, column: 3
          regulSet_1.setClear();//CruiseManager.sysj line: 279, column: 3
          regulResume_1.setClear();//CruiseManager.sysj line: 280, column: 3
          speedSet_1.setClear();//CruiseManager.sysj line: 280, column: 3
          cruiseSpeed_1.setClear();//CruiseManager.sysj line: 281, column: 3
          testt1_1.setClear();//CruiseManager.sysj line: 282, column: 3
          switch(S634731){
            case 0 : 
              if(testt1_1.getprestatus()){//CruiseManager.sysj line: 283, column: 9
                S634731=1;
                active[1]=1;
                ends[1]=1;
                break RUN;
              }
              else {
                thread634757(tdone,ends);
                thread634758(tdone,ends);
                thread634759(tdone,ends);
                int biggest634794 = 0;
                if(ends[2]>=biggest634794){
                  biggest634794=ends[2];
                }
                if(ends[3]>=biggest634794){
                  biggest634794=ends[3];
                }
                if(ends[4]>=biggest634794){
                  biggest634794=ends[4];
                }
                if(biggest634794 == 1){
                  active[1]=1;
                  ends[1]=1;
                  break RUN;
                }
                //FINXME code
                if(biggest634794 == 0){
                  S634731=1;
                  active[1]=1;
                  ends[1]=1;
                  break RUN;
                }
              }
            
            case 1 : 
              S634731=1;
              S634732=0;
              active[1]=0;
              ends[1]=0;
              S634732=0;
              break RUN;
            
          }
        
      }
    }
  }

  public void init(){
    char [] active1 = {1, 1, 1, 1, 1, 1, 1, 1};
    char [] paused1 = {0, 0, 0, 0, 0, 0, 0, 0};
    char [] suspended1 = {0, 0, 0, 0, 0, 0, 0, 0};
    paused = paused1;
    active = active1;
    suspended = suspended1;
    // Now instantiate all the local signals ONLY
    set_1 = new Signal();
    off_1 = new Signal();
    resume_1 = new Signal();
    quickAccel_1 = new Signal();
    quickDecel_1 = new Signal();
    brakePressed_1 = new Signal();
    clock_1 = new Signal();
    speed_1 = new Signal();
    zregulOff_1 = new Signal();
    regulStdby_1 = new Signal();
    regulSet_1 = new Signal();
    regulResume_1 = new Signal();
    speedSet_1 = new Signal();
    cruiseSpeed_1 = new Signal();
    testt1_1 = new Signal();
    // --------------------------------------------------
  }
  
  public void run(){
    while(active[1] != 0){
      int index = 1;
      if(paused[index]==1 || suspended[index]==1 || active[index] == 0){
        for(int h=1;h<paused.length;++h){
          paused[h]=0;
        }
      }
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
        if(!df){
          df = true;
        }
        runClockDomain();
      }
      set_1.setpreclear();
      off_1.setpreclear();
      resume_1.setpreclear();
      quickAccel_1.setpreclear();
      quickDecel_1.setpreclear();
      brakePressed_1.setpreclear();
      clock_1.setpreclear();
      speed_1.setpreclear();
      zregulOff_1.setpreclear();
      regulStdby_1.setpreclear();
      regulSet_1.setpreclear();
      regulResume_1.setpreclear();
      speedSet_1.setpreclear();
      cruiseSpeed_1.setpreclear();
      testt1_1.setpreclear();
      int dummyint = 0;
      for(int qw=0;qw<currsigs.size();++qw){
        dummyint = ((Signal)currsigs.elementAt(qw)).getStatus() ? ((Signal)currsigs.elementAt(qw)).setprepresent() : ((Signal)currsigs.elementAt(qw)).setpreclear();
        ((Signal)currsigs.elementAt(qw)).setpreval(((Signal)currsigs.elementAt(qw)).getValue());
      }
      currsigs.removeAllElements();
      set_1.setClear();
      off_1.setClear();
      resume_1.setClear();
      quickAccel_1.setClear();
      quickDecel_1.setClear();
      brakePressed_1.setClear();
      clock_1.setClear();
      speed_1.setClear();
      zregulOff_1.setClear();
      regulStdby_1.setClear();
      regulSet_1.setClear();
      regulResume_1.setClear();
      speedSet_1.setClear();
      cruiseSpeed_1.setClear();
      testt1_1.setClear();
      if(paused[1]!=0 || suspended[1]!=0 || active[1]!=1);
      else{
      }
      if(active[1] == 0){
      	System.out.println("Finished CD");
      }
      if(!threaded) break;
    }
  }
}
